# Pulse shape determination + Variance comparison
import csv
import statistics
import numpy as np
import matplotlib.pyplot as plt

outFile = open('sortedpulserdata.csv', 'w')
outFileWriter = csv.writer(outFile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
makePlots = '0'
makeSmoothed = '0'

DatabaseFile = open('database.csv', 'w')
DBwriter = csv.writer(DatabaseFile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
DBwriter.writerow(['Channel']+[' ']+['Pass/Fail']+[' ']+['Amplitude']+[' ']+['Sorting Function Response']+[' ']+['Mean']+[' ']+['Variance'])

def running_mean(seq, window_size):
    return np.convolve(seq, np.ones((window_size,))/window_size, mode='valid')

with open('rawOutput.csv') as inputRawValues:
    rawReader = csv.reader(inputRawValues, delimiter=',', quotechar='|')
    channel = 0
    chan = []
    Vpp = {}
    response = {}
    variances = {}
    averages = {}
    Result = {}
    for line in rawReader:
        if len(line) == 1:  # channel number line, only one element
            channel = int(line[0])  # get channel number
            chan.append(channel)
            if chan[0] != 0:
                channelThreshold = 0.02
                high = 0.08
                low = 0.04
                AB = 'P'
            else:
                channelThreshold = 0.0075
                high = 0.09
                low = 0.04
                AB = 'S'
        else:
            values = [float(i) for i in line]  # convert strings to floats
            if makePlots == '0':
                plt.plot(values)
                plt.xlabel('Time (arbitrary units)')
                plt.ylabel('Voltage (V)')
                plt.grid(True)
                plt.savefig('channel' + str(channel) + 'Raw.png')
                plt.close()
            smoothedsignal = running_mean(values, 1000)
            Vpp[channel] = max(smoothedsignal) - min(smoothedsignal)
            avg = statistics.mean(smoothedsignal)
            var = np.var(smoothedsignal)
            variances[channel] = var
            averages[channel] = avg
            peaks = 0
            noise = 0
            if makeSmoothed == '0':
                plt.plot(smoothedsignal)
                plt.xlabel('Time (arbitrary units)')
                plt.ylabel('Voltage (V)')
                plt.grid(True)
                plt.savefig('channel' + str(channel) + 'Smoothed.png')
                plt.close()
            means = []
            for i in range(0, 12):
                total = 0
                for j in range(4000*i, 4000*i+4000):
                    total = total+smoothedsignal[j]
                average = total/4000
                means.append(average)
                ratio = abs(average/avg)
                if 1.25 > ratio > 0.75:
                    noise += 1
                elif ratio > 1.25:
                    peaks += 1
                elif ratio < 0.75: 
                    peaks += 1
            varmean = np.var(means)
            response[channel] = 5*varmean/Vpp[channel]
            if peaks > noise:
                if response[channel] >= channelThreshold:
                    if Vpp[channel] > high:
                        outFileWriter.writerow([int(channel)]+[4])
                        Result[channel] = 'PASS'
                    elif Vpp[channel] < low:
                        outFileWriter.writerow([int(channel)]+[2])                    
                        Result[channel] = 'PASS'
                    elif low <= Vpp[channel] <= high:
                        outFileWriter.writerow([int(channel)]+[3])
                        Result[channel] = 'PASS'
                elif response[channel] < channelThreshold:
                    outFileWriter.writerow([int(channel)]+[1])
                    Result[channel] = 'FAIL'
                else:
                    outFileWriter.writerow([int(channel)]+[1])
                    Result[channel] = 'FAIL'
            elif noise > peaks:
                if Vpp[channel] > high: 
                    outFileWriter.writerow([int(channel)]+[1])
                    Result[channel] = 'FAIL'
                elif response[channel] <= channelThreshold:
                    outFileWriter.writerow([int(channel)]+[0])
                    print 'channel', channel, 'failed'
                    Result[channel] = 'FAIL'
                else:
                    outFileWriter.writerow([int(channel)]+[1])
                    Result[channel] = 'FAIL'
            else: 
                print 'channel', channel
                print 'peaks', peaks
                print 'noise', noise
                outFileWriter.writerow([int(channel)]+[1])
                Result[channel] = 'FAIL'
    if AB == 'S':
        if max(chan) == 50:
            if len(chan) > 51:
                chan = chan[-51:]
        elif max(chan) == 255:
            if len(chan) > 256:
                chan = chan[-256:]
    elif AB == 'P':
        numpad = 0
        numwire = 38
        if min(chan) == 31:
            numpad = 24
            if max(chan) == 235:
                numwire = 37
        else:
            numpad = 39
        pabrange = numpad + numwire
        if len(chan) > pabrange:
            chan = chan[-pabrange:]
    xs = []
    Amplitude = []
    Threshold = []
    Variance = []
    TRatio = []
    for channel in chan:
        DBwriter.writerow([channel]+[' ']+[Result[channel]]+[' ']+[Vpp[channel]]+[' ']+[response[channel]]+[' ']+[averages[channel]]+[' ']+[variances[channel]])
        xs.append(channel)
        tr = response[channel]/Vpp[channel]
        Amplitude.append(Vpp[channel])
        Threshold.append(response[channel])
        Variance.append(variances[channel])
        TRatio.append(tr)   
    plt.plot(xs, Amplitude, label='Signal Amplitude')#, linestyle="", marker=",")
    plt.plot(xs, Variance, label='Signal Variance')#, linestyle="", marker=",")
    plt.plot(xs, Threshold, label='Sorting Function Response')#, linestyle="", marker=",")
    #plt.plot(xs, TRatio, label='Resp/Amp Ratio')#, linestyle="", marker=",")
    plt.xlabel('Readout Element (GFZ Channel Number)')
    plt.ylabel('Voltage (V)')
    # plt.legend(loc=1)
    plt.grid(True)
    plt.savefig('AmpDist.png')
    plt.show()