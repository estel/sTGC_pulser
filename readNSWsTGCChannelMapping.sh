#!/bin/bash
# required arguments: (layer, AB, gfz, channel, PC, stgc)

############################################################################
# It requires that you copy the mapping project in the directory above:
# cd ..
# git clone  https://gitlab.cern.ch/McGill-sTGC/NSW_sTGC_Channel_Mapping.git 
############################################################################


layer=$1
AB=$2
gfz=$3
channel=$4
PC=$5
shortstgc=$6

# Translate AB S/P to strip/pad/wire
if [ $AB == "S" ] 
then 
    AB="strip"
fi
if [ $AB == "P" ] 
then 
    if [ $channel -gt 197 ]
    then
        AB="wire"
    else
        AB="pad"
    fi
fi

# Translate PIVOT/CONFIRM to P/C
if [ $PC == "CONFIRM" ]
then 
	PC="C"
fi

if [ $PC == "PIVOT" ]
then 
	PC="P"
fi

# Attach it to the stgc quad type
stgc="$shortstgc$PC"

# Translate layer number into Gas Gap number
ggap=$layer 
if [ $PC == "C" ]
then 
	if [ $layer == "1" ]
	then
		ggap=4
	elif [ $layer == "2" ]
	then	
        	ggap=3
	elif [ $layer == "3" ]
	then
        	ggap=2
	elif [ $layer == "4" ]
	then
        	ggap=1
	fi
fi

# Use awk to get the line you need from the mapping text file
#myline=$(cat ../NSW_sTGC_Channel_Mapping/mapping.txt | awk  -v GGAP="$ggap" -v AB="$AB"  -v GFZ="$gfz" -v CH="$channel" -v PC="$PC" -v stgc="$stgc" '$1 == stgc && $2  == GGAP  && $3 == AB && $9 == GFZ &&  $11 == CH { print }' )
myline=$(cat ../NSW_sTGC_Channel_Mapping/mapping.txt | awk  -v GGAP="$ggap" -v AB="$AB"  -v GFZ="$gfz" -v CH="$channel" -v PC="$PC" -v stgc="$stgc" '$1 == stgc && $2  == GGAP  && $4 == AB && $10 == GFZ &&  $12 == CH { print }' )

# return the full line
echo $myline

