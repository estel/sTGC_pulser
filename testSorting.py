import subprocess
import os

DATAFOLDER='data/' #sys.argv[1]
#DATAFOLDER='../REFdata/' #sys.argv[1]
UniqueID='WEDGE2'
UniqueID='QL2_PIVOT-TEST'
MYAmplitudeQS1S = []
MYAmplitudeQS1P = []
MYAmplitudeQS1W = []
MYAmplitudeQS1HV0S = []
MYAmplitudeQS1HV0P = []
MYAmplitudeQS1HV0W = []
MYAmplitudeQS1HV1S = []
MYAmplitudeQS1HV1P = []
MYAmplitudeQS1HV1W = []
MYAmplitudeQS2S = []
MYAmplitudeQS2P = []
MYAmplitudeQS2W = []
MYAmplitudeQS3S = []
MYAmplitudeQS3P = []
MYAmplitudeQS3W = []
for PC in ['PIVOT'] : 
    pc = 'P'
    for stgc in ['QL2'] :
    #for stgc in ['QS1-HV0','QS1-HV1','QS2','QS3'] :
        for AB in ['P'] : 
            if AB == 'P' : GFZlist = ['P1']
            elif AB == 'S' : GFZlist = ['P1','P2']
            for layer in ['2'] :
            #for layer in ['1','2','3','4'] :
                slayer='Layer'+layer
                for GFZ in GFZlist : 
                    if stgc == 'QS1': HVlist=['']
                    #if stgc == 'QS1': HVlist=['HV1','HV0']
                    elif stgc == 'QS1-HV0': HVlist=['HV0']
                    elif stgc == 'QS1-HV1': HVlist=['HV1']
                    else: HVlist=['']
                    for HV in HVlist : 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV1' ) and slayer == 'Layer1' and AB == 'S' and GFZ == 'P1' and HV == 'HV1': continue 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV0' ) and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV0' ) and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV0' ) and slayer == 'Layer4' and AB == 'S' and GFZ == 'P2' and HV == 'HV0': continue 
                      print('==============')
                      #find my file
    
                      for runFolder in os.listdir(DATAFOLDER):
                          STGC=stgc
                          if stgc == 'QS1-HV0' : STGC = 'QS1'
                          if stgc == 'QS1-HV1' : STGC = 'QS1'
                          if STGC in runFolder and slayer in runFolder and PC in runFolder and '_'+AB+'_' in runFolder and 'GFZ'+GFZ in runFolder and HV in runFolder and UniqueID in runFolder and not 'kk_' in runFolder and not 'good_' in runFolder:
                              print("runFolder= %s" %(runFolder))



                              ## Redo the sorting with the new thresholds
                              #bashCommand = "sudo python PSDVCsorting.py "+runFolder+" 0 "+stgc+" "+pc+" "+AB+" "+GFZ+" "+layer
                              bashCommand = " python PSDVCsorting.py ../"+DATAFOLDER+runFolder+" 0 "+stgc+" "+pc+" "+AB+" "+GFZ+" "+layer
                              print(bashCommand)
                              process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
                              output, error = process.communicate()
                              print(output)
                              print(error)
                              
                              #gfzCommand = "sudo python sortedchannelplot.py data/"+runFolder+" "+stgc+" "+pc+" "+AB+" "+GFZ+" "+layer
                              gfzCommand = " python sortedchannelplot.py "+DATAFOLDER+runFolder+" "+stgc+" "+pc+" "+AB+" "+GFZ+" "+layer
                              print(gfzCommand)
                              gfzprocess = subprocess.Popen(gfzCommand.split(), stdout=subprocess.PIPE)
                              gfzoutput, gfzerror = gfzprocess.communicate()
                              print(gfzoutput)
                              print(gfzerror)

                             # grepCommand = "grep -r FAIL data/"+runFolder+"/*"
                             # print(grepCommand)
                             # grepprocess = subprocess.Popen(grepCommand.split(), stdout=subprocess.PIPE)
                             # grepoutput, greperror = grepprocess.communicate()
                             # print(grepoutput)
                             # print(greperror)

