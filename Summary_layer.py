from foldernameDB import getFoldersList
import matplotlib.pyplot as plt
import numpy as np
from QABmap import getQuanUniqueID
from QABmap import getFolderUniqueName
from matplotlib.ticker import FormatStrFormatter
from foldernameDB import getFoldersList
from QABmap import Qmap
import Tkinter as tk

window = tk.Tk() #window is an instance of tkinter's Tk class
window.title('Whole Layer')

def show_values():
	stgc.get()
	PC.get()
	layer.get()
	uid.get()
	window.destroy()

#frame 1 (var=stgc)
frm1 = tk.Frame(master=window)
frm1.grid(padx=100, pady=20)
lbl_1 = tk.Label(master=frm1, text='Quad Type:')

stgc = tk.StringVar()

bttn_ql1_hv1 = tk.Radiobutton(master=frm1,text='QL1-HV1', variable=stgc, value='QL1-HV1')
bttn_ql1_hv0 = tk.Radiobutton(master=frm1,text='QL1-HV0', variable=stgc, value='QL1-HV0')
bttn_ql2 = tk.Radiobutton(master=frm1,text='QL2', variable=stgc, value='QL2')
bttn_ql3 = tk.Radiobutton(master=frm1,text='QL3', variable=stgc, value='QL3')
bttn_qs1_hv1 = tk.Radiobutton(master=frm1, text='QS1-HV1', variable=stgc, value='QS1-HV1')
bttn_qs1_hv0 = tk.Radiobutton(master=frm1,text='QS1-HV0', variable=stgc, value='QS1-HV0')
bttn_qs2 = tk.Radiobutton(master=frm1,text='QS2', variable=stgc, value='QS2')
bttn_qs3 = tk.Radiobutton(master=frm1,text='QS3', variable=stgc, value='QS3')

lbl_1.grid(row=0,column=0)
bttn_ql1_hv1.grid(row=0,column=1)
bttn_ql1_hv0.grid(row=0,column=2)
bttn_ql2.grid(row=0,column=3)
bttn_ql3.grid(row=0,column=4)
bttn_qs1_hv1.grid(row=0,column=5)
bttn_qs1_hv0.grid(row=0,column=6)
bttn_qs2.grid(row=0,column=7)
bttn_qs3.grid(row=0,column=8)

#frame 2 (var=PC)
frm2 = tk.Frame(master=window)
frm2.grid(padx=150, pady=20)

PC = tk.StringVar()

lbl_2 = tk.Label(master=frm2, text='Pivot or Confirm:')
bttn_piv = tk.Radiobutton(master=frm2, text='Pivot', variable=PC, value='PIVOT')
bttn_conf = tk.Radiobutton(master=frm2, text='Confirm', variable=PC, value='CONFIRM')

lbl_2.grid(row=1, column=0)
bttn_piv.grid(row=1, column=1)
bttn_conf.grid(row=1, column=2)

#frame 3 (var=uid)
frm3 = tk.Frame(master=window)
frm3.grid(padx=150, pady=20)

uid = tk.StringVar()

lbl_3 = tk.Label(master=frm3, text='Unique Identifier:')
ent_uid = tk.Entry(master=frm3, textvariable=uid)

lbl_3.grid(row=2, column=0)
ent_uid.grid(row=2,column=1)

#frame 4 (var=layer)
frm4 = tk.Frame(master=window)
frm4.grid(padx=100, pady=20)

layer = tk.IntVar()

lbl_4 = tk.Label(master=frm4, text='Layer:')
bttn_lay1 = tk.Radiobutton(master=frm4, text='1', variable=layer, value=1)
bttn_lay2 = tk.Radiobutton(master=frm4, text='2', variable=layer, value=2)
bttn_lay3 = tk.Radiobutton(master=frm4, text='3', variable=layer, value=3)
bttn_lay4 = tk.Radiobutton(master=frm4, text='4', variable=layer, value=4)

lbl_4.grid(row=3,column=0)
bttn_lay1.grid(row=3,column=1)
bttn_lay2.grid(row=3,column=2)
bttn_lay3.grid(row=3,column=3)
bttn_lay4.grid(row=3, column=4)


#frame 8
frm8 = tk.Frame(master=window)
frm8.grid(padx=20, pady=20)

RunBttn = tk.Button(master=frm8, text='RUN', command=show_values)
RunBttn.grid(row=7, column=0)
window.mainloop()
'''
stgc='QL2'
layer=2
channel=0
PC = 'PIVOT'
uid='-7_'
'''
stgc=stgc.get()
layer=layer.get()
channel=0
PC = PC.get()
uid=str('-' + uid.get() +'_')

def whole_layer (layer, stgc, PC, uid):
	
	#specify pre-req of AB=S to get strip data first
	folderList=getFoldersList( "" , stgc , layer, 'S' ,"", "")
	#objectNumber=Qmap(int(layer), AB, gfz, int(channel), PC, stgc, 0) # 0:do not print out	
	if len(folderList) == 0:
		print('There is no data attributed with that Quad name.')
		quit()
	
	quadName = PC + uid
	two_strips = []
	for folder in folderList:
		if quadName in folder:
			two_strips.append(folder)
	
	#make the file name into an openable file path
	temp = []
	for quadFile in two_strips:
		quadFile = 'data/' + str(quadFile) + '/database.csv'
		temp.append(quadFile)
	two_strips = temp
	
	alamP1 = []
	alamP2 = []
	for quadFile in two_strips:
		#load and access the file's database.csv data
		quadData = np.loadtxt(str(quadFile), delimiter = ', ,', skiprows=1, usecols=(0,2))
		quadChannel = quadData[:,0]
		#change the channel column from GFZ into ALAM's mapping
		for i in range(len(quadChannel)):
			channel = quadChannel[i]
			if 'P1' in quadFile:
				gfz='P1'
				alamP1.append(Qmap(int(layer), 'S', gfz, int(channel), PC, stgc, 0))
				P1_amp = quadData[:,1]
			
			else:
				gfz='P2'
				alamP2.append(Qmap(int(layer), 'S', gfz, int(channel), PC, stgc, 0))
				P2_amp = quadData[:,1]

	
	#plot pads and wires


	#specify a pre-req that AB=P
	folderList=getFoldersList( "" , stgc , layer, 'P' ,"", "")
	#objectNumber=Qmap(int(layer), AB, gfz, int(channel), PC, stgc, 0) # 0:do not print out	
	
	if len(folderList) == 0:
		print('There is no data attributed with that Quad name.')
		quit()

	quadName = PC + uid
	myPad = []
	for folder in folderList:
		if quadName in folder:
			myPad.append(folder)

	#make it into an openable file path
	myPad[0] = 'data/' + str(myPad[0]) + '/database.csv'
	
	loadPad = np.loadtxt(str(myPad[0]), delimiter = ', ,', skiprows=1, usecols=(0,2))
	
	channels = loadPad[:,0]
	amps = loadPad[:,1]
	
	for i in range(len(channels)-1):
		if channels[i] != channels[i+1]-1:
			wireIndex = i 
			break
	
	padChans = channels[0:wireIndex+1]
	padAmps = amps[0:wireIndex+1]
	wireChans = channels[wireIndex+1::]
	wireAmps = amps[wireIndex+1::]


	fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True)

	ax1.plot(alamP1, P1_amp, label='SP1', color='lightsteelblue')
	ax1.plot(alamP2, P2_amp, label='SP2', color='slategrey')
	ax1.legend()
	ax1.set_xlabel('Strips')
	ax1.set_ylabel('Amplitude (V)')
	ax1.grid(which='major')
	ax2.plot(padChans, padAmps, color='cornflowerblue')
	ax2.set_xlabel('Pads')
	ax2.grid(which='major')
	ax3.plot(wireChans, wireAmps, color='royalblue')
	ax3.set_xlabel('Wires')
	ax3.grid(which='major')
	plt.show()

whole_layer (layer, stgc, PC, uid)























