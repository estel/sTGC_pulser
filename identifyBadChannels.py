
import os
import sys
import csv
import statistics
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from QABmap import GFZChannel2Pin
from QABmap import Qmap
from QABmap import IsWire
from QABmap import getQuanUniqueID
from QABmap import getFolderUniqueName
from QABmap import IsValidChannel
#from Beep import finished
from matplotlib.ticker import FormatStrFormatter
import math
from Thresholds import getAmplitudeLowThreshold
from Thresholds import getIntegralRatioThreshold
import subprocess

#from foldernameDB import getFoldersList

STRIPPITCH=3.2
WEDGEANGLE=8.5/360.*2.*math.pi
def NumberToPosition(AB, objectNumber):
    position = -999
    if AB=='S':
        position=(objectNumber-9)*(STRIPPITCH/math.cos(WEDGEANGLE))+1.685
    return position


def getLine(layer, AB, gfz, channel, PC, stgc):
    print layer, AB, gfz, channel, PC, stgc
    res = subprocess.check_output(["./readNSWsTGCChannelMapping.sh", layer, AB, gfz, channel, PC, stgc])
    for line in res.splitlines():
        return line


def getVMMNumberAndChannel(layer, AB, gfz, channel, PC, stgc):
    line = getLine(layer, AB, gfz, channel, PC, stgc)
    print line
    cols = line.split(" ")
    print cols
    #return cols[3], cols[4]
    return cols[4], cols[5]
 
def getGasGap( PC, layer):
    ggap=layer
    if PC == "C" or PC == "CONFIRM":
        if layer == "1" :   ggap=4
        elif layer == "2" : ggap=3
        elif layer == "3" : ggap=2
        elif layer == "4" : ggap=1

    return ggap




# give readable names to the arguments
makePlots = sys.argv[2] # not used so far
stgc = sys.argv[3]
PC = sys.argv[4]
AB = sys.argv[5]
gfz = sys.argv[6]
layer = sys.argv[7]


#folder 0 QL1 C S P1 1



#REFdata='/home/userpulser/REFdata'
MYfilename = 'data/'+sys.argv[1]+'/database.csv'    #Write to CSV for GFZ Mapping
#REFfilename='NULL'
#quadFolder='NULL'
#if 'data' in MYfilename :
#    print(" data in myfilename found")
#look for the corresponding reference file
## From the database
quadUniqueID=getQuanUniqueID(MYfilename, stgc)

## Look for the corresponding reference file 
#if 'HV0' in MYfilename: HV='HV0_'
#elif 'HV1' in MYfilename: HV='HV1_'
#else: HV=''
#for runFolder in os.listdir(REFdata+'/'):
#    #Compare HV0 and HV1 to the same reference file QS1. #if stgc in runFolder and 'Layer'+layer in runFolder and gfz in runFolder and '_'+AB+'_' in runFolder and quadUniqueID in runFolder and HV in runFolder: 
#    if stgc in runFolder and 'Layer'+layer in runFolder and gfz in runFolder and '_'+AB+'_' in runFolder and quadUniqueID in runFolder : 
#        print("runFolder = %s" %(runFolder))    
#        REFfilename=REFdata+'/'+runFolder+'/database.csv'
#
#RefFileExists=False
#if os.path.exists(REFfilename) and os.path.getsize(REFfilename) > 0:
#    # Non empty file exists
#    print('Reference file = %s exists and has non-zero size.' %(REFfilename))
#    RefFileExists = True
#else:
#    # Non existent file
#    print('Reference file = %s does not exist or has zero size.' %(REFfilename))

# Get data from your file 
#print("MYfilename = %s" %(MYfilename))
with open(MYfilename) as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    MYxs = []
    MYGfzPinNumber = []
    MYObjectNumber = []
    MYObjectPosition = []
    MYSPW = []
    MYVMMnum = []
    MYVMMch = []
    MYAmplitude = []
    MYResponse = []
    MYVariance = []
    MYIntRatio = []
    firstrow = True
    for row in readCSV:
        if firstrow : 
            firstrow = False
            continue
        channel = row[0]
        amplitude = row[4]
        response = row[6]
        variance = row[10]
        intratio = row[14]



        # If not a valid channel, skip it!
        hvstgc=stgc
        if 'HV0' in MYfilename and 'HV0' not in stgc:
            hvstgc=stgc+"-HV0"
        if 'HV1' in MYfilename and 'HV1' not in stgc:
            hvstgc=stgc+"-HV1"
        isValidChannel = IsValidChannel(hvstgc, AB, int(layer), gfz, int(channel), PC)
        #if isValidChannel : 
        #    print("isValidChannel = %s" %(isValidChannel))
        #print("%s isValidChannel %s %s" %(channel, isValidChannel, stgc))
        isvalidchannel=True
        if not isValidChannel : 
            print("channel in the csv file does not actually exist. Skip it!")
            isvalidchannel=False
            continue;

        MYxs.append(int(channel))
        MYAmplitude.append(float(amplitude))
        MYResponse.append(float(response))
        MYVariance.append(float(variance))
        MYIntRatio.append(float(intratio))
        #print('channel= %s  Amplitude= %s' %(channel, amplitude))


        pin=GFZChannel2Pin(channel)
        MYGfzPinNumber.append(str(pin))

        objectNumber=-1
        objectNumber=Qmap(int(layer), AB, gfz, int(channel), PC, stgc, 0) # 0:do not print out
        MYObjectNumber.append(int(objectNumber))

        objectPosition=-999
        objectPosition=NumberToPosition(AB, int(objectNumber))
        MYObjectPosition.append(float(objectPosition))

        if 'P' in AB and IsWire(stgc, AB, int(layer), gfz, int(channel), PC):
            MYSPW.append('Wire')
        elif 'S' in AB:
            MYSPW.append('Strip')
        elif 'P' in AB:
            MYSPW.append('Pad')
        else:
            MYSPW.append('?')

        VMMnum, VMMch = getVMMNumberAndChannel(layer, AB, gfz, channel, PC, stgc)
        MYVMMnum.append(VMMnum) 
        MYVMMch.append(VMMch) 


print(quadUniqueID)
df = pd.DataFrame() #MYxs, MYAmplitude)
df['StripPadWire']=MYSPW
df['GFZchannel']=MYxs
df['Amplitude']=MYAmplitude
df['GFZPin']=MYGfzPinNumber
df['Number']=MYObjectNumber
df['VMMnumber']=MYVMMnum
df['VMMchannel']=MYVMMch
df['Position']=MYObjectPosition
df['IntRatio']=MYIntRatio # adding a field. Mind format issues!

df.insert(0, 'GFZ', gfz)
#if IsWire(stgc, int(layer), gfz, int(channel)):
#    df.insert(0,'StripPadWire','W')
#else:
#    df.insert(0,'StripPadWire', AB)

df.insert(0,'GasGap', getGasGap(PC, layer))
df.insert(0,'Layer',layer)
df.insert(0,'Quad',quadUniqueID)
df.insert(0,'Test','FAIL')


#df.sort_values(by=['Number'])
#df = df.sort_values(by=['Number'],ascending=False)
df = df.sort_values(by=['Number'])
#print(df.head())
#print('len = %s' %(len( (df['Amplitude']<getAmplitudeLowThreshold(sys.argv[1]) ).index)))

#test
#print df[df['IntRatio']<10.*getIntegralRatioThreshold(sys.argv[1])]
#print df[df['StripPadWire']=='Wire']
#print df[ ( df['IntRatio']<10.*getIntegralRatioThreshold(sys.argv[1]) )  & ( df['StripPadWire']=='Wire' ) ]
#end test


#if len( ( df[df['Amplitude']<getAmplitudeLowThreshold(sys.argv[1])] ).index) :
if len( ( df[df['Amplitude']<getAmplitudeLowThreshold(sys.argv[1])] ).index) or  len( ( df[ ( df['IntRatio']<10.*getIntegralRatioThreshold(sys.argv[1]) ) & ( df['StripPadWire']=='Wire' ) ] ).index)  :
    #print(df[df['Amplitude']<getAmplitudeLowThreshold(sys.argv[1])])

    df_badchannels = df[ ( df['Amplitude']<getAmplitudeLowThreshold(sys.argv[1]) ) | ( ( ( df['IntRatio']<10.*getIntegralRatioThreshold(sys.argv[1]) )  & ( df['StripPadWire']=='Wire' ) ) ) ]
    #df_badchannels = df[df['Amplitude']<getAmplitudeLowThreshold(sys.argv[1])]

    #df_badchannels.style.format({'Amplitude': "{:.6}"})   
    #df_badchannels.style.format({'Position': "{:.3}"})   
    #print(df_badchannels.style.format({'Amplitude': "{:0.6}", 'Position': '{:.2}'}))

    print(df_badchannels.to_string(formatters={"Amplitude": "{:,.6f}".format, "Position": '{:,.2f}'.format}))
    df_precision = df_badchannels.to_string(formatters={"Amplitude": "{:,.6f}".format, "Position": '{:,.2f}'.format})



    export_csv = df_badchannels.to_csv (r'badchannels.csv', index = None, header=True) #Don't forget to add '.csv' at the end of the path


    df_badchannels.insert(0,'Folder',sys.argv[1])

    print(df_badchannels[['Folder','GFZchannel']])



#else:
#    print('is empty')

### Get data from the reference file
##if RefFileExists :
##    with open(REFfilename) as csvfile:
##        readCSV = csv.reader(csvfile, delimiter=',')
##        REFxs = []
##        REFAmplitude = []
##        REFResponse = []
##        REFVariance = []
##        firstrow = True
##        for row in readCSV:
##            if firstrow : 
##                firstrow = False
##                continue
##            channel = row[0]
##            amplitude = row[4]
##            response = row[6]
##            variance = row[10]
##    
##            REFxs.append(int(channel))
##            REFAmplitude.append(float(amplitude))
##            REFResponse.append(float(response))
##            REFVariance.append(float(variance))
##            #print('channel= %s  Amplitude= %s' %(channel, amplitude))
#
##print(len( MYxs))
##print( len( MYAmplitude))
#
##Create amplitude distribution plot
##Build universal unique name for title and output file
#outfilename =  getFolderUniqueName(MYfilename, stgc, HV, layer, AB, gfz)
#
#
#fig, ax = plt.subplots(figsize=(4+20*(MYxs[len(MYxs)-1]-MYxs[0]) /256,6)) # scale the canvas X size depending on the channels we are looking at
#plt.plot(MYxs, MYAmplitude, 'b-',label='Signal Amplitude - CERN')
#plt.title(outfilename)
#if RefFileExists :
#    plt.plot(REFxs, REFAmplitude, '-', color='gray', label='Signal Amplitude - REFERENCE')
##plt.plot(xs, Variance, label='Signal Variance') #no need to plot the variance
##plt.plot(xs, Response, label='Sorting Function Response') #no need to plot the response (might have been calculated differently)
#plt.xlabel('Readout Element (GFZ Channel Number)')
#plt.ylabel('Voltage (V)')
#plt.grid(True)
#
## Don't allow the axis to be on top of your data
#ax.set_axisbelow(True)
## Turn on the minor TICKS, which are required for the minor GRID
#ax.minorticks_on()
## Customize the major grid
#ax.grid(which='major', linestyle='-', linewidth='0.5', color='silver')
## Customize the minor grid
##ax.grid(which='minor', linestyle=':', linewidth='0.5', color='gray')
#
#ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
##plt.axis([ , ,ymin=0, ymax=0.12])
#axes = plt.gca()
#plt.ylim(bottom=0)
#
##Save
#plt.savefig('compare/'+outfilename+'.png')
#
##print("outfilename = %s" %(outfilename))
#plt.show() # TODO comment out if offline reprocessing
#
#
