#!/usr/bin/env python
from Tkinter import *
import setchan
import tkMessageBox
import subprocess

class Ardwin:

    def __init__(self, master):
        frame3 = Frame(master)

        """ arduinoframe = Frame(padx=50,pady=10)
        arduinoframe.pack() """
        """ ardlabel = Label(arduinoframe, text='Connect To Arduino:', width=25)
        ardlabel.pack(side=LEFT)
        ardbutton = Button(arduinoframe, text='Test', command=self.Arduino)
        ardbutton.pack(side=RIGHT) """

        optionframe = Frame(padx=50, pady=10)
        optionframe.pack()
        setlabel = Label(optionframe, text='Enter Channel Number:', width=25)
        setlabel.pack(side=LEFT)
        setbutton = Button(optionframe, text='Set', command=self.Set)
        setbutton.pack(side=RIGHT, padx=10)
        self.set = Entry(optionframe, width=15)
        self.set.pack(side=RIGHT)

        quitframe = Frame(padx=50, pady=10)
        quitframe.pack(side=BOTTOM)
        back = Button(quitframe, text="Done", command=frame3.quit)
        back.pack(side=LEFT)

        frame3.pack()

    def Set(self):
        self.arduino = setchan.connect()
        inset = str(self.set.get())
        setchan.setChannel(inset, self.arduino)
        print('Channel Set to %s' %(inset))
        
        
root = Tk()
arduinowindow = Ardwin(root)
root.mainloop()
