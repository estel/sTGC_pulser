import serial
import time
import usbtmc
import datetime
import matplotlib.pyplot as plt
import sys
import setchan
from QABmap import *
from Beep import finished

## Not used
#def arduinoCom():
#    arduino = serial.Serial('/dev/ttyUSB0', 9600)
#    if not arduino :
#        arduino = serial.Serial('/dev/ttyUSB1', 9600)
#    channel = 0
#    while (channel < 256):
#        arduino.write("+\n")
#        channel += 1
#        print channel
#        time.sleep(0.2)


class Digitizer:
    def __init__(self):
        resource = usbtmc.list_resources()[0]
        self.instrument = usbtmc.Instrument((int)(resource.split("::")[1]), (int)(resource.split("::")[2]))
        instrument_idn = self.instrument.ask("*IDN?")
        print instrument_idn

    def delayedWrite(self, command):
        time.sleep(.01)
        self.instrument.write(command)
        print "WRITE: " , command 
        time.sleep(.01)

    def gather(self):
        #if (0):
        #    self.delayedWrite(":SYSTEM:HEADER OFF")  # Controls whether response contains command header
        #    self.delayedWrite(":WAVEFORM:SOURCE CHANNEL1")  # Output data channel
        #    self.delayedWrite(":WAVEFORM:FORMAT ASCII")  # Output data Format

        #    # self.delayedWrite("ACQUIRE:POINTS:AUTO")   #Precision of numbers?
        #    # self.delayedWrite("ACQuire:AVER OFF")
        #    self.delayedWrite("ACQUIRE:AVERAGE ON")
        #    self.delayedWrite("ACQUIRE:AVERAGE:COUNT 16")
        #    self.delayedWrite("ACQuire:MODE RTIME")
        #    # self.delayedWrite(":CHANNEL3:INPut DC")
        #    # self.delayedWrite(":TRIGger:AND:SOURce CHANNEL2")
        #    # self.delayedWrite("TRIGger:HTHReshold CHANnel2 ")

        #    self.delayedWrite(":DIGITIZE CHANNEL1")  # Collect data
        #    self.delayedWrite(":WAVEFORM:DATA?")
        #    self.delayedWrite(':MEASURE:RESULTS?')

        #    time.sleep(.1)
        #    data = self.instrument.read()

        #    self.delayedWrite(":RUN")
        #    for i in range(4):
        #        self.delayedWrite("CHANNEL" + str(i + 1) + ":DISPLAY ON")
        #        self.delayedWrite(":CHANnel" + str(i + 1) + ":DISPlay:OFFSet 0")

        instrument_idn = self.instrument.ask("*IDN?")
        instrument_idn_name = instrument_idn.split(",")[1]
        #print instrument_idn_name

        if instrument_idn_name == 'RTB2004' :
            #self.delayedWrite("DISPlay:CLEar")
            self.delayedWrite("FORMat ASCii")
            self.delayedWrite("ACQuire:POINts: AUTomatic")
            self.delayedWrite("CHANnel1:STATe ON")
            if(1):
                #print "FORMat?" , self.instrument.ask("FORMat?")
                #print "SING;*OPC?" , self.instrument.ask("SING;*OPC?")
                #print "CHAN1:DATA:HEAD?" , self.instrument.ask("CHAN1:DATA:HEAD?")
                self.delayedWrite("ACQuire:AVERage:COUNt 16")  
                print "CHAN1:DATA:POINTS?" , self.instrument.ask("CHAN1:DATA:POINTS?")
                #print "CHAN1:DATA?" , self.instrument.ask("CHAN1:DATA?")
                self.delayedWrite("CHAN1:DATA?")

            else:
                # try to measure the delay between the pulse sent and the pad signal recieved
                self.delayedWrite("MEASurement1:STATistics:RESet")
                self.delayedWrite("MEASurement1[:ENABle] MEASurement1:MAIN DELay")
                self.delayedWrite("MEASurement1:STATistics[:ENABle] ON")
                self.delayedWrite("ACQuire:AVERage:RESet") 
                self.delayedWrite("ACQuire:TYPE AVERage") 
                self.delayedWrite("ACQuire:AVERage:COUNt 100")  
                self.delayedWrite("MEASurement1:SOURce") 
                self.delayedWrite("REFCurve2:SOURce") 
                self.delayedWrite("MEASurement1:DELay:SLOPe POSitive POSitive")
                #self.delayedWrite("EXPort:MEASurement1:STATistics:NAME MyPadDelayFile")
                #self.instrument.ask("MEASurement<m>:STATistics:VALue:ALL?")
                #self.delayedWrite("MEASurement<m>:STATistics:VALue:ALL?")
                print("sleep 5s")
                time.sleep(10)
                #self.delayedWrite("RUNSingle") 
                self.delayedWrite("MEASurement1:STATistics:VALue:ALL?")
                #print "MEASurement1:STATistics:VALue:ALL?" , self.instrument.ask("MEASurement1:STATistics:VALue:ALL?")
                #self.instrument.ask("MEASurement<m>:STATistics:VALue:ALL?")
                #self.delayedWrite("EXPort:MEASurement1:STATistics:SAVE")
                #self.instrument.ask("EXPort:MEASurement1:STATistics:SAVE")


        else:
            self.delayedWrite(":SYSTEM:HEADER OFF")
            self.delayedWrite(":ACQuire:MODE RTIME")
            self.delayedWrite(":ACQuire:COMPlete 100")
            self.delayedWrite(":WAVeform:SOURce CHANnel1")
            self.delayedWrite(":WAVEFORM:FORMAT ASCII")
            self.delayedWrite("ACQUIRE:AVERAGE ON")
            self.delayedWrite(":ACQuire:COUNt 8")
            #self.delayedWrite(":ACQuire:POINts 50000")
            self.delayedWrite(":DIGitize CHANnel1")
            self.delayedWrite(":WAVeform:DATA?")

        time.sleep(.1)
        data = self.instrument.read()

        return data


def printData(dirpath, data, channel, firstchan):
    import os
    import datetime
    folder = 'data'

    # Make General data directory if it does not exist
    if not os.path.exists(folder):
        os.makedirs(folder)

    # Make Directory for this time slot if it does not exist
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)
    if firstchan == True:
        output = open(dirpath + "/rawOutput.csv", 'w+')  
    else:
        output = open(dirpath + "/rawOutput.csv", 'a')  

    #print('channel= %s FirstChan= %s output= %s' %(channel, firstchan, output))

    data = str(data).strip('[]')
    #data = str.replace(data, ',', '\n')  # switch to columns
    output.write(str(channel)+'\n')
    output.write(data)
    output.write('\n')
    output.close()


def convertstrflt(data):
    data = data.replace(',\n', '').split(',')
    flist = []
    for i in range(len(data)):
        try:
            data[i] = float(data[i])
        except ValueError:
            data.pop(i)

    # list(map(float, data))

    return data


d = Digitizer()


print sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6]
stgc = sys.argv[2]
PC = sys.argv[3]
AB = sys.argv[4]
gfz = sys.argv[5]
layer = sys.argv[6]
dirpath = 'data/' + sys.argv[1]

try:
    arduino = serial.Serial('/dev/ttyUSB0', 9600)
except serial.serialutil.SerialException:
    try:
        arduino = serial.Serial('/dev/ttyUSB1', 9600)
    except serial.serialutil.SerialException:
        try:
            arduino = serial.Serial('/dev/ttyUSB2', 9600)
        except serial.serialutil.SerialException:
            print "Arduino not connected"

time.sleep(2)
channel = 0
print('in Digitiser: %s' %(stgc))
firstchan=True
while (channel < 256):
    #print('In Digitizer channel = %s  first?= %s' %(channel,firstchan))
    if IsValidChannel(stgc, AB, int(layer), gfz, channel, PC):
        setchan.setChannel(str(channel), arduino)
        print channel
        data = d.gather()
        data = convertstrflt(data)
        data = data[0:50000] #?
        printData(dirpath, data, channel, firstchan)
        firstchan=False
        plt.close()
        arduino.write("+\n")
    channel += 1

finished()
