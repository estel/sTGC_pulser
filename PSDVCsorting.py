# Pulse shape determination + Variance comparison
import sys
import csv
import statistics
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from QABmap import *
from Beep import finished
#from Thresholds import getAmplitudeLowThreshold
from Thresholds import *

#def getAmplitudeLowThreshold(foldername):
#    if 'HVFILTER' in foldername:
#        if 'QS1' in foldername: return 0.005
#        if 'QS2' in foldername: return 0.025
#        if 'QS3' in foldername: return 0.03
#    else:
#        if 'QS1' in foldername: return 0.005
#        if 'QS2' in foldername: return 0.025
#        if 'QS3' in foldername: return 0.03
#    return -1



    
def get_sort_vars(data):
    chdct={}
    waveform=running_mean(data,1000)
    #print("length=",len(waveform))
    chdct['offset']=np.mean(waveform)
    chdct['vpp']=max(waveform)-min(waveform)
    #deriv=running_mean(np.diff(waveform),250)
    deriv=running_mean(np.diff(waveform),1000)
    #print("length deriv=",len(deriv))
    half=24000 # 5001
    chdct['x1']=int(np.argmax(deriv[:half]))
    chdct['x2']=int(np.argmin(deriv[half:]))+half
    #chdct['sort']=make_decision(chdct)
    #print('x1=',chdct['x1'], '  x2=',chdct['x2'])
    return chdct



outFile = open('data/'+sys.argv[1]+'/sortedpulserdata.csv', 'w+')    #Write to CSV for GFZ Mapping
outFileWriter = csv.writer(outFile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)

DatabaseFile = open('data/'+sys.argv[1]+'/database.csv', 'w+')   #Write to CSV for database file containing meta-data
DBwriter = csv.writer(DatabaseFile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
DBwriter.writerow(['Channel']+[' ']+['Pass/Fail']+[' ']+['Amplitude']+[' ']+['Sorting Function Response']+[' ']+['Mean']+[' ']+['Variance']+[' ']+['NPeaksRatio']+[' ']+['PosIntegralRatio']+[' ']+['ToT50']+[' ']+['ToT99'])

# give readable names to the arguments
makePlots = sys.argv[2]
stgc = sys.argv[3]
PC = sys.argv[4]
AB = sys.argv[5]
gfz = sys.argv[6]
layer = sys.argv[7]

## set thresholds depending on the quad and type S/P
## TODO: to be put in a separate configuration file

# Actually, it is very similar ofr strips and pads!
#respThl = 0.0075   #SFR threshold used to measure sgnal response
ampLowThl = 0.04
ampHighThl = 0.09    #SFR value to determine hot pad channels
respThl = 0.002   #need it so low for QS1-HV0: TODO: make it also dependant on the quad
intRatioThl = 0.075

ampLowThl = getAmplitudeLowThreshold(sys.argv[1])
ampHighThl = getAmplitudeHighThreshold(sys.argv[1])
respThl = getResponseThreshold(sys.argv[1])
intRatioThl = getIntegralRatioThreshold(sys.argv[1])
#if stgc=='QS1-HV0' : ampLowThl=0.005
#if stgc=='QS1-HV1' : ampLowThl=0.005 #0.015 for strip // 0.01 for pad // 0.005 for wire
#if stgc=='QS1' : ampLowThl=0.005
#if stgc=='QS2' : ampLowThl=0.025
#if stgc=='QS3' : ampLowThl=0.03


def running_mean(seq, window_size):
    return np.convolve(seq, np.ones((window_size,))/window_size, mode='valid')  #Averaging function to smooth the waveform


with open('data/'+sys.argv[1]+'/rawOutput.csv') as inputRawValues:
    rawReader = csv.reader(inputRawValues, delimiter=',', quotechar='|')    #read raw data from CSV
    channel = 0
    chan = []
    Vpp = {}
    integral = {}
    posintegral = {}
    negintegral = {}
    ratiointegral = {}
    tot50 = {}
    tot75 = {}
    tot90 = {}
    tot99 = {}
    testbookers = {}
    response = {}
    peaksratio = {}
    variances = {}
    averages = {}
    Result = {}
    for line in rawReader:
        if len(line) == 1:  # channel number line, only one element
            channel = int(line[0])  # get channel number
            #Only append if it is a valid channel (on QS1 you may have taken data for channels were you don't expect a signal
            if(IsValidChannel(stgc, AB, int(layer), gfz, channel, PC)):
                chan.append(channel)
            print('channel = %s' %(channel))
        else:
            values = [float(i) for i in line]  # convert strings to floats
            if makePlots == '0':
                plt.plot(values)
                plt.xlabel('Time (arbitrary units)')
                plt.ylabel('Voltage (V)')
                plt.grid(True)
                plt.savefig('data/' + sys.argv[1] + '/channel' + str(channel) + 'Raw.png')
                plt.close()

            ##Try Weizmann sorting # makes it very slow
            #testbookers[channel]=get_sort_vars(values)
            #testbooker=get_sort_vars(values)

            Nvalues=len(values)
            Nbins=24
            NvaluesPerBin=int(Nvalues/(float(Nbins)))
            NvaluesForSmoothing=int(Nvalues*0.02) # 2 percent
            #print("Nvalues="+str(Nvalues)+"  NvaluesPerBin="+str(NvaluesPerBin)+" NvaluesForSmoothing="+str(NvaluesForSmoothing))
            raw_avg=statistics.mean(values)
            raw_means=[]
            for i in range(0, Nbins): #?
                raw_total = 0
                NumberOfPoints = NvaluesPerBin #? #If you change this number the value of the response function changes
                for j in range(NumberOfPoints*i, NumberOfPoints*(i+1)):   #
                    raw_total = raw_total+values[j]   #Running window for section means
                raw_average = raw_total/NumberOfPoints
                raw_means.append(raw_average)
                raw_ratio = abs(raw_average/raw_avg) #Pulse shape determination 
                raw_diff = abs(raw_average - raw_avg) #Pulse shape determination 
                raw_normdiff = raw_diff/((max(values)-min(values)) /2)
                #print("raw_ratio = %f  raw_normdiff = %f    raw_diff = %f    raw_average = %f "%(raw_ratio, raw_normdiff, raw_diff, raw_average)) 
            raw_varmean = np.var(raw_means)
            
            #print("raw_varmean = %f"%(raw_varmean))
 

            waveform = running_mean(values, NvaluesForSmoothing)
            ##waveform05 = running_mean(values, int(NvaluesForSmoothing*0.5))
            #waveform01 = running_mean(values, int(NvaluesForSmoothing*0.1))
            #waveform001 = running_mean(values, int(NvaluesForSmoothing*0.01))
            ##waveform2 = running_mean(values, NvaluesForSmoothing*2)
            #waveform5 = running_mean(values, NvaluesForSmoothing*5)
            #waveform10 = running_mean(values, NvaluesForSmoothing*10)
            Vpp1 = max(waveform) - min(waveform) #determine amplitude of the signal
            ##Vpp05 = max(waveform05) - min(waveform05) #determine amplitude of the signal
            #Vpp01 = max(waveform01) - min(waveform01) #determine amplitude of the signal
            #Vpp001 = max(waveform001) - min(waveform001) #determine amplitude of the signal
            ##Vpp2 = max(waveform2) - min(waveform2) #determine amplitude of the signal
            #Vpp5 = max(waveform5) - min(waveform5) #determine amplitude of the signal
            #Vpp10 = max(waveform10) - min(waveform10) #determine amplitude of the signal
            ##print("Vpp01= %f  Vpp05= %f  Vpp1 = %f Vpp2 = %f  Vpp5 = %f"%(Vpp01, Vpp05, Vpp1, Vpp2, Vpp5))
            ##print("Vpp001= %f  Vpp01= %f  Vpp1 = %f Vpp5 = %f  Vpp10 = %f"%(Vpp001, Vpp01, Vpp1, Vpp5, Vpp10))
            ##waveform = running_mean(values, 1000)
            Vpp[channel] = max(waveform) - min(waveform) #determine amplitude of the signal
            peaks = 0
            noise = 0
            if 1: # makePlots == '0':
                plt.plot(waveform)  #Generate the plot of the smoothed waveform
                plt.xlabel('Time (arbitrary units)')
                plt.ylabel('Voltage (V)')
                plt.grid(True)
                plt.savefig('data/' + sys.argv[1] + '/channel' + str(channel) + 'Smoothed.png')
                plt.close()
            # project waveform on the Y axis (Voltage). Histogram the voltage of each point.
            # Projected histograms inx and y
            #if makePlots == '0':
            if 0 : # comment out projection
                num_bins = 50
                n, bins, patches = plt.hist(waveform, num_bins, facecolor='blue', alpha=0.5)
                plt.xlabel('Voltage [V]')
                plt.ylabel('Projection histogram in Y') 
                plt.savefig('data/' + sys.argv[1] + '/channel' + str(channel) + 'ProjX.png')
                plt.close()


            if  1: #makePlots == '0':
                minwf=min(waveform)
                maxwf=max(waveform)
                threshold50 = minwf + (maxwf - minwf)*0.50
                threshold75 = minwf + (maxwf - minwf)*0.75
                threshold90 = minwf + (maxwf - minwf)*0.90
                threshold99 = minwf + (maxwf - minwf)*0.99
                totalwf = sum(i >= minwf for i in waveform)

                tot50[channel] = float( sum(i > threshold50 for i in waveform) )/ totalwf   
                tot75[channel] = float( sum(i > threshold75 for i in waveform) )/ totalwf 
                tot90[channel] = float( sum(i > threshold90 for i in waveform) )/ totalwf 
                tot99[channel] = float( sum(i > threshold99 for i in waveform) )/ totalwf 
                integ=0.
                posinteg=0.
                neginteg=0.
                for i in waveform :
                    if i > threshold50 :
                        integ += i
                        posinteg += (i-threshold50)
                    if i < threshold50 :
                        neginteg += (threshold50 - i)
                integral[channel] = integ / float(sum( i > threshold50 for i in waveform)) / Vpp[channel]
                posintegral[channel] = posinteg / float(sum( i > threshold50 for i in waveform)) / (maxwf - threshold50)
                negintegral[channel] = neginteg / float(sum( i < threshold50 for i in waveform)) / (threshold50 - minwf)
                ratiointegral[channel] = posintegral[channel]/negintegral[channel]
                #print("integ=",integ," integral[channel] = ",integral[channel] ," tot50[channel]=",tot50[channel] ," totalwf=",totalwf )
                #print('minwf=',minwf, ' maxwf=',maxwf, ' thld50=',threshold50, ' thl75=',threshold75, ' thl90=',threshold90, ' thl99=', threshold99 )
                #print('totalwf=', totalwf, ' tot50=',tot50, ' tot75=', tot75, ' tot90=', tot90, 'tot99=', tot99)
                #print(testbooker['x1'],'  ',testbooker['x2'],'  ',testbooker['offset'])
            #    plt.hist(testbooker['x1'], 1000, facecolor='red', alpha=0.5)
            #    plt.xlabel('x1 ')
            #    plt.ylabel('?')
            #    plt.savefig('data/' + sys.argv[1] + '/channel' + str(channel) + 'X1.png')
            #    plt.close()


            avg = statistics.mean(waveform) #MOVED INITIAL AVERAGE AND VAIRANCE CALCULATION AFTER THE SMOOTHING
            var = np.var(waveform)
            variances[channel] = var
            averages[channel] = avg
            means = []
            smoothed_NvaluesPerBin=int(len(waveform)/(float(Nbins)))
            for i in range(0, Nbins): #?
                total = 0
                NumberOfPoints = smoothed_NvaluesPerBin #? #If you change this number the value of the response function changes
                for j in range(NumberOfPoints*i, NumberOfPoints*(i+1)):   #
                    total = total+waveform[j]   #Running window for section means
                average = total/NumberOfPoints
                means.append(average)
                ratio = abs(average/avg) #Pulse shape determination 
                diff = abs(average - avg) #Pulse shape determination 
                normdiff = diff/(Vpp[channel] /2)
                #print("ratio = %f normdiff = %f   diff = %f"%(ratio, normdiff,diff)) 
                #print("ratio = %f  normdiff = %f    diff = %f    average = %f "%(ratio, normdiff, diff, average)) 
                # original
                #if 1.25 > ratio > 0.75:
                #    noise += 1
                #elif ratio > 1.25 or ratio < 0.75:
                #    peaks += 1
 
                # I think this is what they meant
                if normdiff < 0.25:
                    noise += 1
                elif normdiff >= 0.25:
                    peaks += 1

                #print('channel %s i= %s average= %s ratio= %s noise = %s peaks = %s' %(channel, i, average, ratio, noise, peaks))
            varmean = np.var(means)
            #TODO response[channel] = 5*varmean/Vpp[channel] #SFR or Quality Factor
            #response[channel] = raw_varmean #SFR or Quality Factor
            response[channel] = raw_varmean/Vpp[channel] #SFR or Quality Factor
            print("raw_varmean = %f   raw_varmean/vpp = %f"%(raw_varmean, response[channel] ))
            #print('CHANNEL %s  varmean= %s Vpp= %s response = %s' %(channel, varmean, Vpp[channel], response[channel]))
            peaksratio[channel]=float(peaks)/12.0
            #if peaks > noise:
            if 1:
                if response[channel] >= respThl:
                    if Vpp[channel] < ampLowThl:
                        outFileWriter.writerow([int(channel)]+[2])                    
                        Result[channel] = 'FAIL' # Should label these cases as FAIL ??
                    elif  IsWire(stgc, AB, int(layer), gfz, channel, PC) and posintegral[channel]*0.1 < intRatioThl :
                        outFileWriter.writerow([int(channel)]+[5])
                        Result[channel] = 'FAIL'
                    elif ampLowThl <= Vpp[channel] <= ampHighThl:
                        #print(IsWire(stgc, AB, int(layer), gfz, channel, PC))
                        #if  IsWire(stgc, AB, int(layer), gfz, channel, PC):
                        #    print(posintegral[channel])
                        #else :
                        outFileWriter.writerow([int(channel)]+[3])
                        Result[channel] = 'PASS'
                    elif Vpp[channel] > ampHighThl:
                        outFileWriter.writerow([int(channel)]+[4])
                        Result[channel] = 'PASS'

                elif response[channel] < respThl:
                    if Vpp[channel] < ampLowThl:
                        outFileWriter.writerow([int(channel)]+[0])                    
                        Result[channel] = 'FAIL' # Bad shape low amp
                    else :  
                        outFileWriter.writerow([int(channel)]+[1]) # bad shape norm amp
                        Result[channel] = 'FAIL'
                else:
                    outFileWriter.writerow([int(channel)]+[1])
                    Result[channel] = 'FAIL'
            elif noise > peaks: # never happens
                if Vpp[channel] >= ampHighThl:
                    outFileWriter.writerow([int(channel)]+[1])
                    Result[channel] = 'FAIL'
                elif response[channel] <= respThl:
                    outFileWriter.writerow([int(channel)]+[0])
                    print('channel', channel, 'failed')
                    Result[channel] = 'FAIL'
                else:
                    outFileWriter.writerow([int(channel)]+[1])
                    Result[channel] = 'FAIL'
            else: 
                outFileWriter.writerow([int(channel)]+[1])
                Result[channel] = 'FAIL'

    #Cut repeadted data to use only latest version #Actually now if it is run twice the output file is replaced instead of appended, so this should not be needed.
    NChannels=0
    if AB == 'S' : 
        NChannels = NStrips(stgc, int(layer), gfz, PC)
    elif AB == 'P' : 
        NChannels = NPads(stgc, int(layer), PC) + NWires(stgc, int(layer), PC)
    print("Nchannels = %s   len chan = %s" %(NChannels, len(chan)))
    if len(chan) > NChannels:
        chan = chan[-NChannels:]

    xs = []
    Amplitude = []
    Response = []
    Variance = []
    AmplLowThl = []
    RespLowThl = []
    IntegRatioThl = []
    PeaksRatio = []
    #MaxDer = []
    #MinDer = []
    #Offset = []
    Integral = []
    PosIntegral = []
    NegIntegral = []
    RatioIntegral = []
    ToT50 = []
    ToT75 = []
    ToT90 = []
    ToT99 = []

    # loop over all channels and fill with 0 the ones that are not valid. Avoid having a line connecting pad and wire channels
    for channel in range(0,256):
        if  IsValidChannel(stgc, AB, int(layer), gfz, channel, PC) : #IsValidChannel(channel) :
            #Plot a line where you have set the threshold (filled area)  
            AmplLowThl.append(ampLowThl)
            RespLowThl.append(respThl*100)
            if  IsWire(stgc, AB, int(layer), gfz, channel, PC) : #IsValidChannel(channel) :
                IntegRatioThl.append(intRatioThl)
            else:
                IntegRatioThl.append(0)
           
            if channel in chan:
                #DBwriter.writerow(['Channel']+[' ']+['Pass/Fail']+[' ']+['Amplitude']+[' ']+['Sorting Function Response']+[' ']+['Mean']+[' ']+['Variance']+[' ']+['NPeaksRatio']+[' ']+['IntegralRatio']+[' ']+['ToT50']+[' ']+['ToT99'])

                DBwriter.writerow([channel]+[' ']+[Result[channel]]+[' ']+[Vpp[channel]]+[' ']+[response[channel]]+[' ']+[averages[channel]]+[' ']+[variances[channel]]+[' ']+[peaksratio[channel]]+[' ']+[posintegral[channel]]+[' ']+[tot50[channel]]+[' ']+[tot99[channel]])
                xs.append(channel)
                Amplitude.append(Vpp[channel])
                Response.append(response[channel]) 
                #Response.append(response[channel]*100) 
                Variance.append(variances[channel]*100) 
                PeaksRatio.append(peaksratio[channel]*0.1)
                #MaxDer.append((testbookers[channel])['x1']*0.1/48000)
                #MinDer.append((testbookers[channel])['x2']*0.1/48000)
                #Offset.append((testbookers[channel])['offset']*10)
                Integral.append(integral[channel]*0.1)
                PosIntegral.append(posintegral[channel]*0.1)
                NegIntegral.append(negintegral[channel]*0.1)
                RatioIntegral.append(ratiointegral[channel]*0.1)
                ToT50.append(tot50[channel]*0.1/0.5)
                ToT75.append(tot75[channel]*0.1/0.3)
                ToT90.append(tot90[channel]*0.1/0.15)
                ToT99.append(tot99[channel]*0.1/0.05)
 
            else:
                xs.append(channel)
                Amplitude.append(0)
                Response.append(0) 
                Variance.append(0) 
                PeaksRatio.append(0)
                #MaxDer.append(0) 
                #MinDer.append(0) 
                #Offset.append(0)
                Integral.append(0)
                PosIntegral.append(0)
                NegIntegral.append(0)
                RatioIntegral.append(0)
                ToT50.append(0)
                ToT75.append(0)
                ToT90.append(0)
                ToT99.append(0)

        else:
            AmplLowThl.append(0)
            RespLowThl.append(0)
            IntegRatioThl.append(0)
            xs.append(channel)
            Amplitude.append(0)
            Response.append(0) 
            Variance.append(0) 
            PeaksRatio.append(0)
            #MaxDer.append(0) 
            #MinDer.append(0) 
            #Offset.append(0)
            Integral.append(0)
            PosIntegral.append(0)
            NegIntegral.append(0)
            RatioIntegral.append(0)
            ToT50.append(0)
            ToT75.append(0)
            ToT90.append(0)
            ToT99.append(0)

    print("%s   %s" %(len(AmplLowThl),len(xs)))
    #Create amplitude distribution plot
    fig, ax = plt.subplots()
    #Plot threshold areas for reference

    #plt.plot(xs, AmplLowThl, label='Active channels - Amplitude Threshold' ,color='white',linewidth=0)
    #plt.plot(xs, RespLowThl, label='Active channels - Response Threshold' ,color='white',linewidth=0)
    plt.plot(xs, AmplLowThl, color='white',linewidth=0)
    plt.plot(xs, RespLowThl, color='white',linewidth=0)
    plt.plot(xs, IntegRatioThl, color='white',linewidth=0)
    #Plot measured amplitudes etc.
    plt.plot(xs, Response, label='Sorting Function Response',color='green') # plot it first so that it does not cover the amplitude
    plt.plot(xs, Amplitude, label='Signal Amplitude')
    #plt.plot(xs, Variance, label='Signal Variance')
    #plt.plot(xs, PeaksRatio, label='NPeaks/NBins ratio')

    #plt.plot(xs, MaxDer, label='Maximum Derivative')
    #plt.plot(xs, MinDer, label='Minimum Derivative')
    #plt.plot(xs, Offset, label='Offset')
    #plt.plot(xs, Integral, label='Integral above 50% [norm. to ToT50%*Vpp]')
    plt.plot(xs, PosIntegral, label='Positive Integral above 50% [norm. to ToT50%*Vpp/2]', color='red')
    #plt.plot(xs, NegIntegral, label='Negative Integral below 50% [norm. to ToT50%*Vpp/2]')
    #plt.plot(xs, RatioIntegral, label='Pos/Neg Integral ratio [norm. to ToT50%*Vpp/2]')
    #plt.plot(xs, ToT50, label='ToT 50% [norm. to 0.5]')
    #plt.plot(xs, ToT75, label='ToT 75% [norm. to 0.3]')
    #plt.plot(xs, ToT90, label='ToT 90% [norm. to 0.15]')
    #plt.plot(xs, ToT99, label='ToT 99% [norm. to 0.05]')

    #plt.ylim(top=0.2) 
    plt.ylim(top=0.3) 
    plt.fill_between(xs,AmplLowThl, where=np.array(AmplLowThl)>0,alpha=0.2,facecolor='blue')
    plt.fill_between(xs,RespLowThl, where=np.array(RespLowThl)>0,alpha=0.2,facecolor='green')
    plt.fill_between(xs,IntegRatioThl, where=np.array(IntegRatioThl)>0,alpha=0.2,facecolor='red')
    plt.xlabel('Readout Element (GFZ Channel Number)')
    plt.ylabel('Voltage (V)')
    plt.legend(loc='upper left')
    plt.grid(True)
    plt.gca().set_ylim(bottom=0)
    # Don't allow the axis to be on top of your data
    ax.set_axisbelow(True)
    # Turn on the minor TICKS, which are required for the minor GRID
    ax.minorticks_on()
    # Customize the major grid
    ax.grid(which='major', linestyle='-', linewidth='0.5', color='silver')
    ## Customize the minor grid
    #ax.grid(which='minor', linestyle=':', linewidth='0.5', color='gray')

    #Save plot
    plt.savefig('data/' + sys.argv[1] + '/AmpDist.png')

    #Beep when finished
    finished()

    #Pop up the plot. #TODO comment this line out If used for offline reprocessing of the data
    if makePlots == '0':
        plt.show()


    if 0 : ### do not make the extra plots
        ######### China ###########
    
        #Create amplitude distribution plot
        fig, ax = plt.subplots()
    
        plt.plot(xs, AmplLowThl, color='white',linewidth=0)
        #Plot measured amplitudes etc.
        plt.plot(xs, Amplitude, label='Signal Amplitude')
    
        plt.plot(xs, ToT50, label='ToT 50% [norm. to 0.5]')
        plt.plot(xs, ToT75, label='ToT 75% [norm. to 0.3]')
        plt.plot(xs, ToT90, label='ToT 90% [norm. to 0.15]')
        plt.plot(xs, ToT99, label='ToT 99% [norm. to 0.05]')
    
        #plt.ylim(top=0.2) 
        plt.ylim(top=0.3) 
        plt.fill_between(xs,AmplLowThl, where=np.array(AmplLowThl)>0,alpha=0.2,facecolor='blue')
        #plt.fill_between(xs,RespLowThl, where=np.array(RespLowThl)>0,alpha=0.2,facecolor='red')
        plt.xlabel('Readout Element (GFZ Channel Number)')
        plt.ylabel('Voltage (V)')
        plt.legend(loc='upper left')
        plt.grid(True)
        plt.gca().set_ylim(bottom=0)
        # Don't allow the axis to be on top of your data
        ax.set_axisbelow(True)
        # Turn on the minor TICKS, which are required for the minor GRID
        ax.minorticks_on()
        # Customize the major grid
        ax.grid(which='major', linestyle='-', linewidth='0.5', color='silver')
        ## Customize the minor grid
        #ax.grid(which='minor', linestyle=':', linewidth='0.5', color='gray')
    
        #Save plot
        plt.savefig('data/' + sys.argv[1] + '/AmpDistChina.png')
    
        #Beep when finished
    #    finished()
    
        #Pop up the plot. #TODO comment this line out If used for offline reprocessing of the data
    #    plt.show()
    
    
    ######### Israel ###########
    
        #Create amplitude distribution plot
        fig, ax = plt.subplots()
    
        plt.plot(xs, AmplLowThl, color='white',linewidth=0)
        #Plot measured amplitudes etc.
        plt.plot(xs, Amplitude, label='Signal Amplitude')
        #plt.plot(xs, MaxDer, label='Maximum Derivative')
        #plt.plot(xs, MinDer, label='Minimum Derivative')
        #plt.plot(xs, Offset, label='Offset')
        plt.plot(xs, Integral, label='Integral above 50% [norm. to ToT50%*Vpp]')
     
        plt.ylim(top=0.2) 
        plt.fill_between(xs,AmplLowThl, where=np.array(AmplLowThl)>0,alpha=0.2,facecolor='blue')
        plt.xlabel('Readout Element (GFZ Channel Number)')
        plt.ylabel('Voltage (V)')
        plt.legend(loc='upper left')
        plt.grid(True)
        plt.gca().set_ylim(bottom=0)
        # Don't allow the axis to be on top of your data
        ax.set_axisbelow(True)
        # Turn on the minor TICKS, which are required for the minor GRID
        ax.minorticks_on()
        # Customize the major grid
        ax.grid(which='major', linestyle='-', linewidth='0.5', color='silver')
        ## Customize the minor grid
        #ax.grid(which='minor', linestyle=':', linewidth='0.5', color='gray')
    
        #Save plot
        plt.savefig('data/' + sys.argv[1] + '/AmpDistIsrael.png')
    
        #Beep when finished
        #finished()
    
        #Pop up the plot. #TODO comment this line out If used for offline reprocessing of the data
    #    plt.show()
    
    
    ######### Canada ###########
    
        #Create amplitude distribution plot
        fig, ax = plt.subplots()
    
        plt.plot(xs, AmplLowThl, color='white',linewidth=0)
        plt.plot(xs, RespLowThl, color='white',linewidth=0)
        #Plot measured amplitudes etc.
        plt.plot(xs, Amplitude, label='Signal Amplitude')
        plt.plot(xs, Variance, label='Signal Variance')
        plt.plot(xs, Response, label='Sorting Function Response')
        plt.ylim(top=0.2) 
        plt.fill_between(xs,AmplLowThl, where=np.array(AmplLowThl)>0,alpha=0.2,facecolor='blue')
        #plt.fill_between(xs,RespLowThl, where=np.array(RespLowThl)>0,alpha=0.2,facecolor='red')
        plt.xlabel('Readout Element (GFZ Channel Number)')
        plt.ylabel('Voltage (V)')
        plt.legend(loc='upper left')
        plt.grid(True)
        plt.gca().set_ylim(bottom=0)
        # Don't allow the axis to be on top of your data
        ax.set_axisbelow(True)
        # Turn on the minor TICKS, which are required for the minor GRID
        ax.minorticks_on()
        # Customize the major grid
        ax.grid(which='major', linestyle='-', linewidth='0.5', color='silver')
        ## Customize the minor grid
        #ax.grid(which='minor', linestyle=':', linewidth='0.5', color='gray')
    
        #Save plot
        plt.savefig('data/' + sys.argv[1] + '/AmpDistCanada.png')
    
        #Beep when finished
        #finished()
    
        #Pop up the plot. #TODO comment this line out If used for offline reprocessing of the data
        plt.show()
    





    df = pd.DataFrame()
    df['Amplitude']=Amplitude
    df['Variance']=Variance
    df['Response']=Response
    #df['Offset']=Offset
    #df['MaxDer']=MaxDer
    #df['MinDer']=MinDer
    df['PosIntRatio']=PosIntegral
    df['ToT50']=ToT50
    df['ToT75']=ToT75
    df['ToT90']=ToT90
    df['ToT99']=ToT99
    
    #df.insert(0, 'GFZ', gfz)
    #df.insert(0,'Layer',layer)
    #df.insert(0,'Quad',quadUniqueID)
    #df.insert(0,'Test','FAIL')
    
    #df = df.sort_values(by=['Number'])
    print(df.head())
    if len( ( df[df['Amplitude']<getAmplitudeLowThreshold(sys.argv[1])] ).index) :
        df_badchannels = df[df['Amplitude']<getAmplitudeLowThreshold(sys.argv[1])]
        print(df_badchannels.to_string(formatters={"Amplitude": "{:,.6f}".format}))
        print(df.mean(axis=0))
        df_precision = df_badchannels.to_string(formatters={"Amplitude": "{:,.6f}".format, "Variance": '{:,.2f}'.format})
    





