def getAmplitudeLowThreshold(foldername):
    if 'HVFILTER' in foldername:
        if 'QS1' in foldername and 'HV0' in foldername: return 0.0037
        if 'QS1' in foldername and 'HV1' in foldername: return 0.005
        if 'QS2' in foldername: return 0.008
        if 'QS3' in foldername: return 0.012
    else:
        if 'QS1' in foldername and 'HV0' in foldername: return 0.004 
        if 'QS1' in foldername and 'HV1' in foldername: return 0.008 
        if 'QS1' in foldername: return 0.004 
        if 'QS2' in foldername: return 0.02
        if 'QS3' in foldername: return 0.02 
        if 'QL1' in foldername: return 0.0065 #to be optimised 
        if 'QL2' in foldername: return 0.025 #to be optimised
        if 'QL3' in foldername: return 0.03 #to be optimised
    return -1

def getAmplitudeHighThreshold(foldername):
    return 0.098

def getResponseThreshold(foldername):
    if 'QS1' in foldername: return 0.0001 
    if 'QS2' in foldername: return 0.0001
    if 'QS3' in foldername: return 0.0001 
    if 'QL1' in foldername: return 0.0001
    if 'QL2' in foldername: return 0.0001
    if 'QL3' in foldername: return 0.0001
    return -1

def getIntegralRatioThreshold(foldername):
    if 'QS1' in foldername: return 0.075 
    if 'QS2' in foldername: return 0.075
    if 'QS3' in foldername: return 0.075 
    if 'QL1' in foldername: return 0.075
    if 'QL2' in foldername: return 0.075
    if 'QL3' in foldername: return 0.075
    return -1



