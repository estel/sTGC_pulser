'''
this file generates a GUI corresponding to a graphical illustration of the channels 
which are double-tested near the HV1 and HV0 divide. 
after the user specifies quad details on the GUI, clicks RUN, then closes the window, a graph will pop up. 
the graph will plot the channel amplitudes for both HV1 and HV0 runs, 
and will highlight the highest amplitudes for all the channels that were tested in both runs.
'''

import os
import sys
import csv
import statistics
import numpy as np
import matplotlib.pyplot as plt
from QABmap import getQuanUniqueID
from QABmap import getFolderUniqueName
#from Beep import finished
from matplotlib.ticker import FormatStrFormatter
from foldernameDB import getFoldersList
from QABmap import Qmap
from tkMessageBox import showinfo
import Tkinter as tk
from tabulate import tabulate

window = tk.Tk() #window is an instance of tkinter's Tk class
window.geometry('560x760')
window.title('Overlapping Channels')

def show_values():
	stgc.get()
	PC.get()
	layer.get()
	uid.get()
	window.destroy()
	AB.get()
	gfz.get()

#frame 1 (var=stgc)
frm1 = tk.Frame(master=window)
frm1.grid(padx=100, pady=20)
lbl_1 = tk.Label(master=frm1, text='Quad Type:')

stgc = tk.StringVar()

bttn_ql1 = tk.Radiobutton(master=frm1,text='QL1', variable=stgc, value='QL1')
bttn_qs1 = tk.Radiobutton(master=frm1, text='QS1', variable=stgc, value='QS1')


lbl_1.grid(row=0,column=0)
bttn_ql1.grid(row=0,column=1)
bttn_qs1.grid(row=0,column=2)

#frame 2 (var=PC)
frm2 = tk.Frame(master=window)
frm2.grid(padx=150, pady=20)

PC = tk.StringVar()

lbl_2 = tk.Label(master=frm2, text='Pivot or Confirm:')
bttn_piv = tk.Radiobutton(master=frm2, text='Pivot', variable=PC, value='PIVOT')
bttn_conf = tk.Radiobutton(master=frm2, text='Confirm', variable=PC, value='CONFIRM')

lbl_2.grid(row=1, column=0)
bttn_piv.grid(row=1, column=1)
bttn_conf.grid(row=1, column=2)

#frame 3 (var=uid)
frm3 = tk.Frame(master=window)
frm3.grid(padx=150, pady=20)

uid = tk.StringVar()

lbl_3 = tk.Label(master=frm3, text='Unique Identifier:')
ent_uid = tk.Entry(master=frm3, textvariable=uid)

lbl_3.grid(row=2, column=0)
ent_uid.grid(row=2,column=1)

#frame 4 (var=layer)
frm4 = tk.Frame(master=window)
frm4.grid(padx=100, pady=20)

layer = tk.StringVar()

lbl_4 = tk.Label(master=frm4, text='Layer:')
bttn_lay1 = tk.Radiobutton(master=frm4, text='1', variable=layer, value='1')
bttn_lay2 = tk.Radiobutton(master=frm4, text='2', variable=layer, value='2')
bttn_lay3 = tk.Radiobutton(master=frm4, text='3', variable=layer, value='3')
bttn_lay4 = tk.Radiobutton(master=frm4, text='4', variable=layer, value='4')

lbl_4.grid(row=3,column=0)
bttn_lay1.grid(row=3,column=1)
bttn_lay2.grid(row=3,column=2)
bttn_lay3.grid(row=3,column=3)
bttn_lay4.grid(row=3, column=4)

#frame 5 (var=AB)
frm5 = tk.Frame(master=window)
frm5.grid(padx=100, pady=20)

AB = tk.StringVar()

lbl_5 = tk.Label(master=frm5, text='Adapter Board:')
bttn_strip = tk.Radiobutton(master=frm5, text='Strip', variable=AB, value='S')
bttn_pad = tk.Radiobutton(master=frm5, text='Pad', variable=AB, value='P')

lbl_5.grid(row=4, column=0)
bttn_strip.grid(row=4, column=1)
bttn_pad.grid(row=4, column=2)

#frame 6 (var=gfz)
frm6 = tk.Frame(master=window)
frm6.grid(padx=100, pady=20)

gfz = tk.StringVar()

lbl_6 = tk.Label(master=frm6, text='GFZ Number:')
bttn_p1 = tk.Radiobutton(master=frm6, text='P1', variable=gfz, value='P1')
bttn_p2 = tk.Radiobutton(master=frm6, text='P2', variable=gfz, value='P2')

lbl_6.grid(row=5, column=0)
bttn_p1.grid(row=5, column=1)
bttn_p2.grid(row=5, column=2)

#frame 7
frm7 = tk.Frame(master=window)
frm7.grid(padx=20, pady=20)

bttn_runOC = tk.Button(master=frm7, text='RUN overlapping channels', command=show_values)

bttn_runOC.grid(row=10, column=0)

#frame 8, showing overlap options
frm8 = tk.Frame(master=window)
frm8.grid(padx=90, pady=20)

lbl_8a = tk.Label(master=frm8, text='Table of combos with two HV runs')
lbl_8c = tk.Label(master=frm8, text='*Note that not all combos with two HV runs have overlapping channels')
lbl_8b = tk.Label(master=frm8, text='O = overlap          - = no overlap')

lbl_8a.grid(row=6, column=0)
lbl_8c.grid(row=7, column=0)
lbl_8b.grid(row=8, column=0)


#frame 9
frm9 = tk.Frame(master=window)
frm9.grid(padx=20, pady=20)

piv = [		['layer', 'SP1', 'SP2', 'PAD'], 
		[1,'-', 'O', 'O'], 
		[2, '-', 'O', '  O'], 
		[3, '-', 'O', '  O'],
		[4, 'O' , '-', '  O']]

con = [		['layer', 'SP1', 'SP2', 'PAD'], 
		[1, 'O', '-', '  O'],
		[2, '-', 'O', '  O'], 
		[3, '-', 'O', '  O'], 
		[4, '-', 'O', '  O']]

pivLabel = tk.Label(master=frm9, text='QS1/QL1 PIVOT', padx=50)
pivTable = tk.Label(master=frm9, text=tabulate(piv))
conLabel = tk.Label(master=frm9, text='    QS1/QL1 CONFIRM', padx=50)
conTable = tk.Label(master=frm9, text=tabulate(con))

pivLabel.grid(row=9, column=0)
conLabel.grid(row=9, column=1)
pivTable.grid(row=10, column=0)
conTable.grid(row=10, column=1)

window.mainloop() #makes this into an 'event loop'. program won't run again until tk window is closed.

#********************************
#OVERLAPPING CHANNELS CODE BELOW
#********************************

layer=int(layer.get())
AB=AB.get()
gfz=gfz.get()
channel=0
PC = PC.get()
stgc=stgc.get()
uid=str('-' + uid.get() +'_')
print(layer, AB, gfz, PC, stgc, uid)
#quit()
'''
layer=2
AB='S'
gfz='P2'
channel=0
PC = 'PIVOT'
stgc='QL1'
uid='-9_'
'''
def channel_overlap(layer, AB, stgc):
	folderList=getFoldersList( "" , stgc , layer, AB , gfz, "")
	
	if len(folderList) == 0:
		print('There is no data attributed with that Quad name.')
		quit()
	
	#sort through pivots and confirms
	quadChoice = []

	if PC == 'P':
		for folder in folderList:
			if 'PIVOT' in folder:
				quadChoice.append(folder)
	else:
		for folder in folderList:
			if 'CONFIRM' in folder:
				quadChoice.append(folder)

	#check to see if each HV1 has an HV0 

	both_hvs = []
	quadName = PC + uid
	for folder in folderList:
		if quadName in folder:
			both_hvs.append(folder)
	
	if len(both_hvs) == 1:
		print('There is only one HV run for this layer: ', both_hvs)
		quit()

	#sort through hv0 and hv1
	hv0 = []
	hv1 = []
	for hv_file in both_hvs:
		if 'HV0' in hv_file:
			hv0.append(hv_file)
		else:
			hv1.append(hv_file)
	
	
	#make the file name into an openable file path
	for quadFile in hv0:
		quadFile = 'data/' + str(quadFile) + '/database.csv'
		hv0 = np.loadtxt(str(quadFile), delimiter = ', ,', skiprows=1, usecols=(0,2))
	
	for i in hv1:
		i = 'data/' + str(i) + '/database.csv'
		hv1 = np.loadtxt(str(i), delimiter = ', ,', skiprows=1, usecols=(0,2))

	#find the channel numbers that are tested on both hv1 and hv0 runs; dubbed the 'overlapping' channels.
	overlapping_channels = []
	#check to see if one of the hv runs only ran 1 channel...
	if len(hv0) == 2:
		for i in hv1[:,0]:
			#...because the following line must be modified from hv0[:0] to hv0[:,0] for a column containing multiple data points
			for j in hv0[:0]:
				if i == j:
					overlapping_channels.append(i)
	elif len(hv1) == 2:
		for i in hv1[:0]:
			for j in hv0[:,0]:
				if i == j:
					overlapping_channels.append(i)
	else:
	#iterate through all the channel numbers in hv1 run
		for i in hv1[:,0]:
		#iterate through all the channel numbers in hv0 run
			for j in hv0[:,0]:
			#if a channel number is present in both runs
				if i == j:
				#append it to the overlapping channels list
					overlapping_channels.append(i)
	if len(overlapping_channels) == 0:
		print('There are no overlapping channels for this layer :(')	
		quit()

	#compare the amplitudes of the overlapping channels in both of the hv runs, and plot only the highest ones

	overlap_0 = []
	overlap_1 = []
	#iterate through hv0 and overlapping_channels
	for i in range(len(hv0)):
		for j in range(len(overlapping_channels)):
			#if a channel is an overlapping channel,
			if hv0[i][0] == overlapping_channels[j]:
				#then append that channel's corresponding amplitude to a list
				overlap_0.append(hv0[i][1])

	#repeat same procedure for hv1
	for i in range(len(hv1)):
		for j in range(len(overlapping_channels)):
			if hv1[i][0] == overlapping_channels[j]:
				overlap_1.append(hv1[i][1])

	#take only the highest amplitude of a given overlapping channel

	highest_of_overlapping_amps = []
	#iterate over both the hv1 and hv0 lists of overlapping channels
	for i in range(len(overlapping_channels)):
		#if a value is higher in the hv0 overlapping list, append it to the highest amplitude list
		if overlap_0[i] >= overlap_1[i]:
			highest_of_overlapping_amps.append(overlap_0[i])
		#else if a value is higher in the hv1 overlapping list, append it instead
		else:
			highest_of_overlapping_amps.append(overlap_1[i])

	plt.plot(hv0[:,0], hv0[:,1], color='cadetblue', label='HV0')
	if AB == 'PAD':
		if stgc == 'QL1':
			if PC == 'CONFIRM':
				if layer == 1:
					#QL1 C L1
					plt.plot(hv1[0:72,0], hv1[0:72,1], color='slateblue', label='HV1')
				elif layer == 2:
					#QL1 C L2
					plt.plot(hv1[0:64,0], hv1[0:64,1], color='slateblue', label='HV1')
				elif layer == 3:
					#QL1 C L3
					plt.plot(hv1[0:66,0], hv1[0:66,1], color='slateblue', label='HV1')
				elif layer == 4:
					#QL1 C L4
					plt.plot(hv1[0:59,0], hv1[0:59,1], color='slateblue', label='HV1')
			else:
				if layer == 1:
					#QL1 P L1
					plt.plot(hv1[0:77,0], hv1[0:77,1], color='slateblue', label='HV1')
				#there's no L2 combo with overlapping channels
				elif layer == 3:
					#QL1 P L3
					plt.plot(hv1[0:77,0], hv1[0:77,1], color='slateblue', label='HV1')
				elif layer == 4:
					#QL1 P L4
					plt.plot(hv1[0:78,0], hv1[0:78,1], color='slateblue', label='HV1')
			
		else:
			if PC == 'CONFIRM':
				#QS1 C L3 & L1
				plt.plot(hv1[0:48,0], hv1[0:48,1], color='slateblue', label='HV1')
			else:
				if layer == 1:
					#QS1 P L1
					plt.plot(hv1[0:44,0], hv1[0:44,1], color='slateblue', label='HV1')
				elif layer == 4:
					#QS1 P L4
					plt.plot(hv1[0:36,0], hv1[0:36,1], color='slateblue', label='HV1')
	else:
		plt.plot(hv1[:,0], hv1[:,1], color='slateblue', label='HV1')
		
		
	plt.plot(overlapping_channels,highest_of_overlapping_amps, color='midnightblue', label='True Amp')
	plt.xlabel('Channel Number')
	plt.ylabel('Amplitude (V)')
	plt.legend(loc='upper right')
	plt.title(str(stgc)+'-'+str(PC)+uid)
	plt.axvline(overlapping_channels[0], ls='--', color='grey')
	plt.axvline(overlapping_channels[-1], ls='--', color='grey')
	plt.show()

channel_overlap((layer), AB, stgc)

