# Pulse shape determination + Variance comparison
import csv
import statistics
import numpy
import matplotlib.pyplot as plt

outFile = open('sortedpulserdata.csv', 'w')
outFileWriter = csv.writer(outFile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
varianceThreshold = 0.00003
#makePlots = '1'
makePlots = '0'
with open('rawOutput.csv') as inputRawValues:
    rawReader = csv.reader(inputRawValues, delimiter=',', quotechar='|')
    channel = 0
    chan = []
    for line in rawReader:
        if len(line) == 1:  # channel number line, only one element
            channel = int(line[0])  # get channel num
            chan.append(channel)
            if chan[0] != 0:
                high = 0.0005
            else:
                high = 0.0004
        else:
            line = [float(i) for i in line]  # convert strings to floats
            avg = statistics.mean(line)
            var = numpy.var(line)
            #print 'channel', channel, '; AVG:', avg, '; VAR:', var
            peaks = 0
            noise = 0
            if makePlots == '0':
                plt.plot(line)
                plt.savefig('channel' + str(channel) + '.png')
                plt.close()
            for i in range(0, 12):
                total = 0
                for j in range(4000*i, 4000*i+4000):
                    total = total+line[j]
                average = total/4000
                #print channel, i, average
                if average > avg-0.001 and average < avg+0.001:
                    noise += 1
                elif average > avg+0.002 or average < avg-0.002:
                    peaks += 1
            if peaks >= 10:
                #print channel, 'peaks', peaks
                if var > high:
                    outFileWriter.writerow([int(channel)]+[4])
                    print 'channel', channel, 'high variance:', var
                elif varianceThreshold < var < 0.00004:
                    outFileWriter.writerow([int(channel)]+[2])
                    print 'channel', channel, 'low vairance: ', var
                elif var > varianceThreshold:
                    outFileWriter.writerow([int(channel)]+[3])
                elif var < varianceThreshold:
                    outFileWriter.writerow([int(channel)]+[1])
            elif noise >= 10:
                #print channel, 'noise', noise
                if var > high:
                    outFileWriter.writerow([int(channel)]+[1])
                elif var < varianceThreshold:
                    outFileWriter.writerow([int(channel)]+[0])
