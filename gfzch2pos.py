import sys
import math
from QABmap import GFZChannel2Pin
from QABmap import Qmap
from QABmap import IsWire
from QABmap import getQuanUniqueID
from QABmap import getFolderUniqueName
from QABmap import IsValidChannel

print("arguments: stgc PC layer AB gfztype gfzchannel")

stgc=sys.argv[1]
PC=sys.argv[2]
layer=sys.argv[3]
AB=sys.argv[4]
gfz=sys.argv[5]
channel=sys.argv[6]


objectNumber=Qmap(int(layer), AB, gfz, int(channel), PC, stgc, 0) # 0:do not print out


QUAD=stgc
Number=objectNumber

if "S" in QUAD :
    wedgeangle_deg=8.5
elif "L" in QUAD :
    wedgeangle_deg=14

STRIPPITCH=3.2
WEDGEANGLE=wedgeangle_deg/360.*2.*math.pi

#print(WEDGEANGLE)


def NumberToPosition(AB, objectNumber):
    position = -999
    if AB=='S':
        position=(int(objectNumber)-9)*(STRIPPITCH/math.cos(WEDGEANGLE))+1.685
    return position

def NumberToPosition_QL3Par(AB, objectNumber):
    position = -999
    if AB=='S':
        position=(352-int(objectNumber)-9)*(STRIPPITCH)+1.685
    return position



if "QL3" in QUAD and int(Number)>170 :
    print ("for gfzchannel %s Strip number %s , its position to the AB edge ON THE LONG BASE is %s" %(channel, Number, NumberToPosition_QL3Par("S",Number)))
else :
    print ("for gfzchannel %s Strip number %s , its position to the AB edge is %s" %(channel, Number, NumberToPosition("S",Number)))



