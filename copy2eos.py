#pulser_test_CERN_data/QS1P/QS1.P.1/QS1.P.1_GasGap1_P_GFZP1_HV0/
import os, errno, sys, subprocess
from shutil import copyfile
from foldernameDB import getFoldersList
from QABmap import Layer2GasGap

option=""
if len(sys.argv) > 1 :
    option=sys.argv[1]

filesToBeCopiedList=['rawOutput.csv']

QS1Pids=["2","4","8","10","7","9"]
QS1Cids=["1","2","3","4"]
QS2Pids=["1","7","4","10","5","6","8"]
QS2Cids=["10","13","9","8","15","16"]
QS3Pids=["2","6","1","8","7","10","9","11","12"]
QS3Cids=["3","2","4"]

QL1Pids=[]
QL1Cids=[]
QL2Pids=["1","2"]
QL2Cids=["1"]
QL3Pids=["1","2","3","4"]
QL3Cids=[]

QS1Plist=[]
for qid in QS1Pids : QS1Plist.append("QS1.P."+qid)
QS1Clist=[]
for qid in QS1Cids : QS1Clist.append("QS1.C."+qid)
QS2Plist=[]
for qid in QS2Pids : QS2Plist.append("QS2.P."+qid)
QS2Clist=[]
for qid in QS2Cids : QS2Clist.append("QS2.C."+qid)
QS3Plist=[]
for qid in QS3Pids : QS3Plist.append("QS3.P."+qid)
QS3Clist=[]
for qid in QS3Cids : QS3Clist.append("QS3.C."+qid)
QL1Plist=[]
for qid in QL1Pids : QL1Plist.append("QL1.P."+qid)
QL1Clist=[]
for qid in QL1Cids : QL1Clist.append("QL1.C."+qid)
QL2Plist=[]
for qid in QL2Pids : QL2Plist.append("QL2.P."+qid)
QL2Clist=[]
for qid in QL2Cids : QL2Clist.append("QL2.C."+qid)
QL3Plist=[]
for qid in QL3Pids : QL3Plist.append("QL3.P."+qid)
QL3Clist=[]
for qid in QL3Cids : QL3Clist.append("QL3.C."+qid)


#quadIDList=QS1Plist+QS1Clist+QS2Plist+QS2Clist+QS3Plist+QS3Clist+QL1Plist+QL1Clist+QL2Plist+QL2Clist+QL3Plist+QL3Clist
#quadIDList=QS1Plist+QS1Clist+QS2Plist+QS2Clist+QS3Plist+QS3Clist # small only, for now and until we fix the labeling
#quadIDList=QS3Clist+QS3Plist+QS2Clist+QS2Plist+QS1Clist+QS1Plist
quadIDList=QL3Clist+QL3Plist+QL2Clist+QL2Plist+QL1Clist+QL1Plist
#quadIDList=["QS1.P.7"]
print quadIDList

#quadID="QS2.P.8"
for quadID in quadIDList :

    splitquadID=quadID.split(".")
    stgc=splitquadID[0]
    PC=splitquadID[1]
    quadType=stgc+PC
    
    # Loop over all files of this quad
    for layer in ['1','2','3','4'] :
        slayer='Layer'+layer
        gasgap=Layer2GasGap(int(layer), PC)
        for AB in ['P','S'] : 
            if AB == 'P' : GFZlist = ['P1']
            elif AB == 'S' : GFZlist = ['P1','P2']
            for GFZ in GFZlist : 
                if 'QS1' in quadID : 
                    HVlist=['HV1','HV0']
                else: 
                    HVlist=['']
                for HV in HVlist : 
                    if 'QS1' in quadID : 
                        stgc="QS1-"+HV
                    folderList=getFoldersList( quadID , '' , int(layer), AB, GFZ, HV) 
                    print len(folderList)
                    if len(folderList) == 1 :
                        for sourceFolder in folderList :
                            print "working on ... "+sourceFolder
                            sHV=""
                            if HV != "" : 
                                sHV="_"+HV
                            destinationFolder="../pulser_test_CERN_data/"+quadType+"/"+quadID+"/"+quadID+"_GasGap"+str(gasgap)+"_"+AB+"_GFZ"+GFZ+sHV+"/"
                            print "to be copied to "+destinationFolder
    
                            # if destination directory does not exist yet, create it
                            if not os.path.exists(destinationFolder):
                                os.makedirs(destinationFolder)
    
                            for fileToBeCopied in filesToBeCopiedList :
                                if "copy" in option :
                                    src="/home/userpulser/sTGC_pulser/data/"+sourceFolder+"/"+fileToBeCopied
                                    dst="/home/userpulser/sTGC_pulser/"+destinationFolder+fileToBeCopied
                                    #print "cp "+src+" "+dst
                                    #copyfile(src, dst)
                                    print "ln -s  "+src+" "+dst
                                    os.symlink(src, dst)
    
                                # use the same script to rerun on the data??
                                
                                if "rerun" in option:
                                    ## Redo the sorting with the new thresholds
                                    bashCommand = " python PSDVCsorting.py ../"+destinationFolder+" 1 "+stgc+" "+PC+" "+AB+" "+GFZ+" "+layer
                                    print(bashCommand)
                                    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
                                    output, error = process.communicate()
                                    print(output)
                                    print(error)
                                    
                                    gfzCommand = " python sortedchannelplot.py "+destinationFolder+" "+stgc+" "+PC+" "+AB+" "+GFZ+" "+layer
                                    print(gfzCommand)
                                    gfzprocess = subprocess.Popen(gfzCommand.split(), stdout=subprocess.PIPE)
                                    gfzoutput, gfzerror = gfzprocess.communicate()
                                    print(gfzoutput)
                                    print(gfzerror)
    
                                if "sync" in option:
                                    local="/home/userpulser/pulser_test_CERN_data/Q*"
                                    #print "rsync -vrL --progress "+local+" estel@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/det-nsw-stgc/pulser_test_data/pulser_test_CERN_data/."    
                                    sync_command="rsync -vrL --progress "+local+" estel@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/det-nsw-stgc/pulser_test_data/pulser_test_CERN_data/."    
                                    print sync_command
                                    process = subprocess.Popen(sync_command.split(), stdout=subprocess.PIPE)
                                    output, error = process.communicate()
                                    print(output)
                                    print(error)
    
                    else :
                        print "WARNING!len(folderList) = "+str(len(folderList))+"       -> skipping it for now!"
