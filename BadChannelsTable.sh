#usage ./BadChannelsTable.sh QUAD_ALIAS or WEDGE_NAME

if [ -z "$1" ]
  then
    echo "No argument supplied: please provide quad alias or wedge name"
else

  echo " python testCompare.py bad $1 | grep FAIL | sed 's/[0-9].* FAIL/ FAIL /g' | sed 's/ \+/\t| /g' | sed 's/$/ \t| \t| \t|/g' "
  python testCompare.py bad $1 | grep FAIL | sed 's/[0-9].* FAIL/ FAIL /g' | sed 's/ \+/\t| /g' | sed 's/$/ \t| \t| \t|/g'

  echo " python testCompare.py bad $1 | grep sTGC |  awk '{ if(length($3)!=0) {print \"eog data/\"$2\"/channel\"$3\"Smoothed.png  \& \"} }' "
  python testCompare.py bad $1 | grep sTGC |  awk '{ if(length($3)!=0) {print "eog data/"$2"/channel"$3"Smoothed.png & "} }' 


fi

