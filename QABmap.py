import QS3map
import QS2map
import QS1map
import QL3map
import QL2map
import QL1map


def Layer2GasGap(layer, PC):
    gasgap = layer
    if 'C' in PC:  # also in Large wedge??
        if layer == 1:
            gasgap = 4
        if layer == 2:
            gasgap = 3
        if layer == 3:
            gasgap = 2
        if layer == 4:
            gasgap = 1
    return gasgap


def fromFoldernameExtractQuadid(foldername):
    quad = ((foldername.split("_"))[3].split("-"))[0]
    num = ((foldername.split("_"))[4].split("-"))[1]
    pc = (((foldername.split("_"))[4].split("-"))[0])[:1]
    uniqueid = quad+"."+pc+"."+num
    return uniqueid


def getQuanUniqueID(MYfilename, stgc):
    quadUniqueID = ''

    if 'PIVOT' in MYfilename and 'WEDGE1' in MYfilename:
        if (stgc == 'QS3'):
            quadUniqueID = 'QS3_PIVOT-0002'
        if (stgc == 'QS2'):
            quadUniqueID = 'QS2_PIVOT-0001'
        if (stgc == 'QS1') or (stgc == 'QS1-HV0') or (stgc == 'QS1-HV1'):
            quadUniqueID = 'QS1_PIVOT-0002'

    elif 'PIVOT' in MYfilename and 'WEDGE2' in MYfilename:
        if (stgc == 'QS3'):
            quadUniqueID = 'QS3_PIVOT-0006'
        if (stgc == 'QS2'):
            quadUniqueID = 'QS2_PIVOT-0008'
        if (stgc == 'QS1') or (stgc == 'QS1-HV0') or (stgc == 'QS1-HV1'):
            quadUniqueID = 'QS1_PIVOT-0004'
    elif 'QS3_PIVOT-DISHY' in MYfilename:
        quadUniqueID = 'QS3.P.1'
    elif 'QS3_PIVOT-7' in MYfilename:
        quadUniqueID = 'QS3_PIVOT-7'
    elif 'QS3_PIVOT-8' in MYfilename:
        quadUniqueID = 'QS3_PIVOT-8'
    else:
        return fromFoldernameExtractQuadid(MYfilename)
    return quadUniqueID


def getFolderUniqueName(MYfilename, stgc, HV, layer, AB, gfz):
    quadUniqueID = getQuanUniqueID(MYfilename, stgc)
    return str(quadUniqueID+"_"+HV+"Layer"+str(layer)+"_"+AB+"_GFZ"+gfz)


def NumTotalStrips(stgc):
    if stgc == 'QS1':
        return 406
    if stgc == 'QS1-HV1':
        return 406
    if stgc == 'QS1-HV0':
        return 406
    elif stgc == 'QS2':
        return 365
    elif stgc == 'QS3':
        return 307
    elif stgc == 'QL1' or stgc == 'QL1-HV0' or stgc == 'QL1-HV1':
        return 408
    elif stgc == 'QL2':
        return 366
    elif stgc == 'QL3':
        return 353
    else:
        raise ValueError("Quad name: %s not valid!" % (stgc))


def NStrips(stgc, layer, gfz, PC):
    if stgc == 'QS3' and gfz == 'P1':
        return 256
    elif stgc == 'QS3' and gfz == 'P2':
        return 51
    elif stgc == 'QS2' and gfz == 'P1':
        return 256
    elif stgc == 'QS2' and gfz == 'P2':
        return 109
    elif stgc == 'QS1' and gfz == 'P1':
        if (layer == 1 or layer == 4):
            return NumTotalStrips(stgc) - 256
        if (layer == 2 or layer == 3):
            return 256
    elif stgc == 'QS1' and gfz == 'P2':
        if (layer == 2 or layer == 3):
            return NumTotalStrips(stgc)-256
        if (layer == 1 or layer == 4):
            return 256

    if PC == 'P':
        if stgc == 'QS1-HV1' and layer == 1 and gfz == 'P1':
            return 0
        if stgc == 'QS1-HV1' and layer == 1 and gfz == 'P2':
            return 253
        if stgc == 'QS1-HV1' and layer == 2 and gfz == 'P1':
            return 256
        if stgc == 'QS1-HV1' and layer == 2 and gfz == 'P2':
            return 6
        if stgc == 'QS1-HV1' and layer == 3 and gfz == 'P1':
            return 256
        if stgc == 'QS1-HV1' and layer == 3 and gfz == 'P2':
            return 14
        if stgc == 'QS1-HV1' and layer == 4 and gfz == 'P1':
            return 150
        if stgc == 'QS1-HV1' and layer == 4 and gfz == 'P2':
            return 256
        if stgc == 'QS1-HV0' and layer == 1 and gfz == 'P1':
            return 150
        if stgc == 'QS1-HV0' and layer == 1 and gfz == 'P2':
            return 3
        if stgc == 'QS1-HV0' and layer == 2 and gfz == 'P1':
            return 256
        if stgc == 'QS1-HV0' and layer == 2 and gfz == 'P2':
            return 144
        if stgc == 'QS1-HV0' and layer == 3 and gfz == 'P1':
            return 256
        if stgc == 'QS1-HV0' and layer == 3 and gfz == 'P2':
            return 136
        if stgc == 'QS1-HV0' and layer == 4 and gfz == 'P1':
            return 150
        if stgc == 'QS1-HV0' and layer == 4 and gfz == 'P2':
            return 256
    elif PC == 'C':
        if stgc == 'QS1-HV1' and layer == 4 and gfz == 'P1':
            return 0
        if stgc == 'QS1-HV1' and layer == 4 and gfz == 'P2':
            return 253
        if stgc == 'QS1-HV1' and layer == 3 and gfz == 'P1':
            return 256
        if stgc == 'QS1-HV1' and layer == 3 and gfz == 'P2':
            return 6
        if stgc == 'QS1-HV1' and layer == 2 and gfz == 'P1':
            return 256
        if stgc == 'QS1-HV1' and layer == 2 and gfz == 'P2':
            return 14
        if stgc == 'QS1-HV1' and layer == 1 and gfz == 'P1':
            return 150
        if stgc == 'QS1-HV1' and layer == 1 and gfz == 'P2':
            return 256
        if stgc == 'QS1-HV0' and layer == 4 and gfz == 'P1':
            return 150
        if stgc == 'QS1-HV0' and layer == 4 and gfz == 'P2':
            return 3
        if stgc == 'QS1-HV0' and layer == 3 and gfz == 'P1':
            return 256
        if stgc == 'QS1-HV0' and layer == 3 and gfz == 'P2':
            return 144
        if stgc == 'QS1-HV0' and layer == 2 and gfz == 'P1':
            return 256
        if stgc == 'QS1-HV0' and layer == 2 and gfz == 'P2':
            return 136
        if stgc == 'QS1-HV0' and layer == 1 and gfz == 'P1':
            return 150
        if stgc == 'QS1-HV0' and layer == 1 and gfz == 'P2':
            return 256

    if stgc == 'QL3' and gfz == 'P1':
        return 256
    elif stgc == 'QL3' and gfz == 'P2':
        return 97

    elif stgc == 'QL2' and gfz == 'P1':
        return 256
    elif stgc == 'QL2' and gfz == 'P2':
        return 110

    # to be optimized by knowing exactly which strips are in which HV0 and HV1
    elif (stgc == 'QL1' or stgc == 'QL1-HV0' or stgc == 'QL1-HV1'
          ) and gfz == 'P1':
        if (layer == 1 or layer == 4):
            return NumTotalStrips(stgc)-256
        if (layer == 2 or layer == 3):
            return 256
    elif (stgc == 'QL1' or stgc == 'QL1-HV0' or stgc == 'QL1-HV1'
          ) and gfz == 'P2':
        if (layer == 2 or layer == 3):
            return NumTotalStrips(stgc)-256
        if (layer == 1 or layer == 4):
            return 256
    return -9999


def NPads(stgc, layer, PC):
    if stgc == 'QS1' and PC == 'P':
        if (layer == 1 or layer == 2):
            return 68
        if (layer == 3 or layer == 4):
            return 51
    elif stgc == 'QS1' and PC == 'C':
        if (layer == 1 or layer == 2):
            return 68
        if (layer == 3 or layer == 4):
            return 72
    if stgc == 'QS1-HV1' and layer == 1 and PC == 'P':
        return 44
    if stgc == 'QS1-HV1' and layer == 2 and PC == 'P':
        return 44
    if stgc == 'QS1-HV1' and layer == 3 and PC == 'P':
        return 33
    if stgc == 'QS1-HV1' and layer == 4 and PC == 'P':
        return 36
    if stgc == 'QS1-HV0' and layer == 1 and PC == 'P':
        return 28
    if stgc == 'QS1-HV0' and layer == 2 and PC == 'P':
        return 24
    if stgc == 'QS1-HV0' and layer == 3 and PC == 'P':
        return 18
    if stgc == 'QS1-HV0' and layer == 4 and PC == 'P':
        return 18
    if stgc == 'QS1-HV1' and layer == 1 and PC == 'C':
        return 68
    if stgc == 'QS1-HV1' and layer == 2 and PC == 'C':
        return 68
    if stgc == 'QS1-HV1' and layer == 3 and PC == 'C':
        return 72
    if stgc == 'QS1-HV1' and layer == 4 and PC == 'C':
        return 72
    if stgc == 'QS1-HV0' and layer == 1 and PC == 'C':
        return 68
    if stgc == 'QS1-HV0' and layer == 2 and PC == 'C':
        return 68
    if stgc == 'QS1-HV0' and layer == 3 and PC == 'C':
        return 72
    if stgc == 'QS1-HV0' and layer == 4 and PC == 'C':
        return 72
    elif stgc == 'QS2' and PC == 'P':
        if (layer == 1 or layer == 2):
            return 30
        if (layer == 3 or layer == 4):
            return 45
    elif stgc == 'QS2' and PC == 'C':
        if (layer == 1 or layer == 2):
            return 48
        if (layer == 3 or layer == 4):
            return 45
    elif stgc == 'QS3' and PC == 'P':
        if (layer == 1 or layer == 2):
            return 24
        if (layer == 3 or layer == 4):
            return 39
    elif stgc == 'QS3' and PC == 'C':
        if (layer == 1 or layer == 2):
            return 39
        if (layer == 3 or layer == 4):
            return 42
    if (stgc in ['QL1', 'QL1-HV0', 'QL1-HV1']) and PC == 'P':
        if (layer == 1 or layer == 2):
            return 10
        if (layer == 3 or layer == 4):
            return 112
    elif (stgc in ['QL1', 'QL1-HV0', 'QL1-HV1']) and PC == 'C':
        if (layer == 2 or layer == 3):
            return 96
        if (layer == 1 or layer == 4):
            return 96
    elif stgc == 'QL2' and PC == 'P':
        if (layer == 1 or layer == 2):
            return 56
        if (layer == 3 or layer == 4):
            return 75
    elif stgc == 'QL2' and PC == 'C':
        if (layer == 2 or layer == 3):
            return 56
        if (layer == 1 or layer == 4):
            return 56
    elif stgc == 'QL3' and PC == 'P':
        if (layer == 1 or layer == 2):
            return 60
        if (layer == 3 or layer == 4):
            return 70
    elif stgc == 'QL3' and PC == 'C':
        if (layer == 2 or layer == 3):
            return 52
        if (layer == 1 or layer == 4):
            return 56
    return -9999


def NWires(stgc, layer, PC):
    if PC == 'P':
        if stgc == 'QS1' or stgc == 'QS1-HV1':
            if (layer == 1 or layer == 3 or layer == 4):
                return -198 + 217
            if (layer == 2):
                return -198 + 218
        elif stgc == 'QS1-HV0':
            return 0
        elif stgc == 'QS2':
            if (layer == 1 or layer == 3 or layer == 4):
                return -198 + 227
            if (layer == 2):
                return -198 + 228
        elif stgc == 'QS3':
            if (layer == 2 or layer == 3 or layer == 4):
                return -198 + 236
            if (layer == 1):
                return -198 + 235
    elif PC == 'C':
        if stgc == 'QS1' or stgc == 'QS1-HV1':
            if (layer == 1 or layer == 2 or layer == 4):
                return -198 + 217
            if (layer == 3):
                return -198 + 218
        elif stgc == 'QS1-HV0':
            return 0
        elif stgc == 'QS2':
            if (layer == 1 or layer == 2 or layer == 4):
                return -198 + 227
            if (layer == 3):
                return -198 + 228
        elif stgc == 'QS3':
            if (layer == 2 or layer == 3 or layer == 1):
                return -198 + 236
            if (layer == 4):
                return -198 + 235

    if stgc == 'QL1' or stgc == 'QL1-HV0' or stgc == 'QL1-HV1':
        # first and last wire group very small -> merged with the previous
        return -198 + 230 - 2
    elif stgc == 'QL2':
        if (layer == 1 or layer == 4):
            return -198 + 246
        if (layer == 2 or layer == 3):
            return -198 + 247
    if stgc == 'QL3':
        if (layer == 1 or layer == 4):
            return -198 + 255
        if (layer == 2 or layer == 3):
            return -198 + 256
    return -9999


def IsStrip(stgc, AB, layer, gfz, channel, PC):
    if 'P' in AB:
        return False

    if stgc == 'QS3':
        if gfz == 'P1':
            if channel in range(0, 256):
                return True
        elif gfz == 'P2':
            if channel in range(0, 51):
                return True

    elif stgc == 'QS2':
        if gfz == 'P1':
            if channel in range(0, 256):
                return True
        elif gfz == 'P2':
            if channel in range(0, 109):
                return True

    elif stgc == 'QS1':
        if gfz == 'P1':
            if (layer == 1 or layer == 4) and channel in range(176, 256):
                return True
            if (layer == 1 or layer == 4) and channel in range(112, 144):
                return True
            if (layer == 1 or layer == 4) and channel in range(42, 80):
                return True
            if (layer == 2 or layer == 3) and channel in range(0, 256):
                return True
        elif gfz == 'P2':
            if (layer == 1 or layer == 4) and channel in range(0, 256):
                return True
            if (layer == 2 or layer == 3) and channel in range(176, 214):
                return True
            if (layer == 2 or layer == 3) and channel in range(112, 144):
                return True
            if (layer == 2 or layer == 3) and channel in range(0, 80):
                return True
    if PC == 'P':
        if stgc == 'QS1-HV1':
            if gfz == 'P1':
                if (layer == 4) and channel in range(233, 256):
                    return True
                if (layer == 2 or layer == 3) and channel in range(0, 256):
                    return True
            elif gfz == 'P2':
                if (layer == 1) and channel in range(3, 256):
                    return True
                if (layer == 4) and channel in range(0, 256):
                    return True
                if (layer == 2) and channel in range(0, 6):
                    return True
                if (layer == 3) and channel in range(0, 14):
                    return True
        elif stgc == 'QS1-HV0':
            if gfz == 'P1':
                if (layer == 4) and channel in range(176, 233):
                    return True
                if (layer == 1) and channel in range(176, 256):
                    return True
                if (layer == 1 or layer == 4) and channel in range(112, 144):
                    return True
                if (layer == 1 or layer == 4) and channel in range(42, 80):
                    return True
            elif gfz == 'P2':
                if (layer == 1) and channel in range(0, 3):
                    return True
                if (layer == 2 or layer == 3) and channel in range(176, 214):
                    return True
                if (layer == 2 or layer == 3) and channel in range(112, 144):
                    return True
                if (layer == 2) and channel in range(6, 80):
                    return True
                if (layer == 3) and channel in range(14, 80):
                    return True
    elif PC == 'C':
        if stgc == 'QS1-HV1':
            if gfz == 'P1':
                if (layer == 1) and channel in range(233, 256):
                    return True
                if (layer == 2 or layer == 3) and channel in range(0, 256):
                    return True
            elif gfz == 'P2':
                if (layer == 4) and channel in range(3, 256):
                    return True
                if (layer == 1) and channel in range(0, 256):
                    return True
                if (layer == 3) and channel in range(0, 6):
                    return True
                if (layer == 2) and channel in range(0, 14):
                    return True
        elif stgc == 'QS1-HV0':
            if gfz == 'P1':
                if (layer == 1) and channel in range(176, 233):
                    return True
                if (layer == 4) and channel in range(176, 256):
                    return True
                if (layer == 1 or layer == 4) and channel in range(112, 144):
                    return True
                if (layer == 1 or layer == 4) and channel in range(42, 80):
                    return True
            elif gfz == 'P2':
                if (layer == 4) and channel in range(0, 3):
                    return True
                if (layer == 2 or layer == 3) and channel in range(176, 214):
                    return True
                if (layer == 2 or layer == 3) and channel in range(112, 144):
                    return True
                if (layer == 3) and channel in range(6, 80):
                    return True
                if (layer == 2) and channel in range(14, 80):
                    return True
    if stgc == 'QL3':
        if gfz == 'P1':
            if channel in range(0, 256):
                return True
        elif gfz == 'P2':
            if channel in range(0, 97):
                return True
    elif stgc == 'QL2':
        if gfz == 'P1':
            if channel in range(0, 256):
                return True
        elif gfz == 'P2':
            if channel in range(0, 110):
                return True
    elif stgc == 'QL1':
        if gfz == 'P1':
            if (layer == 1 or layer == 4) and channel in range(176, 256):
                return True
            if (layer == 1 or layer == 4) and channel in range(112, 144):
                return True
            if (layer == 1 or layer == 4) and channel in range(40, 80):
                return True
            if (layer == 2 or layer == 3) and channel in range(0, 256):
                return True
        elif gfz == 'P2':
            if (layer == 1 or layer == 4) and channel in range(0, 256):
                return True
            if (layer == 2 or layer == 3) and channel in range(176, 216):
                return True
            if (layer == 2 or layer == 3) and channel in range(112, 144):
                return True
            if (layer == 2 or layer == 3) and channel in range(0, 80):
                return True
    if PC == 'P':
        if stgc == 'QL1-HV1':
            if gfz == 'P1':
                if (layer == 4) and channel in range(231, 256):
                    return True
                if (layer == 2 or layer == 3) and channel in range(0, 256):
                    return True
            elif gfz == 'P2':
                if (layer == 1) and channel in range(1, 256):
                    return True
                if (layer == 4) and channel in range(0, 256):
                    return True
                if (layer == 2) and channel in range(0, 8):
                    return True
                if (layer == 3) and channel in range(0, 16):
                    return True
        elif stgc == 'QL1-HV0':
            if gfz == 'P1':
                if (layer == 4) and channel in range(176, 231):
                    return True
                if (layer == 1) and channel in range(176, 256):
                    return True
                if (layer == 1 or layer == 4) and channel in range(112, 144):
                    return True
                if (layer == 1 or layer == 4) and channel in range(40, 80):
                    return True
            elif gfz == 'P2':
                if (layer == 1) and channel in range(0, 1):
                    return True
                if (layer == 2 or layer == 3) and channel in range(176, 216):
                    return True
                if (layer == 2 or layer == 3) and channel in range(112, 144):
                    return True
                if (layer == 2) and channel in range(7, 80):
                    return True
                if (layer == 3) and channel in range(15, 80):
                    return True

    elif PC == 'C':
        if stgc == 'QL1-HV1':
            if gfz == 'P1':
                if (layer == 1) and channel in range(231, 256):
                    return True
                if (layer == 2 or layer == 3) and channel in range(0, 256):
                    return True
            elif gfz == 'P2':
                if (layer == 4) and channel in range(1, 256):
                    return True
                if (layer == 1) and channel in range(0, 256):
                    return True
                if (layer == 3) and channel in range(0, 8):
                    return True
                if (layer == 2) and channel in range(0, 16):
                    return True
        elif stgc == 'QL1-HV0':
            if gfz == 'P1':
                if (layer == 1) and channel in range(176, 231):
                    return True
                if (layer == 4) and channel in range(176, 256):
                    return True
                if (layer == 1 or layer == 4) and channel in range(112, 144):
                    return True
                if (layer == 1 or layer == 4) and channel in range(40, 80):
                    return True
            elif gfz == 'P2':
                if (layer == 4) and channel in range(0, 2):
                    return True
                if (layer == 2 or layer == 3) and channel in range(176, 216):
                    return True
                if (layer == 2 or layer == 3) and channel in range(112, 144):
                    return True
                if (layer == 3) and channel in range(7, 80):
                    return True
                if (layer == 2) and channel in range(15, 80):
                    return True
    return False


def IsPad(stgc, AB, layer, gfz, channel, PC):
    if 'S' in AB:
        return False
    if stgc == 'QS1':
        if PC == 'P':
            if layer == 1 and channel in range(24, 92):
                return True
            elif layer == 2 and channel in range(36, 104):
                return True
            elif layer == 3 and channel in range(53, 104):
                return True
            elif layer == 4 and channel in range(24, 75):
                return True
        elif PC == 'C':
            if layer == 1 and channel in range(24, 92):
                return True
            elif layer == 2 and channel in range(36, 104):
                return True
            elif layer == 3 and channel in range(32, 104):
                return True
            elif layer == 4 and channel in range(24, 96):
                return True
    elif stgc == 'QS1-HV1':
        if PC == 'P':
            if layer == 1 and channel in range(24, 68):
                return True
            elif layer == 2 and channel in range(60, 104):
                return True
            elif layer == 3 and channel in range(71, 104):
                return True
            elif layer == 4 and channel in range(24, 60):
                return True
        elif PC == 'C':
            if layer == 1 and channel in range(24, 72):
                return True
            elif layer == 2 and channel in range(60, 104):
                return True
            elif layer == 3 and channel in range(56, 104):
                return True
            elif layer == 4 and channel in range(24, 68):
                return True
    elif stgc == 'QS1-HV0':
        if PC == 'P':
            if layer == 1 and channel in range(64, 92):
                return True
            elif layer == 2 and channel in range(36, 60):
                return True
            elif layer == 3 and channel in range(53, 71):
                return True
            elif layer == 4 and channel in range(57, 75):
                return True
        elif PC == 'C':
            if layer == 1 and channel in range(68, 92):
                return True
            elif layer == 2 and channel in range(36, 60):
                return True
            elif layer == 3 and channel in range(32, 60):
                return True
            elif layer == 4 and channel in range(68, 96):
                return True
    elif stgc == 'QS2':
        if PC == 'P':
            if layer == 1 and channel in range(49, 79):
                return True
            elif layer == 2 and channel in range(49, 79):
                return True
            elif layer == 3 and channel in range(41, 86):
                return True
            elif layer == 4 and channel in range(42, 87):
                return True
        elif PC == 'C':
            if layer == 1 and channel in range(40, 88):
                return True
            elif layer == 2 and channel in range(40, 88):
                return True
            elif layer == 3 and channel in range(41, 86):
                return True
            elif layer == 4 and channel in range(42, 87):
                return True
    elif stgc == 'QS3':
        if PC == 'P':
            if layer == 1 and channel in range(31, 55):
                return True
            elif layer == 2 and channel in range(31, 55):
                return True
            elif layer == 3 and channel in range(23, 62):
                return True
            elif layer == 4 and channel in range(24, 63):
                return True
        elif PC == 'C':
            if layer == 1 and channel in range(23, 62):
                return True
            elif layer == 2 and channel in range(88, 127):
                return True
            elif layer == 3 and channel in range(86, 128):
                return True
            elif layer == 4 and channel in range(22, 64):
                return True
    elif stgc == 'QL1':
        if PC == 'P':
            if layer == 1 and channel in range(24, 126):
                return True
            elif layer == 2 and channel in range(18, 120):
                return True
            elif layer == 3 and channel in range(8, 120):
                return True
            elif layer == 4 and channel in range(16, 128):
                return True
        elif PC == 'C':
            if layer == 1 and channel in range(24, 120):
                return True
            elif layer == 2 and channel in range(24, 120):
                return True
            elif layer == 3 and channel in range(19, 115):
                return True
            elif layer == 4 and channel in range(29, 125):
                return True
    elif stgc == 'QL1-HV0':
        if PC == 'P':
            if layer == 1 and channel in range(80, 126):
                return True
            elif layer == 2 and channel in range(18, 54):
                return True
            elif layer == 3 and channel in range(8, 50):
                return True
            elif layer == 4 and (
                    channel in range(16, 24) or channel in range(101, 128)):
                return True
        elif PC == 'C':
            if layer == 1 and channel in range(92, 120):
                return True
            elif layer == 2 and channel in range(24, 60):
                return True
            elif layer == 3 and channel in range(19, 55):
                return True
            elif layer == 4 and channel in range(84, 125):
                return True
    elif stgc == 'QL1-HV1':
        if PC == 'P':
            if layer == 1 and channel in range(24, 99):
                return True
            elif layer == 2 and channel in range(54, 120):
                return True
            elif layer == 3 and channel in range(43, 120):
                return True
            elif layer == 4 and channel in range(24, 102):
                return True
        elif PC == 'C':
            if layer == 1 and channel in range(24, 96):
                return True
            elif layer == 2 and channel in range(56, 120):
                return True
            elif layer == 3 and channel in range(49, 115):
                return True
            elif layer == 4 and channel in range(29, 88):
                return True
    elif stgc == 'QL2':
        if PC == 'P':
            if layer == 1 and channel in range(34, 90):
                return True
            elif layer == 2 and channel in range(38, 94):
                return True
            elif layer == 3 and channel in range(29, 104):
                return True
            elif layer == 4 and channel in range(24, 99):
                return True
        elif PC == 'C':
            if layer == 1 and channel in range(34, 90):
                return True
            elif layer == 2 and channel in range(38, 94):
                return True
            elif layer == 3 and channel in range(38, 94):
                return True
            elif layer == 4 and channel in range(34, 90):
                return True
    elif stgc == 'QL3':
        if PC == 'P':
            if layer == 1 and channel in range(42, 102):
                return True
            elif layer == 2 and channel in range(24, 84):
                return True
            elif layer == 3 and channel in range(24, 94):
                return True
            elif layer == 4 and channel in range(32, 102):
                return True
        elif PC == 'C':
            if layer == 1 and channel in range(50, 102):
                return True
            elif layer == 2 and channel in range(24, 76):
                return True
            elif layer == 3 and channel in range(24, 80):
                return True
            elif layer == 4 and channel in range(46, 102):
                return True
    else:
        return False


def IsWire(stgc, AB, layer, gfz, channel, PC):
    if 'S' in AB:
        return False
    if PC == 'P':
        if stgc == 'QS1' or stgc == 'QS1-HV1':
            if (layer in [1, 3, 4]) and channel in range(198, 217):
                return True
            if (layer == 2) and channel in range(198, 218):
                return True
        elif stgc == 'QS2':
            if (layer in [1, 3, 4]) and channel in range(198, 227):
                return True
            if (layer == 2) and channel in range(198, 228):
                return True
        elif stgc == 'QS3':
            if (layer in [2, 3, 4]) and channel in range(198, 236):
                return True
            if (layer == 1) and channel in range(198, 235):
                return True
    if PC == 'C':
        if stgc == 'QS1' or stgc == 'QS1-HV1':
            if (layer in [1, 2, 4]) and channel in range(198, 217):
                return True
            if (layer == 3) and channel in range(198, 218):
                return True
        elif stgc == 'QS2':
            if (layer in [1, 2, 4]) and channel in range(198, 227):
                return True
            if (layer == 3) and channel in range(198, 228):
                return True
        elif stgc == 'QS3':
            if (layer in [3, 2, 1]) and channel in range(198, 236):
                return True
            if (layer == 4) and channel in range(198, 235):
                return True

    # inverted the labeling due to gasgap <-> layer confusion (no need)
    if stgc == 'QL1' or stgc == 'QL1-HV1':
        if channel in range(199, 229):
            return True
    elif stgc == 'QL2':
        if (layer == 1 or layer == 4) and channel in range(198, 246):
            return True
        if (layer == 2 or layer == 3) and channel in range(198, 247):
            return True
    if stgc == 'QL3':
        if (layer == 2 or layer == 3) and channel in range(198, 256):
            return True
        if (layer == 1 or layer == 4) and channel in range(198, 255):
            return True
    return False


def IsValidChannel(stgc, AB, layer, gfz, channel, PC):
    strip = IsStrip(stgc, AB, int(layer), gfz, int(channel), PC)
    pad = IsPad(stgc, AB, int(layer), gfz, int(channel), PC)
    wire = IsWire(stgc, AB, int(layer), gfz, int(channel), PC)
    if (AB == 'S' and strip) or (AB == 'P' and (pad or wire)):
        return True
    return False


def GFZChannel2Pin(chan):
    channel = int(chan)
    # Convert from channel number to gfz pin number
    # (same for all quads/layers/connectors)
    pin = 'NULL'
    if channel in range(0, 128):
        pin = "j" + str(127 - channel)
    elif channel in range(128, 256):
        pin = "i" + str(255 - channel)
    else:
        raise ValueError("""[GFZChannel2Pin] I'm sorry, that is not a valid
                         channel number %s""" % (channel))
    return pin


def Qmap(layer, AB, gfz, channel, PC, stgc,
         printout=1, return_channel_type=False):
    """
    Inputs:
    -layer: int, 1 2 3 or 4
    -AB: string, S or P
    -gfz: string, P1 or P2
    -channel: float for some reason, e.g. 204.0
    -stgc: string, quad type (QS1 or QS2 or ...)
    -printout: whether or not to print messages

    Returns:
    - Int, the strip/pad/wire number
    (Alam's mapping, large base = 1 for pads, small base = 1 for strips,
    HV side = 1 for wires)
    - if return_channel_type==True, also returns'strip'/'pad'/'wire'

    Raises:
    - ValueError if invalid parameters given
    """
    # check that it is a valid channel
    if AB == 'S':  # Strips
        if not IsStrip(stgc, AB, int(layer), gfz, int(channel), PC):
            raise ValueError(
                """invalid strip channel number...
                stgc={stgc}, AB={AB}, layer={layer}, gfz={gfz},
                channel={channel}, PC={PC}""".format(
                stgc=stgc, AB=AB, layer=layer, gfz=gfz, channel=channel, PC=PC))
        else:
            if printout:
                print('Good strip channel')
    # similar function for the pads and wires
    elif AB == 'P':  # Pads
        if IsPad(stgc, AB, int(layer), gfz, channel, PC):
            if printout:
                print('Good pad channel')
        elif IsWire(stgc, AB, int(layer), gfz, channel, PC):
            if printout:
                print('Good wire channel')
        else:
            raise ValueError(
                """invalid pad/wire channel number...
                stgc={stgc}, AB={AB}, layer={layer}, gfz={gfz},
                channel={channel}, PC={PC}""".format(
                stgc=stgc, AB=AB, layer=layer, gfz=gfz, channel=channel, PC=PC))

    # Convert from channel number to gfz pin number
    # (same for all quads/layers/connectors)
    pin = GFZChannel2Pin(channel)

    gasgap = Layer2GasGap(int(layer), PC)

    # convert from pin number to key and look up the key in the mapping
    if pin != 'NULL':
        key = (gfz, pin)
        if AB == 'S':  # Strips
            strip = 'NULL'
            if stgc == 'QS3':
                strip = QS3map.STRMAPQS3[key][int(layer)-1]
            elif stgc == 'QS2':
                strip = QS2map.STRMAPQS2[key][int(layer)-1]
            elif stgc == 'QS1' or stgc == 'QS1-HV1' or stgc == 'QS1-HV0':
                strip = QS1map.STRMAPQS1[key][int(layer)-1]
            elif stgc == 'QL3':
                strip = QL3map.STRMAPQL3[key][int(layer)-1]
            elif stgc == 'QL2':
                strip = QL2map.STRMAPQL2[key][int(layer)-1]
            elif stgc == 'QL1' or stgc == 'QL1-HV0' or stgc == 'QL1-HV1':
                strip = QL1map.STRMAPQL1[key][int(layer)-1]

            if strip == 'NULL' or strip == -1:
                raise ValueError(
                    "No STRIP map entry found for" +
                    "gfz= %s and pin= %s in quad %s" % (gfz, pin, stgc))

            if printout:
                print("""The Channel %s corresponds to strip number %s
                      (strip 1 is the closest to the beampipe)"""
                      % (channel, strip))
            if int(layer) == 1 or int(layer) == 4:
                connecter = strip
            elif int(layer) == 2 or int(layer) == 3:
                connecter = int(NumTotalStrips(stgc))+1-int(strip)
            if printout:
                print("""In the Adapter Board this strip is the
                      %s -th from the AB notch""" % (connecter))
                print('')
            if return_channel_type:
                return strip, 'strip'
            else:
                return strip
        elif AB == 'P':  # Pads
            # general range covering all quads and layers
            if channel in range(0, 128):
                pad = "NULL"
                if stgc == 'QS3':
                    if PC == 'PIVOT' or PC == 'P':
                        pad = QS3map.PADMAPQS3P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        pad = QS3map.PADMAPQS3C[key][int(layer)-1]
                elif stgc == 'QS2':
                    if PC == 'PIVOT' or PC == 'P':
                        pad = QS2map.PADMAPQS2P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        pad = QS2map.PADMAPQS2C[key][int(layer)-1]
                elif stgc == 'QS1' or stgc == 'QS1-HV1' or stgc == 'QS1-HV0':
                    if PC == 'PIVOT' or PC == 'P':
                        pad = QS1map.PADMAPQS1P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        pad = QS1map.PADMAPQS1C[key][int(layer)-1]
                elif stgc == 'QL3':
                    if PC == 'PIVOT' or PC == 'P':
                        pad = QL3map.PADMAPQL3P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        pad = QL3map.PADMAPQL3C[key][int(layer)-1]
                elif stgc == 'QL2':
                    if PC == 'PIVOT' or PC == 'P':
                        pad = QL2map.PADMAPQL2P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        pad = QL2map.PADMAPQL2C[key][int(layer)-1]
                elif stgc == 'QL1' or stgc == 'QL1-HV0' or stgc == 'QL1-HV1':
                    if PC == 'PIVOT' or PC == 'P':
                        pad = QL1map.PADMAPQL1P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        pad = QL1map.PADMAPQL1C[key][int(layer)-1]

                if pad == "NULL":
                    raise ValueError("""No PAD map entry found for gfz= %s
                        and pin= %s in quad %s %s""" % (gfz, pin, stgc, PC))
                if pad != -1:
                    if printout:
                        print("""The Pad connected to GFZ Channel
                        %s is %s""" % (channel, pad))
                else:
                    raise ValueError("""I'm sorry, this layer has no Pad
                    connected to that channel number: %s pad %s""" % (
                        channel, pad))
                if return_channel_type:
                    return pad, 'pad'
                else:
                    return pad
            # WIRES general range covering all quads and layers
            elif channel in range(198, 256):
                wire = "NULL"
                if stgc == 'QS3':
                    if PC == 'PIVOT' or PC == 'P':
                        wire = QS3map.WIREMAPQS3P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        wire = QS3map.WIREMAPQS3C[key][int(layer)-1]
                elif stgc == 'QS2':
                    if PC == 'PIVOT' or PC == 'P':
                        wire = QS2map.WIREMAPQS2P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        wire = QS2map.WIREMAPQS2C[key][int(layer)-1]
                elif stgc == 'QS1' or stgc == 'QS1-HV1' or stgc == 'QS1-HV0':
                    if PC == 'PIVOT' or PC == 'P':
                        wire = QS1map.WIREMAPQS1P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        wire = QS1map.WIREMAPQS1C[key][int(layer)-1]

                if stgc == 'QL3':
                    if PC == 'PIVOT' or PC == 'P':
                        wire = QL3map.WIREMAPQL3P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        wire = QL3map.WIREMAPQL3C[key][int(layer)-1]
                elif stgc == 'QL2':
                    if PC == 'PIVOT' or PC == 'P':
                        wire = QL2map.WIREMAPQL2P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        wire = QL2map.WIREMAPQL2C[key][int(layer)-1]
                elif stgc == 'QL1' or stgc == 'QL1-HV1' or stgc == 'QL1-HV0':
                    if PC == 'PIVOT' or PC == 'P':
                        wire = QL1map.WIREMAPQL1P[key][int(layer)-1]
                    elif PC == 'CONFIRM' or PC == 'C':
                        wire = QL1map.WIREMAPQL1C[key][int(layer)-1]
                if wire == "NULL":
                    raise ValueError("""No WIRE map entry found for gfz= %s and
                                     pin= %s in quad %s""" % (gfz, pin, stgc))
                if wire != -1:
                    if printout:
                        print("""The Wire Group connected to GFZ Channel
                              %s is %s""" % (channel, wire))
                else:
                    raise ValueError("""I'm sorry, this layer has no Wire Group
                                     connected to that channel number""")
                if return_channel_type:
                    return wire, 'wire'
                else:
                    return wire
            else:
                raise ValueError("No valid pin %s" % (pin))
    else:
        raise ValueError("I'm sorry, that is not a valid channel number...")
    return


if __name__ == "__main__":
    layer = 1
    AB = "P"
    gfz = "P1"
    channel = 34
    PC = 'C'
    stgc = 'QL2'
    print(Qmap(layer, AB, gfz, channel, PC, stgc, printout=1))
