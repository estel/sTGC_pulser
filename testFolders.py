import os
import sys
from foldernameDB import getQuadUniqueIDList
print("Usage: python testFolders.py /Users/Estel/Downloads/QS3-P-6_Data/  WEDGE2")

DATAFOLDER=sys.argv[1]
#WEDGE='WEDGE1-HVFILTER' ; QUADS=['QS1','QS2','QS3']
##WEDGE3##WEDGE='' ; QUADS=['QS1.P.8', 'QS2.P.4', 'QS3.P.1']
#WEDGE='' ; QUADS=['QS1.P.8', 'QS2.P.10', 'QS3.P.8']
#WEDGE='' ; QUADS=['QS2.P.5']
#WEDGE='' ; QUADS=['QS3.P.7']
#WEDGE='' ; QUADS=['QS1.P.10']
WEDGE='' ; QUADS=['QS2.P.6']
QUADS=[sys.argv[2]]

#if WEDGE != '':
#    QUADS=[WEDGE]
#QUADS=getQuadUniqueIDList(WEDGE)
print(os.listdir(DATAFOLDER))
print(QUADS)
for quad in QUADS :
    #for stgc in ['QS1'] :
    #for stgc in ['QS1-HV0','QS1-HV1','QS2','QS3'] :
    for stgc in ['QS1','QS2','QS3', 'QL1', 'QL2', 'QL3' ] :
        #if 'QS1' in stgc: STGC='QS1'
        #else: STGC=stgc
        if stgc not in  quad: continue
        for layer in ['Layer1','Layer2','Layer3','Layer4'] :
            for AB in ['_P_GFZP1','_S_GFZP1','_S_GFZP2'] : 
                for PC in ['PIVOT', 'CONFIRM'] : 
                    quad_split=quad.rsplit('.', -1)
                    if len(quad_split)>1:
                        if (PC=='PIVOT' and 'P' not in quad) : continue
                        if (PC=='CONFIRM' and 'C' not in quad) : continue
                    NUM=''
                    if len(quad_split)>1:
                        if quad_split[1]=='P' : NUM="PIVOT-"+quad_split[2]
                        if quad_split[1]=='C' : NUM="CONFIRM-"+quad_split[2]
                        #special case: renaming
                        if quad_split[0]=='QS3' and quad_split[1]=='P' and quad_split[2]=='1': NUM='PIVOT-DISHY'
                    #print(NUM)
                    if stgc == 'QS1' or stgc == 'QL1' :
                      HVlist=['HV1','HV0']
                    else:
                      HVlist=['']
                    print('====================================================')
                    for HV in HVlist : 
                      if PC=='PIVOT' :
                        if ( stgc == 'QS1' or stgc == 'QL1' ) and layer == 'Layer1' and AB == '_S_GFZP1' and HV == 'HV1': continue 
                        if ( stgc == 'QS1' or stgc == 'QL1' ) and layer == 'Layer2' and AB == '_S_GFZP1' and HV == 'HV0': continue 
                        if ( stgc == 'QS1' or stgc == 'QL1' ) and layer == 'Layer3' and AB == '_S_GFZP1' and HV == 'HV0': continue 
                        if ( stgc == 'QS1' or stgc == 'QL1' ) and layer == 'Layer4' and AB == '_S_GFZP2' and HV == 'HV0': continue 
    
                      elif  stgc == 'QS1' or PC=='CONFIRM'  :
                        if ( stgc == 'QS1' or stgc == 'QL1' ) and layer == 'Layer4' and AB == '_S_GFZP1' and HV == 'HV1': continue 
                        if ( stgc == 'QS1' or stgc == 'QL1' ) and layer == 'Layer3' and AB == '_S_GFZP1' and HV == 'HV0': continue 
                        if ( stgc == 'QS1' or stgc == 'QL1' ) and layer == 'Layer2' and AB == '_S_GFZP1' and HV == 'HV0': continue 
                        if ( stgc == 'QS1' or stgc == 'QL1' ) and layer == 'Layer1' and AB == '_S_GFZP2' and HV == 'HV0': continue 
                    #  if stgc == 'QS1-HV1' and layer == 'Layer1' and AB == '_S_GFZP1' : continue 
                    #  if stgc == 'QS1-HV0' and layer == 'Layer2' and AB == '_S_GFZP1' : continue 
                    #  if stgc == 'QS1-HV0' and layer == 'Layer3' and AB == '_S_GFZP1' : continue 
                    #  if stgc == 'QS1-HV0' and layer == 'Layer4' and AB == '_S_GFZP2' : continue 
                      print('==============')
                      FOUND=False
                      for runFolder in os.listdir(DATAFOLDER):
                          if stgc in runFolder and layer in runFolder and PC in runFolder and AB in runFolder and HV in runFolder and WEDGE in runFolder and NUM in runFolder:
                              if 'kk_' in runFolder or 'old_' in runFolder or 'ZOOMOUT' in runFolder or 'V_' in runFolder or 'good_' in runFolder or 'TEST' in runFolder: continue
                              #print(runFolder)
                              # avoid that CONFIRM-15 is included when searching for CONFIRM-1 etc.
                              mystring = (runFolder.split(NUM))[1].split("_Layer")[0]
                              if len(mystring) > 0 :
                                 if mystring[0].isdigit() :
                                    #print("this is not the quad I am looking for!")
                                    continue

                              FOUND=True
                              if WEDGE=='' : wedge=quad
                              else: wedge=WEDGE
                              # print out the code to hardcode the good runs
                              #print('        if (wedge==\'\' or \'%s\' in wedge) and ( \'%s\' in QuadId ) and ( layer == \'\' or layer==%s ) and ( ab == \'\' or ab == \'%s\' ) and ( gfz == \'\' or gfz == \'%s\' ) and ( hv == \'\' or hv == \'%s\' ):' %( wedge, stgc, layer[-1:], AB[1:2], AB[-2:], HV ))
                              print('        if (wedge==\'\' or wedge==\'%s\') and ( \'%s\' in QuadId ) and ( layer == \'\' or layer==%s ) and ( ab == \'\' or ab == \'%s\' ) and ( gfz == \'\' or gfz == \'%s\' ) and ( hv == \'\' or hv == \'%s\' ):' %( wedge, stgc, layer[-1:], AB[1:2], AB[-2:], HV ))

                              print('            folderlist.extend([\'%s\'])' %( runFolder))
                      if FOUND == False :
                          if WEDGE=='' : wedge=quad
                          else: wedge=WEDGE
                          print(' >>>>>>>>>>>>>=========>>>>>>>>>>>>>>>  MISSING test! wedge== %s  quad== %s layer== %s ab== %s  gfz== %s  hv== %s ' %( wedge, stgc, layer[-1:], AB[1:2], AB[-2:], HV ))  

