#def beep():
#    print "\a"
#beep()



import os
import numpy as np

A4 = 440  # Hz
twelfthroot2 = np.power(2., 1./12.)
octaves = np.array([1./8., 1./4., 1./3., 1./2., 1, 2, 4, 8, 16])

A = A4 * octaves
Asharp, Bflat = [A * twelfthroot2]*2
B = Bflat * twelfthroot2
C = B * twelfthroot2
Csharp, Dflat = [C * twelfthroot2]*2
D = Dflat * twelfthroot2
Dsharp, Eflat = [D * twelfthroot2]*2
E = Eflat * twelfthroot2
F = E * twelfthroot2
Fsharp, Gflat = [F * twelfthroot2]*2
G = Gflat * twelfthroot2
Gsharp, Aflat = [G * twelfthroot2]*2

bpm = 120.  # beats per minute
 
quarter = 60./bpm  # seconds
eighth = quarter/2
sixthth = eighth/2
half = quarter*2



with open('number.txt', 'r+') as f:
    number = int(f.readline())
    
def play_note(length, note):
    if note == 'rest':
        os.system('sleep {}'.format(length))
    else:
        os.system('play -nq -t alsa synth {} sine {}'.format(length, note))

def rick() :
    os.system('play -nq -t alsa synth {} sine {}'.format(quarter/4, C[4]))
    os.system('play -nq -t alsa synth {} sine {}'.format(quarter/4, D[4]))
    os.system('play -nq -t alsa synth {} sine {}'.format(quarter/4, F[4]))
    os.system('play -nq -t alsa synth {} sine {}'.format(quarter/4, D[4]))
    
    os.system('play -nq -t alsa synth {} sine {}'.format(quarter/2+quarter/4, A[5]))
    os.system('play -nq -t alsa synth {} sine {}'.format(quarter/2+quarter/4, A[5]))
    os.system('play -nq -t alsa synth {} sine {}'.format(quarter/2+quarter, G[4]))

def finished():
    global number
    if number == 0:
        play_note(quarter+eighth, 'rest')
        play_note(sixthth, Csharp[5])
        play_note(sixthth, B[5])
        play_note(quarter, Csharp[5])
        play_note(half+eighth, Fsharp[4])
    elif number == 1:
        play_note(sixthth, D[5])
        play_note(sixthth, Csharp[5])
        play_note(eighth, D[5])
        play_note(eighth, Csharp[5])
        play_note(half+eighth, B[5])
    elif number == 2:
        play_note(sixthth, D[5])
        play_note(sixthth, Csharp[5])
        play_note(quarter, D[5])
        play_note(half+eighth, Fsharp[4])
    elif number == 3:
        play_note(sixthth, B[5])
        play_note(sixthth, A[5])
        play_note(eighth, B[5])
        play_note(eighth, A[5])
        play_note(eighth, Gsharp[4])
        play_note(eighth, B[5])

    #
    elif number == 4:
        play_note(quarter+eighth, A[5])
        play_note(sixthth, Csharp[5])
        play_note(sixthth, B[5])
        play_note(quarter, Csharp[5])
        play_note(half+eighth, Fsharp[4])
    elif number == 5:
        play_note(sixthth, D[5])
        play_note(sixthth, Csharp[5])
        play_note(eighth, D[5])
        play_note(eighth, Csharp[5])
        play_note(half+eighth, B[5])
    elif number == 6:
        play_note(sixthth, D[5])
        play_note(sixthth, Csharp[5])
        play_note(quarter, D[5])
        play_note(half+eighth, Fsharp[4])
    elif number == 7:
        play_note(sixthth, B[5])
        play_note(sixthth, A[5])
        play_note(eighth, B[5])
        play_note(eighth, A[5])
        play_note(eighth, Gsharp[4])
        play_note(eighth, B[5])
        play_note(quarter+eighth, A[5])

    #
    elif number == 8:
        play_note(quarter+eighth, A[5])
        play_note(sixthth, Gsharp[4])
        play_note(sixthth, A[5])
        play_note(quarter+eighth, B[5])
        play_note(sixthth, A[5])
        play_note(sixthth, B[5])
        play_note(eighth, Csharp[5])
        play_note(eighth, B[5])
        play_note(eighth, A[5])
        play_note(eighth, Gsharp[4])
        play_note(quarter, Fsharp[4])
    elif number == 9:
        play_note(quarter, D[5])
        play_note(half+quarter, Csharp[5])
        play_note(sixthth, D[5])
        play_note(sixthth, Csharp[5])
        play_note(sixthth, B[5])
        play_note(sixthth, Csharp[5])
        play_note(half+half, Csharp[5])
    
    number = number+1
    if number == 10:
        number = 0
    with open('number.txt', 'w+') as f:
        f.write(str(number))
    

if __name__ == "__main__":
    final_countdown()
    #finished()

