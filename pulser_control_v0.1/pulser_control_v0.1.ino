// Pins Arduino nano
#define A1_s1       7
#define A0_s1       8
#define A1_s2       3
#define A0_s2       2
#define G4          A5
#define G3          9
#define G2          12
#define G1          A0
#define E1_65       13
#define E1_66       4
#define E1_67       6
#define D           A1
#define C           A2
#define B           A3
#define A           A4
#define readOutput A7

const byte numChars = 32;
char receivedChars[numChars]; // an array to store the received data
int channel = 0;

boolean newData = false;

void setup() {
  Serial.begin(9600);

  pinMode(A1_s1, OUTPUT);
  pinMode(A0_s1, OUTPUT);
  pinMode(A1_s2, OUTPUT);
  pinMode(A0_s2, OUTPUT);
  pinMode(G4, OUTPUT);
  pinMode(G3, OUTPUT);
  pinMode(G2, OUTPUT);
  pinMode(G1, OUTPUT);
  pinMode(E1_65, OUTPUT);
  pinMode(E1_66, OUTPUT);
  pinMode(E1_67, OUTPUT);
  pinMode(D, OUTPUT);
  pinMode(C, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(A, OUTPUT);
  
  pinMode(readOutput, INPUT);
  digitalWrite(readOutput, LOW);

  Serial.println("<Arduino is ready>");
  Serial.println("Enter a number between [0,255] to select a channel");
  Serial.println("Enter +,- to go to next,previous channel");
}

void write_mux(boolean a1_s1, boolean a0_s1, boolean a1_s2, boolean a0_s2,
               boolean g4, boolean g3, boolean g2, boolean g1,
               boolean e1_65, boolean e1_66, boolean e1_67,
               boolean d, boolean c, boolean b, boolean a) {

  digitalWrite(A1_s1, a1_s1);
  digitalWrite(A0_s1, a0_s1);
  digitalWrite(A1_s2, a1_s2);
  digitalWrite(A0_s2, a0_s2);
  digitalWrite(G4, g4);
  digitalWrite(G3, g3);
  digitalWrite(G2, g2);
  digitalWrite(G1, g1);
  digitalWrite(E1_65, e1_65);
  digitalWrite(E1_66, e1_66);
  digitalWrite(E1_67, e1_67);
  digitalWrite(D, d);
  digitalWrite(C, c);
  digitalWrite(B, b);
  digitalWrite(A, a);
}



void loop() {
 recvWithEndMarker();
 parseData();
}

void recvWithEndMarker() {
 static byte ndx = 0;
 char endMarker = '\n';
 char rc;
 
 while (Serial.available() > 0 && newData == false) {
   rc = Serial.read();
  
   if (rc != endMarker) {
    receivedChars[ndx] = rc;
    ndx++;
    if (ndx >= numChars) {
      ndx = numChars - 1;
    }
   }
   else {
    receivedChars[ndx] = '\0'; // terminate the string
    ndx = 0;
    newData = true;
   }
 }
}

void parseData() {
 if (newData == true) {
 Serial.print("This just in ... ");
 Serial.println(receivedChars);
 if(!strcmp(receivedChars,"+"))
  channel+=1;
 else if(!strcmp(receivedChars,"-"))
  channel-=1;
 else
  channel = atoi(receivedChars);
 if(channel == 256)
  channel = 0;
 if(channel == -1)
  channel = 255;
 Serial.println(channel);
 write_mux(channel);
 newData = false;
 }
}

void write_mux(int channel){
  switch (channel) {
      case 0: write_mux(1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1); break;
      case 1: write_mux(1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1); break;
      case 2: write_mux(0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1); break;
      case 3: write_mux(0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1); break;
      case 4: write_mux(1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0); break;
      case 5: write_mux(0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0); break;
      case 6: write_mux(0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0); break;
      case 7: write_mux(1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0); break;
      case 8: write_mux(1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1); break;
      case 9: write_mux(0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1); break;
      case 10: write_mux(0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1); break;
      case 11: write_mux(1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1); break;
      case 12: write_mux(1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0); break;
      case 13: write_mux(0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0); break;
      case 14: write_mux(0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0); break;
      case 15: write_mux(1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0); break;
      case 16: write_mux(1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1); break;
      case 17: write_mux(0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1); break;
      case 18: write_mux(0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1); break;
      case 19: write_mux(1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1); break;
      case 20: write_mux(0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0); break;
      case 21: write_mux(1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0); break;
      case 22: write_mux(1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0); break;
      case 23: write_mux(0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0); break;
      case 24: write_mux(1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1); break;
      case 25: write_mux(0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1); break;
      case 26: write_mux(0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1); break;
      case 27: write_mux(1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1); break;
      case 28: write_mux(1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0); break;
      case 29: write_mux(0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0); break;
      case 30: write_mux(0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0); break;
      case 31: write_mux(1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0); break;
      case 32: write_mux(1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1); break;
      case 33: write_mux(0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1); break;
      case 34: write_mux(0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1); break;
      case 35: write_mux(1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1); break;
      case 36: write_mux(1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0); break;
      case 37: write_mux(0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0); break;
      case 38: write_mux(0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0); break;
      case 39: write_mux(1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0); break;
      case 40: write_mux(0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1); break;
      case 41: write_mux(1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1); break;
      case 42: write_mux(1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1); break;
      case 43: write_mux(0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1); break;
      case 44: write_mux(1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0); break;
      case 45: write_mux(0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0); break;
      case 46: write_mux(0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0); break;
      case 47: write_mux(1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0); break;
      case 48: write_mux(1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1); break;
      case 49: write_mux(0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1); break;
      case 50: write_mux(0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1); break;
      case 51: write_mux(1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1); break;
      case 52: write_mux(1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0); break;
      case 53: write_mux(0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0); break;
      case 54: write_mux(0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0); break;
      case 55: write_mux(1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0); break;
      case 56: write_mux(1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1); break;
      case 57: write_mux(0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1); break;
      case 58: write_mux(0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1); break;
      case 59: write_mux(1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1); break;
      case 60: write_mux(0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1); break;
      case 61: write_mux(1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1); break;
      case 62: write_mux(1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1); break;
      case 63: write_mux(0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1); break;
      case 64: write_mux(1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0); break;
      case 65: write_mux(0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0); break;
      case 66: write_mux(0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0); break;
      case 67: write_mux(1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0); break;
      case 68: write_mux(1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1); break;
      case 69: write_mux(0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1); break;
      case 70: write_mux(0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1); break;
      case 71: write_mux(1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1); break;
      case 72: write_mux(1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0); break;
      case 73: write_mux(0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0); break;
      case 74: write_mux(0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0); break;
      case 75: write_mux(1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0); break;
      case 76: write_mux(1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0); break;
      case 77: write_mux(0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0); break;
      case 78: write_mux(0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0); break;
      case 79: write_mux(1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0); break;
      case 80: write_mux(0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0); break;
      case 81: write_mux(1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0); break;
      case 82: write_mux(1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0); break;
      case 83: write_mux(0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0); break;
      case 84: write_mux(0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1); break;
      case 85: write_mux(1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1); break;
      case 86: write_mux(1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1); break;
      case 87: write_mux(0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1); break;
      case 88: write_mux(1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1); break;
      case 89: write_mux(0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1); break;
      case 90: write_mux(0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1); break;
      case 91: write_mux(1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1); break;
      case 92: write_mux(1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0); break;
      case 93: write_mux(0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0); break;
      case 94: write_mux(0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0); break;
      case 95: write_mux(1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0); break;
      case 96: write_mux(1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1); break;
      case 97: write_mux(0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1); break;
      case 98: write_mux(0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1); break;
      case 99: write_mux(1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1); break;
      case 100: write_mux(1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0); break;
      case 101: write_mux(0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0); break;
      case 102: write_mux(0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0); break;
      case 103: write_mux(1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0); break;
      case 104: write_mux(1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1); break;
      case 105: write_mux(1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1); break;
      case 106: write_mux(0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1); break;
      case 107: write_mux(0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1); break;
      case 108: write_mux(0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0); break;
      case 109: write_mux(0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0); break;
      case 110: write_mux(1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0); break;
      case 111: write_mux(1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0); break;
      case 112: write_mux(0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1); break;
      case 113: write_mux(0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1); break;
      case 114: write_mux(1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1); break;
      case 115: write_mux(1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1); break;
      case 116: write_mux(0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0); break;
      case 117: write_mux(0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0); break;
      case 118: write_mux(1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0); break;
      case 119: write_mux(1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0); break;
      case 120: write_mux(0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1); break;
      case 121: write_mux(0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1); break;
      case 122: write_mux(1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1); break;
      case 123: write_mux(1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1); break;
      case 124: write_mux(1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0); break;
      case 125: write_mux(1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0); break;
      case 126: write_mux(0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0); break;
      case 127: write_mux(0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0); break;
      case 128: write_mux(1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0); break;
      case 129: write_mux(1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0); break;
      case 130: write_mux(0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0); break;
      case 131: write_mux(0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0); break;
      case 132: write_mux(0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1); break;
      case 133: write_mux(0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1); break;
      case 134: write_mux(1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1); break;
      case 135: write_mux(1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1); break;
      case 136: write_mux(1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1); break;
      case 137: write_mux(1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1); break;
      case 138: write_mux(0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1); break;
      case 139: write_mux(0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1); break;
      case 140: write_mux(1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0); break;
      case 141: write_mux(1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0); break;
      case 142: write_mux(0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0); break;
      case 143: write_mux(0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0); break;
      case 144: write_mux(1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1); break;
      case 145: write_mux(1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1); break;
      case 146: write_mux(0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1); break;
      case 147: write_mux(0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1); break;
      case 148: write_mux(1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0); break;
      case 149: write_mux(1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0); break;
      case 150: write_mux(0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0); break;
      case 151: write_mux(0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0); break;
      case 152: write_mux(1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0); break;
      case 153: write_mux(0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0); break;
      case 154: write_mux(0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0); break;
      case 155: write_mux(1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0); break;
      case 156: write_mux(1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0); break;
      case 157: write_mux(0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0); break;
      case 158: write_mux(0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0); break;
      case 159: write_mux(1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0); break;
      case 160: write_mux(0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1); break;
      case 161: write_mux(1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1); break;
      case 162: write_mux(1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1); break;
      case 163: write_mux(0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1); break;
      case 164: write_mux(0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0); break;
      case 165: write_mux(1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0); break;
      case 166: write_mux(1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0); break;
      case 167: write_mux(0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0); break;
      case 168: write_mux(1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1); break;
      case 169: write_mux(0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1); break;
      case 170: write_mux(0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1); break;
      case 171: write_mux(1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1); break;
      case 172: write_mux(0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0); break;
      case 173: write_mux(1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0); break;
      case 174: write_mux(1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0); break;
      case 175: write_mux(0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0); break;
      case 176: write_mux(1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1); break;
      case 177: write_mux(0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1); break;
      case 178: write_mux(0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1); break;
      case 179: write_mux(1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1); break;
      case 180: write_mux(0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0); break;
      case 181: write_mux(1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0); break;
      case 182: write_mux(1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0); break;
      case 183: write_mux(0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0); break;
      case 184: write_mux(0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1); break;
      case 185: write_mux(1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1); break;
      case 186: write_mux(1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1); break;
      case 187: write_mux(0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1); break;
      case 188: write_mux(0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0); break;
      case 189: write_mux(1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0); break;
      case 190: write_mux(1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0); break;
      case 191: write_mux(0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0); break;
      case 192: write_mux(0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1); break;
      case 193: write_mux(1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1); break;
      case 194: write_mux(1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1); break;
      case 195: write_mux(0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1); break;
      case 196: write_mux(1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0); break;
      case 197: write_mux(0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0); break;
      case 198: write_mux(0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0); break;
      case 199: write_mux(1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0); break;
      case 200: write_mux(0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1); break;
      case 201: write_mux(1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1); break;
      case 202: write_mux(1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1); break;
      case 203: write_mux(0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1); break;
      case 204: write_mux(0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0); break;
      case 205: write_mux(1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0); break;
      case 206: write_mux(1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0); break;
      case 207: write_mux(0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0); break;
      case 208: write_mux(0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1); break;
      case 209: write_mux(1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1); break;
      case 210: write_mux(1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1); break;
      case 211: write_mux(0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1); break;
      case 212: write_mux(0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0); break;
      case 213: write_mux(1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0); break;
      case 214: write_mux(1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0); break;
      case 215: write_mux(0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0); break;
      case 216: write_mux(1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1); break;
      case 217: write_mux(0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1); break;
      case 218: write_mux(0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1); break;
      case 219: write_mux(1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1); break;
      case 220: write_mux(0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0); break;
      case 221: write_mux(1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0); break;
      case 222: write_mux(1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0); break;
      case 223: write_mux(0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0); break;
      case 224: write_mux(0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1); break;
      case 225: write_mux(1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1); break;
      case 226: write_mux(1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1); break;
      case 227: write_mux(0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1); break;
      case 228: write_mux(0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0); break;
      case 229: write_mux(1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0); break;
      case 230: write_mux(1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0); break;
      case 231: write_mux(0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0); break;
      case 232: write_mux(0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1); break;
      case 233: write_mux(1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1); break;
      case 234: write_mux(1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1); break;
      case 235: write_mux(0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1); break;
      case 236: write_mux(1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1); break;
      case 237: write_mux(0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0); break;
      case 238: write_mux(0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1); break;
      case 239: write_mux(0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0); break;
      case 240: write_mux(1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1); break;
      case 241: write_mux(0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0); break;
      case 242: write_mux(0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1); break;
      case 243: write_mux(0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0); break;
      case 244: write_mux(0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1); break;
      case 245: write_mux(1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0); break;
      case 246: write_mux(1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1); break;
      case 247: write_mux(1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0); break;
      case 248: write_mux(0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1); break;
      case 249: write_mux(1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0); break;
      case 250: write_mux(1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1); break;
      case 251: write_mux(1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0); break;
      case 252: write_mux(1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1); break;
      case 253: write_mux(1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1); break;
      case 254: write_mux(0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1); break;
      case 255: write_mux(0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1); break;
    }
}
