import sys

import math



QUAD=sys.argv[1]
Number=sys.argv[2]

if "S" in QUAD :
    wedgeangle_deg=8.5
elif "L" in QUAD :
    wedgeangle_deg=14

STRIPPITCH=3.2
WEDGEANGLE=wedgeangle_deg/360.*2.*math.pi

def NumberToPosition(AB, objectNumber):
    position = -999
    if AB=='S':
        position=(int(objectNumber)-9)*(STRIPPITCH/math.cos(WEDGEANGLE))+1.685
    return position

def NumberToPosition_QL3Par(AB, objectNumber):
    position = -999
    if AB=='S':
        position=(352-int(objectNumber)-9)*(STRIPPITCH)+1.685
    return position



if "QL3" in QUAD and int(Number)>170 :
    print ("for Strip number %s , its position to the AB edge ON THE LONG BASE is %s" %(Number, NumberToPosition_QL3Par("S",Number)))
else :
    print ("for Strip number %s , its position to the AB edge is %s" %(Number, NumberToPosition("S",Number)))


