import subprocess
import os
import sys

#Mar_20_sTGC_QS2_PIVOT-WEDGE2-20V_Layer2_P_GFZP1
#May_07_sTGC_QS2_PIVOT-5_Layer2_S_GFZP2

DATAFOLDER='data/ProjectionTest/'
myFolder=sys.argv[1]
UniqueID=''
for PC in ['PIVOT', 'CONFIRM'] : 
    if PC == 'PIVOT' : 
       sPC='.P.'
       pc='P'
    elif PC == 'CONFIRM' : 
       sPC='.C.'
       pc='C'
    if PC not in myFolder and sPC not in myFolder : continue
    print(pc)
    #pc = 'P'
    #for stgc in ['QS2'] :
    for stgc in ['QS1-HV0','QS1-HV1','QS2','QS3'] :
        #for AB in ['P','S'] : 
        if stgc not in myFolder: continue
        print(stgc)
        for AB in ['S','P'] : 
            sAB='_'+AB+'_'
            if sAB not in myFolder: continue
            print(sAB)
            if AB == 'P' : GFZlist = ['P1']
            elif AB == 'S' : GFZlist = ['P1','P2']
            #GFZlist = ['P1']
            for layer in ['1','2','3','4'] :
            #for layer in ['2'] :
                slayer='Layer'+layer
                if slayer not in myFolder: continue
                print(slayer)
                for GFZ in GFZlist : 
                    if stgc == 'QS1': HVlist=['']
                    #if stgc == 'QS1': HVlist=['HV1','HV0']
                    elif stgc == 'QS1-HV0': HVlist=['HV0']
                    elif stgc == 'QS1-HV1': HVlist=['HV1']
                    else: HVlist=['']
                    for HV in HVlist : 
                      print(HV)
                      if (stgc == 'QS1'  or stgc == 'QS1-HV1' ) and slayer == 'Layer1' and AB == 'S' and GFZ == 'P1' and HV == 'HV1': continue 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV0' ) and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV0' ) and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV0' ) and slayer == 'Layer4' and AB == 'S' and GFZ == 'P2' and HV == 'HV0': continue 
                      print('==============')
                      #find my file
    
                      for runFolder in os.listdir(DATAFOLDER):
                          print('hello   ',runFolder)
                          STGC=stgc
                          if stgc == 'QS1-HV0' : STGC = 'QS1'
                          if stgc == 'QS1-HV1' : STGC = 'QS1'
                          print(STGC,' ',slayer,'',sPC,' ',AB,' GFZ',GFZ, ' ', HV, ' ', UniqueID)
                          if STGC in runFolder and slayer in runFolder and ( PC in runFolder or sPC in runFolder ) and '_'+AB+'_' in runFolder and 'GFZ'+GFZ in runFolder and HV in runFolder and UniqueID in runFolder and not 'kk_' in runFolder and not 'good_' in runFolder:
                              print("runFolder= %s" %(runFolder))



                              ## Redo the sorting with the new thresholds
                              #bashCommand = "sudo python PSDVCsorting.py "+runFolder+" 0 "+stgc+" "+pc+" "+AB+" "+GFZ+" "+layer
                              bashCommand = " python PSDVCsorting.py ../"+DATAFOLDER+runFolder+" 0 "+stgc+" "+pc+" "+AB+" "+GFZ+" "+layer
                              print(bashCommand)
                              process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
                              output, error = process.communicate()
                              print(output)
                              print(error)
                              
                           #   #gfzCommand = "sudo python sortedchannelplot.py data/"+runFolder+" "+stgc+" "+pc+" "+AB+" "+GFZ+" "+layer
                           #   gfzCommand = " python sortedchannelplot.py "+DATAFOLDER+runFolder+" "+stgc+" "+pc+" "+AB+" "+GFZ+" "+layer
                           #   print(gfzCommand)
                           #   gfzprocess = subprocess.Popen(gfzCommand.split(), stdout=subprocess.PIPE)
                           #   gfzoutput, gfzerror = gfzprocess.communicate()
                           #   print(gfzoutput)
                           #   print(gfzerror)

                             # grepCommand = "grep -r FAIL data/"+runFolder+"/*"
                             # print(grepCommand)
                             # grepprocess = subprocess.Popen(grepCommand.split(), stdout=subprocess.PIPE)
                             # grepoutput, greperror = grepprocess.communicate()
                             # print(grepoutput)
                             # print(greperror)

