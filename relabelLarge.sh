
for folder in  data/* ; do

  if [[ $folder == *"QL"* ]] ; then  
    
    gasgapfolder="${folder/Layer/GasGap}" 
    echo mv $folder $gasgapfolder

  fi
done

for gasgapfolder in  data/* ; do

  if [[ $gasgapfolder == *"QL"* ]] ; then  

    invertedlayerfolder=""
    if [[ $gasgapfolder == *"GasGap1"* ]] ; then
      invertedlayerfolder="${gasgapfolder/GasGap1/Layer4}" 
    elif [[ $gasgapfolder == *"GasGap2"* ]] ; then
      invertedlayerfolder="${gasgapfolder/GasGap2/Layer3}" 
    elif [[ $gasgapfolder == *"GasGap3"* ]] ; then
      invertedlayerfolder="${gasgapfolder/GasGap3/Layer2}" 
    elif [[ $gasgapfolder == *"GasGap4"* ]] ; then
      invertedlayerfolder="${gasgapfolder/GasGap4/Layer1}" 
    fi

    if [[ $invertedlayerfolder != "" ]] ; then
        echo mv $gasgapfolder $invertedlayerfolder
    fi
  fi
done
