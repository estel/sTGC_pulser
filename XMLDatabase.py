import csv
import os
import xml.etree.ElementTree as ET 

directpath = '/home/userpulser/sTGC_pulser/data'

root = ET.Element("sTGC")

for folder in os.listdir(directpath):
    for csv_file in os.listdir(os.path.join(directpath, folder)):
        if csv_file == 'database.csv':
            with open(os.path.join(directpath, folder, csv_file)) as DBvalues:
                DBreader = csv.reader(DBvalues, delimiter=',', quotechar='|')
                test = str(folder)
                chambertag = test[12:15]
                chamber = ET.SubElement(root, "Chamber")
                chamber.text = chambertag
                quadtag = test[16:26]
                quad = ET.SubElement(chamber, "Quad")
                quad.text = quadtag
                layertag = test[27:32]+' '+test[32]
                layer = ET.SubElement(quad, "Layer")
                layer.text = layertag
                ABtag = test[34]+'AB'
                AB = ET.SubElement(layer, "AB")
                AB.text = ABtag
                GFZtag = test[39:41]
                GFZ = ET.SubElement(AB, "GFZ")
                GFZ.text = GFZtag
                for line in DBreader:
                    if line[0] == 'Channel':
                            chantag = line[0]
                            restag = 'Result'
                            amptag = line[4]
                            resptag = 'Response'
                            avgtag = line[8]
                            vartag = line[10]
                    else:
                        Channel = ET.SubElement(GFZ, chantag)
                        Channel.text = line[0]
                        result = ET.SubElement(Channel, restag)
                        result.text = line[2]
                        amplitude = ET.SubElement(Channel, amptag)
                        amplitude.text = line[4]
                        response = ET.SubElement(Channel, resptag)
                        response.text = line[6]
                        mean = ET.SubElement(Channel, avgtag)
                        mean.text = line[8]
                        variance = ET.SubElement(Channel, vartag)
                        variance.text = line[10]
tree = ET.ElementTree(root)
tree.write('sTGCPulserTestingDatabase.xml')
