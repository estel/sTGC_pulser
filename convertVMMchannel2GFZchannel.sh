#!/bin/bash
# required arguments: (layer, AB, gfz, channel, PC, stgc)

############################################################################
# It requires that you copy the mapping project in the directory above:
# cd ..
# git clone  https://gitlab.cern.ch/McGill-sTGC/NSW_sTGC_Channel_Mapping.git 
############################################################################

#QL1P Strip Layer 1 VMM7 CH22,23
#QL1 P S  1 7 22


stgc=$1
PC=$2
AB=$3
layer=$4
vmmNum=$5
vmmchannel=$6


AB=${AB:0:1}


# Translate AB S/P to strip/pad/wire
if [ $AB == "S" ] 
then 
    AB="strip"
fi
if [ $AB == "P" ] 
then 
    AB="pad"
fi
if [ $AB == "W" ] 
then 
    AB="wire"
fi


# Attach p/c  to the stgc quad type
stgcpc="$stgc$PC"

# Translate layer number into Gas Gap number
ggap=$layer 
if [ $PC == "C" ]
then 
	if [ $layer == "1" ]
	then
		ggap=4
	elif [ $layer == "2" ]
	then	
        	ggap=3
	elif [ $layer == "3" ]
	then
        	ggap=2
	elif [ $layer == "4" ]
	then
        	ggap=1
	fi
fi

#Translate VMMnum from 1 to 8
VMMN=$(( $vmmNum+1 ))


#QS1P	1	strip	1	0	406	406	-1	P2	i0	255

#echo GGAP=$ggap  AB=$AB  VMMN=$VMMN  VMMCH=$vmmchannel  PC=$PC  stgc=$stgc
# Use awk to get the line you need from the mapping text file
myline=$(cat ../NSW_sTGC_Channel_Mapping/mapping.txt | awk  -v GGAP="$ggap" -v AB="$AB"  -v VMMN="$VMMN" -v VMMCH="$vmmchannel" -v PC="$PC" -v stgcpc="$stgcpc" '$1 == stgcpc && $2  == GGAP  && $3 == AB && $4 == VMMN &&  $5 == VMMCH { print }' )
stripnumber=$( echo $myline |  sed -n 's/ \+/ /gp' | cut -d " " -f 6  )
#echo stripnumber= $stripnumber
distance=$( python getPositionLarge.py $stgc $stripnumber) 
#distance=$( python getPosition.py $stripnumber) 

# return the full line
echo $myline $distance

