
import os
import sys
import csv
import statistics
import numpy as np
import matplotlib.pyplot as plt
from QABmap import getQuanUniqueID
from QABmap import getFolderUniqueName
#from Beep import finished
from matplotlib.ticker import FormatStrFormatter


# give readable names to the arguments
makePlots = sys.argv[2] # not used so far
stgc = sys.argv[3]
PC = sys.argv[4]
AB = sys.argv[5]
gfz = sys.argv[6]
layer = sys.argv[7]

REFdata='/home/userpulser/REFdata'
MYfilename = 'data/'+sys.argv[1]+'/database.csv'    #Write to CSV for GFZ Mapping
REFfilename='NULL'
quadFolder='NULL'
if 'data' in MYfilename :
    print(" data in myfilename found")
#look for the corresponding reference file
## From the database
quadUniqueID=getQuanUniqueID(MYfilename, stgc)
print("quadUniqueID = %s"%(quadUniqueID))
# Look for the corresponding reference file 
if 'HV0' in MYfilename: HV='HV0_'
elif 'HV1' in MYfilename: HV='HV1_'
else: HV=''
for runFolder in os.listdir(REFdata+'/'):
    #Compare HV0 and HV1 to the same reference file QS1. #if stgc in runFolder and 'Layer'+layer in runFolder and gfz in runFolder and '_'+AB+'_' in runFolder and quadUniqueID in runFolder and HV in runFolder: 
    print("try runFolder = %s" %(runFolder))    
    if stgc in runFolder and 'Layer'+layer in runFolder and gfz in runFolder and '_'+AB+'_' in runFolder and quadUniqueID in runFolder : 
        if 'kk_' in runFolder : continue
        print("FOUND! runFolder = %s" %(runFolder))    
        REFfilename=REFdata+'/'+runFolder+'/database.csv'

RefFileExists=False
if os.path.exists(REFfilename) and os.path.getsize(REFfilename) > 0:
    # Non empty file exists
    print('Reference file = %s exists and has non-zero size.' %(REFfilename))
    RefFileExists = True
else:
    # Non existent file
    print('Reference file = %s does not exist or has zero size.' %(REFfilename))

# Get data from your file 
print("MYfilename = %s" %(MYfilename))
with open(MYfilename) as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    MYxs = []
    MYAmplitude = []
    MYResponse = []
    MYVariance = []
    firstrow = True
    for row in readCSV:
        if firstrow : 
            firstrow = False
            continue
        channel = row[0]
        amplitude = row[4]
        response = row[6]
        variance = row[10]

        MYxs.append(int(channel))
        MYAmplitude.append(float(amplitude))
        MYResponse.append(float(response))
        MYVariance.append(float(variance))
        #print('channel= %s  Amplitude= %s' %(channel, amplitude))

# Get data from the reference file
if RefFileExists :
    with open(REFfilename) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        REFxs = []
        REFAmplitude = []
        REFResponse = []
        REFVariance = []
        firstrow = True
        for row in readCSV:
            if firstrow : 
                firstrow = False
                continue
            channel = row[0]
            amplitude = row[4]
            response = row[6]
            variance = row[10]
    
            REFxs.append(int(channel))
            REFAmplitude.append(float(amplitude))
            REFResponse.append(float(response))
            REFVariance.append(float(variance))
            #print('channel= %s  Amplitude= %s' %(channel, amplitude))

#print(len( MYxs))
#print( len( MYAmplitude))

#Create amplitude distribution plot
#Build universal unique name for title and output file
outfilename =  getFolderUniqueName(MYfilename, stgc, HV, layer, AB, gfz)


fig, ax = plt.subplots(figsize=(4+20*(MYxs[len(MYxs)-1]-MYxs[0]) /256,6)) # scale the canvas X size depending on the channels we are looking at
plt.plot(MYxs, MYAmplitude, 'b-',label='Signal Amplitude - CERN')
plt.title(outfilename)
if RefFileExists :
    plt.plot(REFxs, REFAmplitude, '-', color='gray', label='Signal Amplitude - REFERENCE')
#plt.plot(xs, Variance, label='Signal Variance') #no need to plot the variance
#plt.plot(xs, Response, label='Sorting Function Response') #no need to plot the response (might have been calculated differently)
plt.xlabel('Readout Element (GFZ Channel Number)')
plt.ylabel('Voltage (V)')
plt.grid(True)

# Don't allow the axis to be on top of your data
ax.set_axisbelow(True)
# Turn on the minor TICKS, which are required for the minor GRID
ax.minorticks_on()
# Customize the major grid
ax.grid(which='major', linestyle='-', linewidth='0.5', color='silver')
# Customize the minor grid
#ax.grid(which='minor', linestyle=':', linewidth='0.5', color='gray')

ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
#plt.axis([ , ,ymin=0, ymax=0.12])
axes = plt.gca()
plt.ylim(bottom=0)

#Save
plt.savefig('compare/'+outfilename+'.png')

#print("outfilename = %s" %(outfilename))
plt.show() # TODO comment out if offline reprocessing


