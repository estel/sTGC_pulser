import subprocess
import os
import sys
from QABmap import getFolderUniqueName
import errno    
from foldernameDB import getFoldersList

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

OPTION=''
wedge=''
if len(sys.argv)>1 : OPTION=sys.argv[1]
if len(sys.argv)>2 : wedge=sys.argv[2]
else: print('usage: python testCompare.py OPTION [compare, gfz, bad] WEDGE')
DATAFOLDER='data/' #sys.argv[1]

#wedge='QS3.P.1'
#wedge='QS2.P.4'
#wedge='QS1.P.8'
#wedge='QS3.P.8'
#wedge='QS2.P.5'
#wedge='QS2.P.10'
#wedge='QS1.P.10'
#wedge='QS1.P.7'
#wedge='QS2.P.6'
#wedge='QS2.C.10'

if wedge == '' :
    wedge='QS2.P.6'
    print("wedge set to %s by default"%(wedge))
    print('usage: python testCompare.py OPTION [compare, gfz, bad] WEDGE')


if 'Q' in wedge:
    quad_split=wedge.rsplit('.', -1)
    if len(quad_split) < 3 :
        print("Something is wrong, wedge contains 'Q' but is not of the format X.Y.Z ... wedge= %s"%(wedge))
    mypc=[quad_split[1]]
    mystgc=[quad_split[0]]
       
else:
    mypc=['P', 'C']
    mystgc=['QS1-HV0','QS1-HV1','QS2','QS3', 'QL1-HV0', 'QL1-HV1', 'QL2', 'QL3']

##UniqueID='WEDGE2'
##UniqueID='QS2_PIVOT-4'
#UniqueID='QS3_PIVOT-DISHY'
##UniqueID='WEDGE1-HVFILTER'
for pc in mypc : 
#for PC in ['PIVOT'] : 
#for PC in ['CONFIRM'] : 
    if pc=='P': PC='PIVOT'
    if pc=='C': PC='CONFIRM'
    #pc='P'
    #pc='C'
    #for stgc in ['QS1-HV0','QS1-HV1','QS2','QS3'] :
    #for stgc in ['QS1','QS2','QS3'] :
    for stgc in mystgc :
    #for stgc in ['QS1-HV0','QS1-HV1'] :
        #for layer in ['2'] :
        for layer in ['1','2','3','4'] :
            slayer='Layer'+layer
            #for AB in ['S'] : 
            for AB in ['P','S'] : 
                if AB == 'P' : GFZlist = ['P1']
                elif AB == 'S' : GFZlist = ['P1','P2']
                for GFZ in GFZlist : 
                    if stgc == 'QS1' or stgc == 'QL1': HVlist=['HV1','HV0']
                    else: HVlist=['']
                    for HV in HVlist : 
                      if pc=='P':
                          if stgc == 'QS1' and slayer == 'Layer1' and AB == 'S' and GFZ == 'P1' and HV == 'HV1': continue 
                          if stgc == 'QS1' and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                          if stgc == 'QS1' and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                          if stgc == 'QS1' and slayer == 'Layer4' and AB == 'S' and GFZ == 'P2' and HV == 'HV0': continue 

                          if stgc == 'QL1' and slayer == 'Layer1' and AB == 'S' and GFZ == 'P1' and HV == 'HV1': continue 
                          if stgc == 'QL1' and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                          if stgc == 'QL1' and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                          if stgc == 'QL1' and slayer == 'Layer4' and AB == 'S' and GFZ == 'P2' and HV == 'HV0': continue 

                          if stgc == 'QS1-HV1' and slayer == 'Layer1' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QS1-HV0' and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QS1-HV0' and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QS1-HV0' and slayer == 'Layer4' and AB == 'S' and GFZ == 'P2' : continue 

                          if stgc == 'QL1-HV1' and slayer == 'Layer1' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QL1-HV0' and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QL1-HV0' and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QL1-HV0' and slayer == 'Layer4' and AB == 'S' and GFZ == 'P2' : continue 

                      if pc=='C': 
                          if stgc == 'QS1' and slayer == 'Layer4' and AB == 'S' and GFZ == 'P1' and HV == 'HV1': continue 
                          if stgc == 'QS1' and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                          if stgc == 'QS1' and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                          if stgc == 'QS1' and slayer == 'Layer1' and AB == 'S' and GFZ == 'P2' and HV == 'HV0': continue 

                          if stgc == 'QL1' and slayer == 'Layer4' and AB == 'S' and GFZ == 'P1' and HV == 'HV1': continue 
                          if stgc == 'QL1' and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                          if stgc == 'QL1' and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                          if stgc == 'QL1' and slayer == 'Layer1' and AB == 'S' and GFZ == 'P2' and HV == 'HV0': continue 

                          if stgc == 'QS1-HV1' and slayer == 'Layer4' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QS1-HV0' and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QS1-HV0' and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QS1-HV0' and slayer == 'Layer1' and AB == 'S' and GFZ == 'P2' : continue 

                          if stgc == 'QL1-HV1' and slayer == 'Layer4' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QL1-HV0' and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QL1-HV0' and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' : continue 
                          if stgc == 'QL1-HV0' and slayer == 'Layer1' and AB == 'S' and GFZ == 'P2' : continue 

                      if 'HV0' in stgc : HV='HV0'
                      if 'HV1' in stgc : HV='HV1'

                      print('============== %s %s'%(stgc, HV))
                      #find my file
    
#                      for runFolder in os.listdir(DATAFOLDER):
#                          if stgc in runFolder and slayer in runFolder and PC in runFolder and '_'+AB+'_' in runFolder and 'GFZ'+GFZ in runFolder and HV in runFolder and UniqueID in runFolder:
#                              if 'V-' in runFolder or 'V_'in runFolder or 'ZOOMOUT' in runFolder or 'kk_' in runFolder or 'good_' in runFolder or 'old_' in runFolder: continue
#                              print("runFolder= %s" %(runFolder))



                      print (getFoldersList (wedge, '', int(layer), AB, GFZ, HV) )
                      for runFolder in getFoldersList (wedge, '', int(layer), AB, GFZ, HV) :
                      #print (getFoldersList (wedge, '','','','','') )
                      #for runFolder in getFoldersList (wedge, '','','','','') :
                              


    
                              if 'compare' in OPTION:
                                  comp = ['python', 'compareAmplitudeVsChannel.py', runFolder, '0',  stgc, PC,  AB, GFZ, layer]
                                  out = subprocess.check_call(comp)
                                  outFolderUniqueName=getFolderUniqueName(runFolder, stgc, HV, int(layer), AB, GFZ)
                                  print("out %s outFolderUniqueName= %s" %(out, outFolderUniqueName))

                              if 'gfz' in OPTION:
                                  #copy and rename the GFZ plots to be easily included in the latex summary file
                                  mkdir_p("GFZplots/"+outFolderUniqueName) #TODO to be tested
                                  print("cp %s%s/GFZmap.png GFZplots/%s_GFZmap.png" %(DATAFOLDER, runFolder,outFolderUniqueName))
                                  copy = ['cp',DATAFOLDER+runFolder+'/GFZmap.png','GFZplots/'+outFolderUniqueName+'_GFZmap.png']
                                  subprocess.check_call(copy)

                              if 'bad' in OPTION:
                                  bad = ['python', 'identifyBadChannels.py', runFolder, '0',  stgc, pc,  AB, GFZ, layer]
                                  outbad = subprocess.check_call(bad)
                                  outFolderUniqueName=getFolderUniqueName(runFolder, stgc, HV, int(layer), AB, GFZ)
                                  #print("out %s outFolderUniqueName= %s" %(outbad, outFolderUniqueName))
