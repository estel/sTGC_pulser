import sys
import csv
import numpy
import matplotlib.pyplot as plt

outFile = open('sortedpulserdata.csv', 'w')
outFileWriter = csv.writer(outFile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
varianceThreshold = 0.00001

makePlots = '0'

with open('rawOutput.csv') as inputRawValues:
    rawReader = csv.reader(inputRawValues, delimiter=',', quotechar='|')
    channel = 0
    chan = []
    for line in rawReader:
        if len(line) == 1: #channel number line, only one element
            channel = int(line[0]) #get channel num
            chan.append(channel)
            if chan[0] != 0:
                high = 0.004
            else:
                high = 0.0001
        else: #data line, many elements
            line = [float(i) for i in line] #convert strings to floats
            var = numpy.var(line) #compute variance 
            maximum = max(line) 
            r1 = int(maximum*1000)
            r2 = int((maximum-0.009)*1000)
            if makePlots == '0':
                plt.plot(line)
                plt.savefig('channel' + str(channel) + '.png')
                plt.close()
            if(var > high):
                outFileWriter.writerow([int(channel)]+[3])
                print channel, var
            elif varianceThreshold < var < 0.000015:
                outFileWriter.writerow([int(channel)]+[1])
                print 'channel '+ str(channel)+' - low vairance: '+str(var)
            elif var > varianceThreshold:
                outFileWriter.writerow([int(channel)]+[2])
                print channel, var
            elif var < varianceThreshold:
                outFileWriter.writerow([int(channel)]+[0])
                print 'channel '+str(channel)+' Failed: '+str(var)
            # else:
            #     print channel, var
            # if(var > varianceThreshold): #signal
            #     outFileWriter.writerow([int(channel)]+[1])
            # else: #noise
            #     outFileWriter.writerow([int(channel)]+[0])
            # for v in line:
            #     if ((v*1000) > (r1-5)):
            #         print 'channel '+str(channel), v
            #         # plt.plot(v)
            #         # plt.savefig('channel' + str(channel) + 'max.png')
            #         # plt.close()