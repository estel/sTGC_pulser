echo "Usage: ./convertXLS2MAP.sh <QUAD>"
echo "Example: ./convertXLS2MAP.sh QS1"
echo "Output: QS1map.py "

QUAD="QL1"
#QUAD=$1 #"QS1"


echo "Selected QUAD = "$QUAD
AB="P"

#STRFILE="sTGC_AB_MappingSTRIP.csv"
PADFILE="sTGC_AB_MappingPAD_QL1.csv"
#GFZ="P1"
#PIN="i0"
#LAYER=1

OUTFILE="${QUAD}map_new.py"
rm $OUTFILE

#for AB in "S" "P" "W" ; do 
for AB in "P"  ; do 
  if [ "$AB" == "S" ] ; then
      TMPFILE="${QUAD}_${AB}.txt"
      rm $TMPFILE
      echo "STRMAP${QUAD} = {" >> $TMPFILE
      for GFZ in "P1" "P2" ; do 
          for pinLetter in "i" "j" ; do 
              for pinNumber in {0..127} ; do 
                  PIN="${pinLetter}${pinNumber}"
                  #echo $PIN
                  if [ "$QUAD" == "QS1" ] ; then  grep $GFZ $STRFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$7","$8","$9","$10"]," }' >> ${TMPFILE}; fi
                  if [ "$QUAD" == "QS2" ] ; then  grep $GFZ $STRFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$12","$13","$14","$15"]," }' >> ${TMPFILE}; fi
                  if [ "$QUAD" == "QS3" ] ; then  grep $GFZ $STRFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$17","$18","$19","$20"]," }' >> ${TMPFILE}; fi
                  if [ "$QUAD" == "QL1" ] ; then  grep $GFZ $STRFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$22","$23","$24","$25"]," }' >> ${TMPFILE}; fi
                  if [ "$QUAD" == "QL2" ] ; then  grep $GFZ $STRFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$27","$28","$29","$30"]," }' >> ${TMPFILE}; fi
                  if [ "$QUAD" == "QL3" ] ; then  grep $GFZ $STRFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$32","$33","$34","$35"]," }' >> ${TMPFILE}; fi
              done
          done
      done
      echo "}" >> $TMPFILE
      grep -v "\[-1,-1,-1,-1\]" ${TMPFILE} >> $OUTFILE
      rm $TMPFILE
  fi
  
  if [ "$AB" == "P" ] ; then
      for PC in "P" "C" ; do 
          TMPFILE="${QUAD}_${AB}_${PC}.txt"
          rm $TMPFILE
          echo "PADMAP${QUAD}${PC} = {" >> $TMPFILE
            for GFZ in "P1" ; do 
                for pinLetter in "j" ; do 
                    #for pinNumber in {8..119} ; do 
                    for pinNumber in {0..120} ; do 
                        PIN="${pinLetter}${pinNumber}"
                        #echo $PIN
                        if [ "$QUAD" == "QS1" ] && [ "$PC" == "P" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$7","$8","$9","$10"]," }' >> ${TMPFILE}; fi
                        if [ "$QUAD" == "QS1" ] && [ "$PC" == "C" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$12","$13","$14","$15"]," }' >> ${TMPFILE}; fi
                        if [ "$QUAD" == "QS2" ] && [ "$PC" == "P" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$17","$18","$19","$20"]," }' >> ${TMPFILE}; fi
                        if [ "$QUAD" == "QS2" ] && [ "$PC" == "C" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$22","$23","$24","$25"]," }' >> ${TMPFILE}; fi
                        if [ "$QUAD" == "QS3" ] && [ "$PC" == "P" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$27","$28","$29","$30"]," }' >> ${TMPFILE}; fi
                        if [ "$QUAD" == "QS3" ] && [ "$PC" == "C" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$32","$33","$34","$35"]," }' >> ${TMPFILE}; fi
  
                        if [ "$QUAD" == "QL1" ] && [ "$PC" == "P" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$37","$38","$39","$40"]," }' >> ${TMPFILE}; fi
                        if [ "$QUAD" == "QL1" ] && [ "$PC" == "C" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$42","$43","$44","$45"]," }' >> ${TMPFILE}; fi
                        if [ "$QUAD" == "QL2" ] && [ "$PC" == "P" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$47","$48","$49","$50"]," }' >> ${TMPFILE}; fi
                        if [ "$QUAD" == "QL2" ] && [ "$PC" == "C" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$52","$53","$54","$55"]," }' >> ${TMPFILE}; fi
                        if [ "$QUAD" == "QL3" ] && [ "$PC" == "P" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$57","$58","$59","$60"]," }' >> ${TMPFILE}; fi
                        if [ "$QUAD" == "QL3" ] && [ "$PC" == "C" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$62","$63","$64","$65"]," }' >> ${TMPFILE}; fi
                    done
                done
            done
          echo "}" >> $TMPFILE
          cat $TMPFILE | sed s/,,/,-1,/g | sed s/,,/,-1,/g | sed s/\\[,/[-1,/g | sed s/,\\]/,-1]/g | grep -v "\[-1,-1,-1,-1\]" >> $OUTFILE
          rm $TMPFILE
      done
  fi
  
  if [ "$AB" == "W" ] ; then
      TMPFILE="${QUAD}_${AB}.txt"
      rm $TMPFILE
      echo "WIREMAP${QUAD} = {" >> $TMPFILE
      for GFZ in "P1" ; do 
          for pinLetter in "i" ; do 
              for pinNumber in {0..57} ; do 
                  PIN="${pinLetter}${pinNumber}"
                  #echo $PIN
                  if [ "$QUAD" == "QS1" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$7","$8","$9","$10"]," }' >> ${TMPFILE}; fi
                  if [ "$QUAD" == "QS2" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$17","$18","$19","$20"]," }' >> ${TMPFILE}; fi
                  if [ "$QUAD" == "QS3" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$27","$28","$29","$30"]," }' >> ${TMPFILE}; fi
                  if [ "$QUAD" == "QL1" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$37","$38","$39","$40"]," }' >> ${TMPFILE}; fi
                  if [ "$QUAD" == "QL2" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$47","$48","$49","$50"]," }' >> ${TMPFILE}; fi
                  if [ "$QUAD" == "QL3" ] ; then  grep $GFZ $PADFILE | grep "${PIN}," | awk -F, '{ print "('"'"${GFZ}"'"','"'"${PIN}"'"'):["$57","$58","$59","$60"]," }' >> ${TMPFILE}; fi
              done
          done
      done
      echo "}" >> $TMPFILE
      #grep -v "\[,,,\]" ${TMPFILE} >> $OUTFILE
      cat $TMPFILE | sed s/,,/,-1,/g | sed s/,,/,-1,/g | sed s/\\[,/[-1,/g | sed s/,\\]/,-1]/g | grep -v "\[-1,-1,-1,-1\]" >> $OUTFILE
      rm $TMPFILE
  fi
  
done


