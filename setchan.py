import serial
import time

def connect():
    try:
        arduino = serial.Serial('/dev/ttyUSB0', 9600)
    except serial.serialutil.SerialException:
        try:
            arduino = serial.Serial('/dev/ttyUSB1', 9600)
        except serial.serialutil.SerialException:
            try:
                arduino = serial.Serial('/dev/ttyUSB2', 9600)
            except serial.serialutil.SerialException:
                print "Arduino not connected"
 
    time.sleep(1)
    return arduino

def setChannel(channel, arduino):
    arduino.write(channel+"\n")
    time.sleep(2)
