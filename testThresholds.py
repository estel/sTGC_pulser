import subprocess
import os
import csv
import matplotlib.pyplot as plt
import numpy as np
from QABmap import IsValidChannel
from QABmap import IsPad
from QABmap import IsWire


DATAFOLDER='data/' #sys.argv[1]
UniqueID='WEDGE2'
MYAmplitudeQS1S = []
MYAmplitudeQS1P = []
MYAmplitudeQS1W = []
MYAmplitudeQS1HV0S = []
MYAmplitudeQS1HV0P = []
MYAmplitudeQS1HV0W = []
MYAmplitudeQS1HV1S = []
MYAmplitudeQS1HV1P = []
MYAmplitudeQS1HV1W = []
MYAmplitudeQS2S = []
MYAmplitudeQS2P = []
MYAmplitudeQS2W = []
MYAmplitudeQS3S = []
MYAmplitudeQS3P = []
MYAmplitudeQS3W = []
for PC in ['PIVOT'] : 
    pc = 'P'
    for stgc in ['QS1-HV0','QS1-HV1','QS2','QS3'] :
    #for stgc in ['QS1','QS2','QS3'] :
        for AB in ['P','S'] : 
            if AB == 'P' : GFZlist = ['P1']
            elif AB == 'S' : GFZlist = ['P1','P2']
            for layer in ['1','2','3','4'] :
                slayer='Layer'+layer
                for GFZ in GFZlist : 
                    if stgc == 'QS1': HVlist=['HV1','HV0']
                    elif stgc == 'QS1-HV0': HVlist=['HV0']
                    elif stgc == 'QS1-HV1': HVlist=['HV1']
                    else: HVlist=['']
                    for HV in HVlist : 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV1' ) and slayer == 'Layer1' and AB == 'S' and GFZ == 'P1' and HV == 'HV1': continue 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV0' ) and slayer == 'Layer2' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV0' ) and slayer == 'Layer3' and AB == 'S' and GFZ == 'P1' and HV == 'HV0': continue 
                      if (stgc == 'QS1'  or stgc == 'QS1-HV0' ) and slayer == 'Layer4' and AB == 'S' and GFZ == 'P2' and HV == 'HV0': continue 
                      print('==============')
                      #find my file
    
                      for runFolder in os.listdir(DATAFOLDER):
                          STGC=stgc
                          if stgc == 'QS1-HV0' : STGC = 'QS1'
                          if stgc == 'QS1-HV1' : STGC = 'QS1'
                          if STGC in runFolder and slayer in runFolder and PC in runFolder and '_'+AB+'_' in runFolder and 'GFZ'+GFZ in runFolder and HV in runFolder and UniqueID in runFolder and not 'kk_' in runFolder and not 'good_' in runFolder:
                              print("runFolder= %s" %(runFolder))

                              ## Redo the sorting with the new thresholds
                              pbc = ['python', 'pulserBoardControl.py', runFolder, stgc, pc, AB, GFZ, layer]
                              try:
                                  subprocess.check_call(pbc)
                              except:
                                  n=1
    
                              MYfilename = DATAFOLDER+'/'+runFolder+'/database.csv'
                              with open(MYfilename) as csvfile:
                                  readCSV = csv.reader(csvfile, delimiter=',')
                                  firstrow = True
                                  for row in readCSV:
                                      if firstrow : 
                                          firstrow = False
                                          continue
                                      amplitude = row[4]
                                      channel = row[0]
                                      #print('IsValidChannel= %s %s' %(channel, IsValidChannel(stgc, AB, int(layer), GFZ, int(channel), pc)))
                                      if not IsValidChannel(stgc, AB, int(layer), GFZ, int(channel), pc): continue
                              
                                      if (stgc =='QS1' or stgc =='QS1-HV0' or stgc =='QS1-HV1' ) and AB == 'S' : MYAmplitudeQS1S.append(float(amplitude))
                                      elif (stgc =='QS1' or stgc =='QS1-HV0' or stgc =='QS1-HV1' ) and AB == 'P' and IsPad(stgc, AB, int(layer), GFZ, int(channel), pc): MYAmplitudeQS1P.append(float(amplitude))
                                      elif (stgc =='QS1' or stgc =='QS1-HV0' or stgc =='QS1-HV1' ) and AB == 'P' and IsWire(stgc, AB, int(layer), GFZ, int(channel),pc):    MYAmplitudeQS1W.append(float(amplitude))

                                      elif (stgc =='QS2') and AB == 'S' : MYAmplitudeQS2S.append(float(amplitude))
                                      elif (stgc =='QS2') and AB == 'P' and IsPad(stgc, AB, int(layer), GFZ, int(channel), pc) : MYAmplitudeQS2P.append(float(amplitude))
                                      elif (stgc =='QS2') and AB == 'P' and IsWire(stgc, AB, int(layer), GFZ, int(channel), pc) : MYAmplitudeQS2W.append(float(amplitude))

                                      elif (stgc =='QS3') and AB == 'S' : MYAmplitudeQS3S.append(float(amplitude))
                                      elif (stgc =='QS3') and AB == 'P' and IsPad(stgc, AB, int(layer), GFZ, int(channel), pc): MYAmplitudeQS3P.append(float(amplitude))
                                      elif (stgc =='QS3') and AB == 'P' and IsWire(stgc, AB, int(layer), GFZ, int(channel), pc) : MYAmplitudeQS3W.append(float(amplitude))

                                      if (stgc =='QS1-HV1' ) and AB == 'S' : MYAmplitudeQS1HV1S.append(float(amplitude))
                                      elif (stgc =='QS1-HV1' ) and AB == 'P' and IsPad(stgc, AB, int(layer), GFZ, int(channel), pc) : MYAmplitudeQS1HV1P.append(float(amplitude))
                                      elif (stgc =='QS1-HV1' ) and AB == 'P' and IsWire(stgc, AB, int(layer), GFZ, int(channel), pc) : MYAmplitudeQS1HV1W.append(float(amplitude))

                                      elif (stgc =='QS1-HV0') and AB == 'S' : MYAmplitudeQS1HV0S.append(float(amplitude))
                                      elif (stgc =='QS1-HV0') and AB == 'P' and IsPad(stgc, AB, int(layer), GFZ, int(channel), pc) : MYAmplitudeQS1HV0P.append(float(amplitude))
                                      ## DOESNOT EXIST##elif (stgc =='QS1-HV0') and AB == 'P' and IsWire(stgc, AB, int(layer), GFZ, int(channel)) : MYAmplitudeQS1HV0W.append(float(amplitude))

        print("finish %s" %(stgc))
    minimum=0
    maximum=0.08
    binwidth = 0.001

    for spw in ['Strip','Pad','Wire'] :
        fig, ax = plt.subplots() # 
        if spw == 'Strip':
            plt.hist(MYAmplitudeQS1HV0S, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS1-HV0 Strip Amplitude', color='deeppink' , alpha=0.4)
            plt.hist(MYAmplitudeQS1HV1S, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS1-HV1 Strip Amplitude', color='m' , alpha=0.4)
            plt.hist(MYAmplitudeQS2S, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS2 Strip Amplitude', color='darkorange' , alpha=0.4)
            plt.hist(MYAmplitudeQS3S, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS3 Strip Amplitude', color='red' , alpha=0.4)
        elif spw == 'Pad':
            plt.hist(MYAmplitudeQS1HV0P, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS1-HV0 Pad Amplitude', color='darkolivegreen' , alpha=0.4)
            plt.hist(MYAmplitudeQS1HV1P, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS1-HV1 Pad Amplitude', color='darkturquoise' , alpha=0.4)
            plt.hist(MYAmplitudeQS2P, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS2 Pad Amplitude', color='blue' , alpha=0.4)
            plt.hist(MYAmplitudeQS3P, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS3 Pad Amplitude', color='green' , alpha=0.4)
        elif spw == 'Wire':
            plt.hist(MYAmplitudeQS1HV1W, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS1-HV1 Wire Amplitude', color='orangered' , alpha=0.4)
            plt.hist(MYAmplitudeQS2W, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS2 Wire Amplitude', color='yellowgreen' , alpha=0.4)
            plt.hist(MYAmplitudeQS3W, bins=np.arange(minimum, maximum + binwidth, binwidth), label='QS3 Wire Amplitude', color='blueviolet' , alpha=0.4)
    
        plt.title('Amplitude histogram '+spw)
        ax.legend()
        plt.xlabel('Vpp Amplitude [V]')
        plt.ylabel('Frequency')
        plt.grid(True)
    
        # Don't allow the axis to be on top of your data
        ax.set_axisbelow(True)
        ## Turn on the minor TICKS, which are required for the minor GRID
        ax.minorticks_on()
        ## Customize the major grid
        ax.grid(which='major', linestyle='-', linewidth='0.5', color='silver')
        ## Customize the minor grid
        ax.grid(which='minor', linestyle=':', linewidth='0.5', color='gray')
        #

        ##Save
        plt.savefig('frequency/'+spw+'.png')
        
        #print("outfilename = %s" %(outfilename))
        plt.show()

 

