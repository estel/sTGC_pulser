'''
this file generates a GUI corresponding to a graphical illustration of the channel amplitudes for an entire strip. 
after the user specifies quad details on the GUI, clicks RUN, then closes the window, a graph will pop up. 
the graph will plot both the amplitudes of the specified layer's SP1 and SP2, with reference to Alam's channel mapping.
'''

import os
import sys
import csv
import statistics
import numpy as np
import matplotlib.pyplot as plt
from QABmap import getQuanUniqueID
from QABmap import getFolderUniqueName
#from Beep import finished
from matplotlib.ticker import FormatStrFormatter
from foldernameDB import getFoldersList
from QABmap import Qmap
from tkMessageBox import showinfo
import Tkinter as tk
import subprocess

window = tk.Tk() #window is an instance of tkinter's Tk class
window.title('Combined Strips')
def show_values():
	stgc.get()
	PC.get()
	layer.get()
	uid.get()
	window.destroy()

#frame 1 (var=stgc)
frm1 = tk.Frame(master=window)
frm1.grid(padx=100, pady=20)
lbl_1 = tk.Label(master=frm1, text='Quad Type:')

stgc = tk.StringVar()

bttn_ql1_hv1 = tk.Radiobutton(master=frm1,text='QL1-HV1', variable=stgc, value='QL1-HV1')
bttn_ql1_hv0 = tk.Radiobutton(master=frm1,text='QL1-HV0', variable=stgc, value='QL1-HV0')
bttn_ql2 = tk.Radiobutton(master=frm1,text='QL2', variable=stgc, value='QL2')
bttn_ql3 = tk.Radiobutton(master=frm1,text='QL3', variable=stgc, value='QL3')
bttn_qs1_hv1 = tk.Radiobutton(master=frm1, text='QS1-HV1', variable=stgc, value='QS1-HV1')
bttn_qs1_hv0 = tk.Radiobutton(master=frm1,text='QS1-HV0', variable=stgc, value='QS1-HV0')
bttn_qs2 = tk.Radiobutton(master=frm1,text='QS2', variable=stgc, value='QS2')
bttn_qs3 = tk.Radiobutton(master=frm1,text='QS3', variable=stgc, value='QS3')

lbl_1.grid(row=0,column=0)
bttn_ql1_hv1.grid(row=0,column=1)
bttn_ql1_hv0.grid(row=0,column=2)
bttn_ql2.grid(row=0,column=3)
bttn_ql3.grid(row=0,column=4)
bttn_qs1_hv1.grid(row=0,column=5)
bttn_qs1_hv0.grid(row=0,column=6)
bttn_qs2.grid(row=0,column=7)
bttn_qs3.grid(row=0,column=8)

#frame 2 (var=PC)
frm2 = tk.Frame(master=window)
frm2.grid(padx=150, pady=20)

PC = tk.StringVar()

lbl_2 = tk.Label(master=frm2, text='Pivot or Confirm:')
bttn_piv = tk.Radiobutton(master=frm2, text='Pivot', variable=PC, value='PIVOT')
bttn_conf = tk.Radiobutton(master=frm2, text='Confirm', variable=PC, value='CONFIRM')

lbl_2.grid(row=1, column=0)
bttn_piv.grid(row=1, column=1)
bttn_conf.grid(row=1, column=2)

#frame 3 (var=uid)
frm3 = tk.Frame(master=window)
frm3.grid(padx=150, pady=20)

uid = tk.StringVar()

lbl_3 = tk.Label(master=frm3, text='Unique Identifier:')
ent_uid = tk.Entry(master=frm3, textvariable=uid)

lbl_3.grid(row=2, column=0)
ent_uid.grid(row=2,column=1)

#frame 4 (var=layer)
frm4 = tk.Frame(master=window)
frm4.grid(padx=100, pady=20)

layer = tk.IntVar()

lbl_4 = tk.Label(master=frm4, text='Layer:')
bttn_lay1 = tk.Radiobutton(master=frm4, text='1', variable=layer, value=1)
bttn_lay2 = tk.Radiobutton(master=frm4, text='2', variable=layer, value=2)
bttn_lay3 = tk.Radiobutton(master=frm4, text='3', variable=layer, value=3)
bttn_lay4 = tk.Radiobutton(master=frm4, text='4', variable=layer, value=4)

lbl_4.grid(row=3,column=0)
bttn_lay1.grid(row=3,column=1)
bttn_lay2.grid(row=3,column=2)
bttn_lay3.grid(row=3,column=3)
bttn_lay4.grid(row=3, column=4)


#frame 8
frm8 = tk.Frame(master=window)
frm8.grid(padx=20, pady=20)

RunBttn = tk.Button(master=frm8, text='RUN', command=show_values)
RunBttn.grid(row=7, column=0)
window.mainloop()

 #makes this into an 'event loop'. program won't run again until tk window is closed.
#**************************
#COMBINED STRIPS CODE BELOW
#**************************

stgc='QL1'
layer=2
channel=0
PC = 'P'
uid='-9_'
AB='S'
'''
stgc=stgc.get()
layer=layer.get()
channel=0
PC = PC.get()
uid=str('-' + uid.get() +'_')
AB='S'
'''
def runAgain():
	subprocess.check_call(['sudo', 'python', 'Mac_combined_strips.py'])

def combined_strips(layer, stgc, PC, uid, AB):
	folderList=getFoldersList( "" , stgc , layer, AB ,"", "")
	#objectNumber=Qmap(int(layer), AB, gfz, int(channel), PC, stgc, 0) # 0:do not print out	
	
	if len(folderList) == 0:
		print('There is no data attributed with that Quad name.')
		quit()
	
	#first plot it for all the other quads
	otherQuads = []
	if PC == 'P':
		for folder in folderList:
			if 'PIVOT' in folder:	
				otherQuads.append('data/' + str(folder) + '/database.csv')
	else:
		for folder in folderList:
			if 'CONFIRM' in folder:
				otherQuads.append('data/' + str(folder) + '/database.csv')
	
	myStrips = []
	for folder in otherQuads:
		if uid in folder:
			myStrips.append(folder)
	
	if len(myStrips) == 1:

		print('There is only one file')
		quit()
	if len(myStrips) == 0:
		print('Nothing to see here')
		quit()

	for quadFile in otherQuads:
		quadData = np.loadtxt(str(quadFile), delimiter=', ,', skiprows=1, usecols=(0,2))
		quadChannel = quadData[:,0]
		alamP1 = []
		alamP2 = []

		for i in range(len(quadChannel)):
			channel = quadChannel[i]
			
			if 'P1' in quadFile:
				gfz='P1'
				alamP1.append(Qmap(int(layer), AB, gfz, int(channel), PC, stgc, 0))
			elif 'P2' in quadFile:
				gfz='P2'
				alamP2.append(Qmap(int(layer), AB, gfz, int(channel), PC, stgc, 0))
			amp = quadData[:,1]
	
		if len(alamP1) == len(amp):
			plt.plot(alamP1, amp, color='darkgrey', zorder=0)
			#if a QS1 or QL1, plot both hvs
			if stgc=='QS1' or stgc=='QL1':
				if uid in quadFile:
					if 'HV0' in quadFile:
						plt.plot(alamP1, amp, color='indianred', zorder=10, label='HV0-SP1')
					else:
						plt.plot(alamP1, amp, color='maroon', zorder=10, label='HV1-SP1')
			elif quadFile in myStrips:
				plt.plot(alamP1, amp, color='maroon', zorder=10, label='SP1')
		if len(alamP2) == len(amp):
			plt.plot(alamP2, amp, color='lightgrey', zorder=0)
			if stgc=='QS1' or stgc=='QS1' or stgc=='QL1' or stgc=='QL1':
				if uid in quadFile:
					if 'HV0' in quadFile:
						plt.plot(alamP2, amp, color='rosybrown', zorder=10, label='HV0-SP2')
					else:
						plt.plot(alamP2, amp, color='lightcoral', zorder=10, label='HV1-SP2')
			elif quadFile in myStrips:
				plt.plot(alamP2, amp, color='lightcoral', zorder=10, label='SP2')

	plt.legend(loc='upper right')
	plt.title(str(stgc)+' '+str(PC) + str(uid))
	plt.xlabel("Channel Number (Alam's)")
	plt.ylabel('Amplitude (V)')
	plt.show()

combined_strips(layer, stgc, PC, uid, AB)



